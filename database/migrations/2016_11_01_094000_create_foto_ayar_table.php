<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotoAyarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotogaleri_ayar', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Fotograf');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fotogaleri_ayar');    
    }
}
