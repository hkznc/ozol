<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKategoriTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('kategori', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->Integer('UstKategoriId');
            $table->Integer('KategoriSirasi');
            $table->string('Adi');
            $table->string('Aciklama');
            $table->string('Slug')->unique();
            $table->string('Resim');
            $table->string('Icerik');
            $table->string('MetaTag');
            $table->string('MetaTitle');
            $table->string('MetaDescription');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('kategori');
    }

}
