<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fotogaleri extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('fotogaleriler', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->string('Adi');
            $table->integer('Sira');
            $table->string('MetaTag');
            $table->string('MetaTitle');
            $table->string('MetaDescription');
            $table->tinyInteger('is_active');
            $table->integer('ekleyen');
            $table->integer('songuncelleyen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('fotogaleriler');
    }

}
