<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIletisimBilgilerTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('iletisimbilgileri', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Adres');
            $table->string('Telefon');
            $table->string('Fax');
            $table->string('Eposta');
            $table->string('SubeAdi');
            $table->integer('Sira');
            $table->string('Kordinat');
            $table->tinyInteger('is_active');
            $table->integer('ust_Adres');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('iletisimbilgileri');
    }

}
