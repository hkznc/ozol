<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSosyalmedyaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sosyalmedya', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->string('Adi');
            $table->string('Resim');
            $table->string('LinkAcilisTipi');
            $table->string('Link');
            $table->integer('Sira');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('sosyalmedya');
    }

}
