<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popup', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Resim');
            $table->string('ResimKapak');
            $table->string('FotoGenislik',5);
            $table->string('FotoUzunluk',5);
            $table->string('Genislik',5);
            $table->string('Baslik');
            $table->string('BaslikRenk');
            $table->string('Icerik');
            $table->string('IcerikRenk');
            $table->integer('Sure');
            $table->datetime('baslangic');
            $table->datetime('bitis');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('popup');
    }
}
