<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHaberTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('haber', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Adi');
            $table->string('Slug');
            $table->string('Resim');
            $table->longText('Icerik');
            $table->string('MetaTag');
            $table->string('MetaTitle');
            $table->string('MetaDescription');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('haber');
    }

}
