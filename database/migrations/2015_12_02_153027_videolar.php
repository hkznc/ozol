<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Videolar extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('videolar', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->string('Adi');
            $table->string('Link');
            $table->string('Slug');
            $table->integer('Sira');
            $table->tinyInteger('is_active');
            $table->integer('ekleyen');
            $table->integer('songuncelleyen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('videolar');
    }

}
