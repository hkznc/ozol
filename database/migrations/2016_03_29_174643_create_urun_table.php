<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrunTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('urun', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Adi');
            $table->longtext('Brosur');
            $table->longtext('Manuel');
            $table->longtext('Soru'); 
            $table->string('Urun_UstKat');
            $table->string('Urun_AltKat');  
            $table->integer('Sira');
            $table->tinyInteger('OneCikarilmis');
            $table->tinyInteger('Gosterim');
            $table->string('AltUrunGosterimi');
            $table->tinyInteger('Kampanya');
            $table->tinyInteger('UrunGizli');
            $table->string('Slug');
            $table->string('Resim');
            $table->string('Resim2');
            $table->string('Resim3');
            $table->longText('Icerik');
            $table->longText('Icerik2');
            $table->longText('Icerik3');
            $table->longText('Icerik4');
            $table->longText('Icerik5');
            $table->longText('KisaAciklama');
            $table->string('MetaTag');
            $table->string('MetaTitle');
            $table->string('MetaDescription');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('urun');
    }

}
