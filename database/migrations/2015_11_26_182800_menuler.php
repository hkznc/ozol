<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Menuler extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('menuler', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Adi');
            $table->string('Slug');
            $table->string('MenuTipi');
            $table->string('LinkAcilisTipi');
            $table->longText('Icerik');
            $table->string('Link');
            $table->integer('UstKatId');
            $table->integer('Sira');
            $table->string('MetaTag');
            $table->string('MetaTitle');
            $table->string('MetaDescription');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('menuler');
    }

}
