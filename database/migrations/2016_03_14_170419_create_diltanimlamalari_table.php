<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiltanimlamalariTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('diltanimlamalari', function (Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->integer('DilId');
            $table->integer('SabitId');
            $table->string('Slug');
            $table->string('Ceviri');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('diltanimlamalari');
    }

}
