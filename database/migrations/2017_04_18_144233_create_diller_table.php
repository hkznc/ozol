<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDillerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diller', function (Blueprint $table) {
            $table->increments('id');
            $table->string('KisaAd');
            $table->string('UzunAd');
            $table->string('Resim');
            $table->tinyInteger('DefaultDil');
            $table->tinyInteger('Sira');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('diller');
    }
}
