<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ik', function (Blueprint $table) {
            $table->increments('id');
            $table->text('Fotograf');
            $table->text('Belge');
            $table->string('Adi');
            $table->string('Soyadi');
            $table->string('DogumYeriIl');
            $table->date('DogumTarihi');
            $table->string('Cinsiyet');
            $table->string('TelefonEv');
            $table->string('TelefonCep');
            $table->string('AskerlikDurumu');
            $table->string('TcNo');
            $table->text('EhliyetTipi');
            $table->string('EgitimSeviyesi');
            $table->text('YabanciDiller');
            $table->text('YuksekOgretim');
            $table->string('BilgisayarKullanma');
            $table->string('IsleIlgiliKullanilanAraclar');
            $table->text('Referanslar');
            $table->text('Adres');
            $table->string('BasvuruYapilanPozisyon');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ik');
    }
}
