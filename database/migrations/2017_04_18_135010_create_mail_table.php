<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('DilId');
            $table->string('Driver',50);
            $table->string('Host');
            $table->integer('Port');
            $table->string('GonderilecekMailAdres');
            $table->string('GonderilecekMailAdresIsmi');
            $table->string('Encryption');
            $table->string('KullaniciAdi');
            $table->string('Sifre');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mail');
    }
}
