<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIletisimIsteklerisTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('iletisimistekleri', function(Blueprint $table) {
            $table->engine = "MyISAM";
            $table->increments('id');
            $table->string('AdSoyad');
            $table->string('Eposta');
            $table->string('Telefon');
            $table->string('Konu');
            $table->longText('Mesaj');
            $table->string('gelen_ip',255);
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('iletisimistekleri');
    }

}
