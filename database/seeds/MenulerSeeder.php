<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Menu as Menu;
use App\Http\Fnk;

class MenulerSeeder extends Seeder {

    public function run() {
        Menu::truncate();
        Menu::create([
            'id' => 1,
            'DilId' => 1,
            'Adi' => 'Anasayfa',
            'Slug' => 'Anasayfa',
            'MenuTipi' => 'link',
            'Link' => url('/'),
            'LinkAcilisTipi' => '',
            'Icerik' => '',
            'UstKatId' => 0,
            'Sira' => 1,
            'is_active' => 1
        ]);
        
    }

}
