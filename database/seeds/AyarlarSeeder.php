<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Ayarlar;

class AyarlarSeeder extends Seeder {

    public function run() {
        Ayarlar::truncate();
        Ayarlar::create([
            'id' => 1,    
            'DilId' => 1,    
            'FirmaAdi' => 'Portakal Yazılım',
            'Logo' => 'portakal.png',
            'MetaTag' => json_encode(array('Portakal Yazılım')),
            'MetaTitle' => 'Portakal Yazılım',
            'MetaDescription' => 'Portakal Yazılım',
            'is_active' => 1
        ]);
    }

}
