<?php

use Illuminate\Database\Seeder;
use App\Http\Models\IletisimBilgileri;

class IletisimSeeder extends Seeder {

    public function run() {
        IletisimBilgileri::truncate();
        IletisimBilgileri::create([
            'id' => 1,
            'DilId' => 1,
            'Telefon' => '90 312 299 24 77',
            'Fax' => '90 312 299 23 36',
            'Eposta' => 'info@portakalyazilim.com.tr',
            'SubeAdi' => 'Portakal Yazılım',
            'Adres' => 'Kavaklıdere Mahallesi, Karanfil Sokak, No: 77/3 Kızılay   Çankaya /ANKARA',
            'Kordinat' => '',
            'is_active' => 1
        ]);
       
    }

}
