<?php

use Illuminate\Database\Seeder;
use App\Http\Models\AltMenu as AltMenu;

class AltMenulerSeeder extends Seeder {

    public function run() {
        AltMenu::truncate();
        
    }

}
