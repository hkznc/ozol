<?php

use Illuminate\Database\Seeder;
use App\Http\Models\GoogleAnalytics;

class AnalyticsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        GoogleAnalytics::truncate();
        GoogleAnalytics::create([
            'id' => 1, 
            'Analytics' => '<script> . . .</script>', 
            'AnalyticsAccountId' => '0', 
            'is_active' => '1'
        ]);
       
    }

}
