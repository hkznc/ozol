<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    private function list_files($dir) {
        $seederlar = [];
        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db") {
                        $bol = explode('.', $file);
                        if ($bol[1] == 'php' && @$bol[0] != 'DatabaseSeeder') {
                            $seederlar[] = $bol[0];
                        }
                    }
                }
                closedir($handle);
            }
        }
        return $seederlar;
    }

    public function run() {
        $seederlar = $this->list_files(__DIR__);

        foreach ($seederlar as $dosya) {
            $this->call($dosya);
        }
    }

}
