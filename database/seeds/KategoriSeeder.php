<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Kategori;

class KategoriSeeder extends Seeder {

    public function run() {
      Kategori::truncate();
       
    }

}
