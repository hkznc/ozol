<?php

use Illuminate\Database\Seeder;
use App\Http\Models\IletisimIstekleri;

class IletisimistekSeeder extends Seeder {

    public function run() {
        IletisimIstekleri::truncate();
    }

}
