<?php $__env->startSection('content'); ?>


<div class="container container-fluid padding">
    <div class="containerrow welcome text-center">
        <div class="col-12">
            <h1 class="display-4"><?php echo e(@$ayarlar->AnaSayfaBaslik); ?> </h1>
            <p class="lead"><?php echo e(@$ayarlar->AnaSayfaIcerikSutun1); ?></p>
        </div>
    </div>
</div>

<div class="container padding">
    <div class="row text-center padding">
      <div class="col-xs-12 col-sm-6 col-md-3">
            <img src="<?php echo e(url('images/image/yonetim.jpg')); ?>" class="img-size-card">
            <h3>Yönetim</h3>
            <p>Yönetim, zamanı ve paranızı daha kaliteli kullanmanızı sağlar.</p>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <img src="<?php echo e(url('images/image/guvenlik.jpg')); ?>" class="img-size-card">
            <h3>Güvenlik</h3>
            <p>Güvenliğin olmadığı bir yerde mevcudiyetten bahsedilemez.</p>
        </div>
       
        <div class="col-xs-12 col-sm-6 col-md-3">
            <img src="<?php echo e(url('images/image/kalite.jpg')); ?>" class="img-size-card">
            <h3>Kalite</h3>
            <p>Kalite, yaşamın daha iyi kılınması için gereklidir.</p>
        </div>
         <div class="col-xs-12 col-sm-6 col-md-3">
            <img src="<?php echo e(url('images/image/temizlik.jpg')); ?>" class="img-size-card">
            <h3>Temizlik</h3>
            <p>Temiz olmak performansınızı ve imajınızı artıracaktır.</p>
        </div>
    </div>
    <hr class="my-4">
</div>

<div class="container-fluid padding">
    <div class="row padding" style="margin-left:10%;margin-right: 10%">
        <div class="col-lg-6">
            <h1 class="display-4"> <?php echo e(@$ayarlar->AnaSayfaAltIcerikSutun1); ?></h1>
            <p class="lead"><?php echo e(@$ayarlar->AnaSayfaAltIcerikSutun2); ?></p>

        </div>
        <div class="col-lg-6">
            <img src="<?php echo e(url('images/image/nedenbiz.jpg')); ?>" class="img-fluid">
        </div>

    </div>

</div>

<div class="my-container">
    <div class="my-container-up">

    </div>
</div>


<div class="container-fluid renk padding">
    <div class="row welcome  text-center">
        <div class="col-12  ">
            <h1 class="display-4">Biz Neler Yapıyoruz!</h1>
        </div>
    </div>
</div>

<div class="container-fluid padding">
    <div class="row padding" style="margin-left:10%;margin-right: 10%">

        <?php if(@$tumhizmetler): ?>
            <?php foreach($tumhizmetler as $key=>$value): ?>
              <div class="col-md-4" style="margin-top:1%; word-break: break-word;">
                <div class="card">
                <img class="card-imd-top" src="<?php echo e(url('images/uploads/Hizmetler/').'/'.@$value->Resim); ?>" style="">
                <div class="card-body">
                    <a href="<?php echo e(url($dil.'/hizmetler-url'.'/'.$value->id)); ?>"><h4 class="card-title"><?php echo e(@$value->Adi); ?></h4></a>
                    <p> <?php echo @$value->KisaIcerik; ?></p>
                </div>
                </div>
             </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

</div>



<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style-footer.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style-nav.css')); ?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<style type="text/css">
/*.my-container{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background: fixed url(../images/image/baloncukone.jpg);
    background-size: cover;
}
.my-container-up{
     width: 100%;
    height: 45%;
    background: white;
    opacity: 0.7;
    position: absolute;
}
*/
a{
    color:#1ea24e!important;
    text-decoration: none!important;
}
a:hover{
    color:#0e5453f2!important;
    text-decoration: none!important;
}

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('Site.Layout.Master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>