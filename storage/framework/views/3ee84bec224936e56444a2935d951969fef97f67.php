<?php $__env->startSection('navbar'); ?>
<nav class="navbar navbar-expand-md navbar-light bg-light sticky-top " id="navbar">
    <div class="container-fluid">
        <a class="navbar-brand" href="#"><img src="<?php echo e(url('images/').'/'.@$ayarlar->Logo); ?>" class="img-size-logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">

                 <?php echo Fnk::Menuler(); ?>


            </ul>
        </div>
    </div>
</nav>
<?php $__env->stopSection(); ?>

