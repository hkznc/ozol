<?php $__env->startSection('Title',$menu->MetaTitle); ?>
<?php $__env->startSection('MetaTag'); ?>
    <?php echo @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('MetaDescription',$menu->MetaDescription); ?>
<?php $__env->startSection('content'); ?>

<div class="sayfa-my-container">
    <div class="sayfa-my-container-up">
        <div class="carousel-caption">
         
        </div>

    </div>
</div>


<div class="container-fluid padding">
    <div class="row padding" style="margin-left:10%;margin-right: 10%; word-wrap: break-word!important;margin-top:3%">
    <?php if(!empty($menu->Resim)): ?>
        <div class="col-lg-7 col-md-7 col-xs-12" style=" ">
            <h1 class="display-4"> <?php echo e(@$menu->Adi); ?></h1>
            <p class="lead"><?php echo @$menu->Icerik; ?></p>

        </div>
        <div class="col-lg-5 col-md-5 col-xs-12">
             <img src="<?php echo e(url('images/uploads/Hizmetler/').'/'.@$menu->Resim); ?>" class="img-fluid img-fluid-hizmetler">
        </div>
    <?php else: ?>
         <div class="col-lg-12 col-md-12 col-xs-12">

            <h1 class="display-4" style="margin-top:2%;text-align: center!important;"> <?php echo e(@$menu->Adi); ?></h1>
            <p class="lead"><?php echo @$menu->Icerik; ?></p>

        </div>
    <?php endif; ?>
    </div>

</div>











<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style-footer.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style-nav.css')); ?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
/*.sayfa-my-container{
    width: 100%;
    height: 45%;
    margin-top: -65px;
   background: fixed url(../images/image/baloncukone.jpg)!important;
    background-size: cover;

}
.sayfa-my-container-up{
    width: 100%;
    height: 45%;
    background: fixed url(../images/image/baloncukone.jpg);
    opacity: 0.7;
    position: absolute;
}*/
.my-container-two{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background:  fixed url("../images/image/kurumsaltwo.jpg") ;
    background-size: cover;

}
</style>




<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('Site.Layout.Master2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>