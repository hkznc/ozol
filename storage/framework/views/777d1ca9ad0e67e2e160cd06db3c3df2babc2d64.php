<?php $__env->startSection('content'); ?>

<div class="hizmetler-my-container">
    <div class="hizmetler-my-container-up">
        <div class="carousel-caption">
            <h1 class="display-3">Bloglar</h1>
        </div>
    </div>
</div>
<div class="container-fluid padding">
    <div class="row padding" style="margin-left:10%;margin-right: 10%">

        <?php if(@$bloglar): ?>
            <?php foreach($bloglar as $key=>$value): ?>
              <div class="col-md-4" style="margin-top:1% ; word-break: break-word;">
                <div class="card">
                <img class="card-imd-top" src="<?php echo e(url('images/uploads/').'/'.@$value->Resim); ?>" style="">
                <div class="card-body" >
                    <h4 class="card-title"><a href="<?php echo e(url($dil.'/'.Fnk::Ceviri("bloglar").'/'.$value->id)); ?>"><?php echo e($value->Adi); ?></a></h4>
                    <p > <?php echo substr(strip_tags($value->Icerik),0,150); ?> .....</p>
                </div>
                </div>
             </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>






<?php $__env->stopSection(); ?>
<?php $__env->startSection('css'); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style-footer.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/style-nav.css')); ?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo e(asset('css/style-services.css')); ?>">
<style type="text/css">
.my-container{
     width: 100%;
    height: 40%;
    margin-top: 0;
    background:  fixed url("../images/image/srou-isareti.jpg") ;
    background-size: cover;
}
.my-container-up{

    height: 40%;
    background: rgba(50,80,70,0.3)!important;
    opacity: 0.7;
    position: absolute;
}


/*.bg-card{
    background: fixed url("../image/card-backgraound-leave.jpg");
     /*background: rgba(50,80,70,0.3)!important;*/
 /*}*/
</style>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Site.Layout.Master2', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>