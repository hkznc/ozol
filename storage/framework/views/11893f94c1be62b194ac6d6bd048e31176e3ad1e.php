<?php $__env->startSection('footer'); ?>
<div class="container-fluid padding" style="height: auto;">
    <div class="row text-center padding">
        <div class="col-12" >
            <h2>Bizi Sosyal Medya'da Takip Edin</h2>
            <br>
        </div>
        <div class="col-12 social padding">
            <ul class="social-network social-circle">
                <?php if(!empty($sosyal)): ?>
                    <?php foreach($sosyal as $key=>$value): ?>
                    <li><a href="<?php echo e(@$value->Link); ?>" <?php echo e(@$value->LinkAcilisTipi); ?> class="ico<?php echo e(mb_convert_case(mb_strtolower($value->Adi), MB_CASE_TITLE, "UTF-8")); ?> bg-icon-media" title="<?php echo e(mb_convert_case(mb_strtolower($value->Adi), MB_CASE_TITLE, "UTF-8")); ?>"><i class="fa fa-<?php echo e($value->Adi); ?>"></i></a></li>

                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
<footer>
    <div class="container-fluid padding footer">
        <div class="row text-center">
            <div class="col-md-4">
                <hr class="light">
                <h5> Bize Ulaşın</h5>
                <hr class="light">
                <p>
                    <a href="tel:<?php echo e($iletisimbilgileri->Telefon); ?>" class="col-md-3">
                        <i class="fa fa-phone-square "></i>
                    <?php echo e(@$iletisimbilgileri->Telefon); ?>

                    </a>
                </p>
                <?php if(!empty($iletisimbilgileri->Telefon2)): ?>
                 <p>
                    <a href="tel:<?php echo e($iletisimbilgileri->Telefon2); ?>" class="col-md-3">
                        <i class="fa fa-phone-square "></i>
                    <?php echo e(@$iletisimbilgileri->Telefon2); ?>

                    </a>
                </p>
                <?php endif; ?>

                <p>
                    <a href="#" class="col-md-3">
                        <i class="fa fa-fax "></i>
                    <?php echo e(@$iletisimbilgileri->Fax); ?>

                    </a>
                </p>

                <p>
                    <a href="#" class="col-md-3">
                        <i class="fa fa-location-arrow"></i>
                    </a>
                <?php echo e(@$iletisimbilgileri->Adres); ?>

                </p>

            </div>
            <div class="col-md-4">
                <hr class="light">
                <h5> Çalışma Saatlerimiz</h5>
                <hr class="light">
                <p> Hafta içi: <?php echo e(@$iletisimbilgileri->Haftaici_calismasaatleri); ?></p>
                <p> Hafta sonu: <?php echo e(@$iletisimbilgileri->Haftasonu_calismasaatleri); ?></p>
            </div>
            <div class="col-md-4">
                <hr class="light">
                <h5>Hızlı Gezin</h5>
                <hr class="light">
                <?php if(!empty($altmenuler)): ?>
                    <?php echo e(Fnk::Altmenuler()); ?>

                <?php endif; ?>
               <?php /*  <p><a href="#" class="text-light">Teklif Al</a></p>
                <p><a href="html/kurumsal.html" class="text-light">Hakkımızda</a></p>
                <p><a href="#" class="text-light">İletişim</a></p>
                <p><a href="html/galeri.html" class="text-light">Galeri</a></p> */ ?>

            </div>
            <div class="col-12">
                <hr class="light">
                <h5><a href="#" type="link" data-toggle="modal" data-target="#myModal" style="color: #b4b4b4;font-size: 10px">Web Tasarım,
                    Programlama ve SEO: PORTAKAL YAZILIM</a></h5>
            </div>
        </div>

        <!-- The Modal -->

    </div>
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->

                <!-- Modal body -->
                <div class="modal-body">
                    <p>Özol Yönetim web sayfası tasarım ve kodlaması Portakal Yazılım tarafından, günümüzün en son
                        teknoloji standartlarına uygun olarak yapılmıştır.
                    <p>Bu standartlar uygulanırken arama motorlarında sıralama, her türlü platformdan ve cihazdan
                        erişebilirlik, responsive uyumluluğu, hızlı yüklenme, güncelleme ve bakım işlemlerinin
                        yönetilebilir ve kolay olması, hukuksal sorumluluk gibi değerler gözetilmiştir. </p>
                    Sayfa tasarlanırken ve kodlanırken uygulanan teknolojilerden bazıları şunlardır: W3C, HTML5, Ajax,
                    Jquery, Php, Laravel, MVC, MySQL.
                    Web sunucusu hizmeti Portakal Yazılım'ın Türkiye lokasyonlu kendi sunucuları üzerinden
                    sağlanmaktadır.</p>

                    <p> Bu Projeye Portakal Yazılım tarafından profesyonel SEO hizmeti verilmektedir.
                        Proje hakkında detaylı bilgi için <a href="http://www.portakalyazilim.com.tr/tr/" type="link">tıklayınız</a>.
                    </p>
                    <img src="<?php echo e(url('images/image/logoseffaf-1.gif')); ?>" style="width: 30%;height: auto">
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="link" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
                </div>

            </div>
        </div>
    </div>
</footer>

<?php $__env->stopSection(); ?>