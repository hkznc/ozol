<?php $__env->startSection('slider'); ?>
<div id="sld" class="carousel slide" data-ride="carousel">

<?php if(@$sliders): ?>
    <ul class="carousel-indicators">
        <?php foreach($sliders as $key=>$slider): ?>
            <li data-target="#sld" data-slide-to="<?php echo e($key); ?>" <?php if($key==0): ?> class="active" <?php endif; ?>></li>
        <?php endforeach; ?>
    </ul>
    <div class="carousel-inner ">
    <?php foreach($sliders as $key1=>$slider): ?>
      <div <?php if($key1==0): ?> class="carousel-item active " <?php else: ?> class="carousel-item " <?php endif; ?> >
            <img src="<?php echo e(url('images/uploads/Slider').'/'.$slider->Resim); ?>" alt="Ev ve ofis" class="img-slide-size">
            <div class="carousel-caption">
                <h1 class="display-2"><?php echo e(@$slider->Icerik); ?></h1>
                <h3><?php echo e(@$slider->Icerik2); ?></h3>
                 <?php if($key1==0): ?>
                <a href="<?php echo e(url(Request::segment(1).'/'.\App\Http\Fnk::Ceviri('iletisim-url'))); ?>" type="button" class="btn btn-success btn-lg" style="margin-top: 13%">Bizden Teklif Alın</a>
                <?php endif; ?>
            </div>
        </div>

    <?php endforeach; ?>
    <a class="carousel-control-prev" href="#sld" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#sld" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
<?php endif; ?>
</div>


<?php $__env->stopSection(); ?>