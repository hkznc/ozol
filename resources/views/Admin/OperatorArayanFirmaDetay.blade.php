@extends('Admin.Layout.Master')

@section('title', 'Operatör Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Firma Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->FirmaAdi}}</td>
				</tr>			
				<tr>
					<th style="width:200px;">Firma Adresi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->FirmaAdres}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Firma Telefonu</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>+9{{$veri->FirmaTelefon}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Firma E-Posta</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->FirmaEmail}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Firma Yetkilisi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->FirmaYetkili}}</td>
				</tr>

				<tr>
					<th style="width:200px;">Yetkilinin Görevi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ $veri->YetkiliGorevi }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Yetkilinin Telefonu</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->YetkiliTelefon}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Firma Web Sitesi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->FirmaWebSitesi}}</td>
				</tr>
				
				<tr>
					<th style="width:200px;">Çalışma Türü</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->CalismaTuru}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Çalışma Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->CalismaTipi}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Günlük/Aylık Çalışma Süresi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->GACalismaSuresi}}</td>
				</tr>
				
				@foreach( json_decode($veri->DeliciAdi) as $key=>$value )
				<?php $key++;?>
				<tr>
					<th style="width:200px;">{{$key."."}} Delici Adı  / Modeli </th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>
						@foreach(@$value as $value_)
						->{{$value_}}  
						@endforeach
					</td>
				</tr>
				@endforeach


				@foreach( json_decode(@$veri->Ulke) as $key=>$array )
				<?php $key++;?>
				<tr>
				
					<th style="width:200px;">{{$key."."}} Ülke  / İl / İlçe </th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>
						@foreach(@$array as $value_)
							->{{$value_}}
						@endforeach
					</td>
				</tr>
				@endforeach

				<tr>
					<th style="width:200px;">Patlatma Belgesi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->PatlatmaBelgesi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Operator Ehliyeti</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->OperatorEhliyeti}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Yag/Filtre Bakım Bilgisi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->YagFiltreBakimBilgisi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Delici Deneyim Süresi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->DeliciDeneyimSuresi}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop