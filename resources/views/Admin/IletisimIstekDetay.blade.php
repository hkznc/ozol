@extends('Admin.Layout.Master')

@section('title', 'İletişim İstek Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Adı Soyadı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->AdSoyad}}</td>
				</tr>
				<tr>
					<th style="width:200px;">E-Posta Adresi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Eposta}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Telefon}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Konu</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Konu}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Mesaj</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Mesaj!!}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->created_at->format('d.m.Y H:i')}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->updated_at->format('d.m.Y H:i')}}</td>
				</tr>
				<tr>
					<td colspan="3">
					<a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a>
					<a href="{{url('Admin/IletisimIstekleri/Cevapla/'.$veri->id)}}" class="btn btn-success pull-right" style="margin-right:10px">Cevapla</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop