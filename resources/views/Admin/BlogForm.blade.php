@extends('Admin.Layout.Form')

@section('baslik', 'Blog')

@section('form')

<div class="form-group">
    <label for="Icerik" class="col-sm-2 control-label">Haber Başlik</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Haber Adı">
    </div>
</div>
<div class="form-group" >
    <label class="col-sm-2 control-label">Haber Resmi</label>
    <div class="col-sm-10">
       <img src="{{url('images/uploads/'.@$veri->Resim)}}" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
      <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
<div class="form-group">
    <label for="Icerik" class="col-sm-2 control-label">Haber İçerik</label>
    <div class="col-sm-10">
        <textarea  type="text" class="form-control ck" required value="" name="Icerik" id="Icerik" placeholder="Haber İçerik">{{@$veri->Icerik}}</textarea>
    </div>
</div>
@stop
@section('js')

@stop