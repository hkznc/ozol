@extends('Admin.Layout.Master')

<?php
	$url = Request::url();
	$link = explode('Sirala',$url);
	$link = $link[0].'Sirala' ;
?>
@section('title') Foto Galeri Sıralama @endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="pull-left">@yield('title')</span>
    	</div>
        <div class="panel-body">
            <p class="alert alert-info">
        		@yield('title') İşlemini Sürükle - Bırak Yaparak Sitenizdeki Görünüm Sırasını Değiştirebilirsiniz.
        	</p>
        	<div id="sortable">
                <?php 
                    foreach( $galeri as $sirasayisi=>$v ){
                        $fotograf = \App\Http\Models\Fotograf::where('GaleriId', $v->id)->where('is_active',1)->orderby('Sira','asc')->first();
                        $sirasayisi++;   
                        echo '
                            <div class="col-xs-2" style="margin-left:10px;margin-bottom:5px;width:150px;height:150px;" id="id-'.$v->id.'">
                                <p>
                                    <img width="100" height="100" src="'.url('images/uploads/FotoGaleri/'.@$fotograf->Resim.'').'" class="img-rounded img-responsive">'.$sirasayisi.'.</img>
                                </p>
                            </div>';
                    }
        	   ?>
        	</div>
        </div>
    </div>
@stop

@section('css')
    
    <style>
        #sortable > div { float: left; }
        img { padding: 0.5em; }
    </style>

@stop

@section('js')
    
    <script src="{{asset('plugins/jquery-ui/jquery-ui.js')}}"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sortable').sortable({
                axis: 'x,y',
                revert: true,
                stop: function (event, ui) {
                    var oData = $(this).sortable('serialize');
                    console.log(oData);
                   	$.ajax({
                        data: oData+'&_token={{{csrf_token()}}}',
                        type: 'POST',
                        url: '{{$link}}'
                    });
                }
            });
        });
    </script>

@stop