@extends('Admin.Layout.Master')

@section('title', 'Foto Galeri Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Foto Galeri Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi}}</td>
				</tr>
                <tr>
                    <th style="width:200px;">Kapak Fotoğrafı</th>
                    <th style="width: 30px;text-align:center;">:</th>
                    <td><img width='150' height='150' src="{{url('images/uploads/FotoGaleri/kapak/'.$veri->KapakFotografi)}}" style="max-width: 100%;"></td>
                </tr>
				<tr>
					<th style="width:200px;">Fotoğraflar</th>
					<th style="width: 30px;text-align:center;">:</th>
                    <td>
                        @foreach($foto as $f)
                        <div class="img-wrap" id='resimgoster{{$f->id}}'>
                            <span class="close">&times;</span>
                            <img height="100" width="100" src="{{url("/images/uploads/FotoGaleri/".$f->Resim)}}" data-id="{{$f->id}}">
                        </div>
                        @endforeach
                    </td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTag}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTitle}}</td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaDescription}}</td>
				</tr>
				
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
                      <?php
                          $ekleyen = \App\Http\Models\User::find($veri->ekleyen);
                          $guncelleyen = \App\Http\Models\User::find($veri->songuncelleyen);
                      ?>
                <tr>
					<th style="width:200px;">Ekleyen</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$ekleyen->name or '-'}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Son Güncelleyen</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$guncelleyen->name or '-'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop

@section('css')
<style>
    .img-wrap {
        float:left;
        margin:4px;
        position: relative;
    border: 1px red solid;
    font-size: 0;
    width:102px;
    height:102px;
}
.img-wrap .close {
    position: absolute;
    top: 2px;
    right: 2px;
    z-index: 100;
    background-color: #FFF;
    padding: 5px 2px 2px;
    color: #000;
    font-weight: bold;
    cursor: pointer;
    opacity: .2;
    text-align: center;
    font-size: 22px;
    line-height: 10px;
    border-radius: 50%;
}
.img-wrap:hover .close {
    opacity: 1;
}
</style>
@stop

@section('js')
<script>
$('.img-wrap .close').on('click', function() {
    var id = $(this).closest('.img-wrap').find('img').data('id');
     AjaxPost('Foto/ResimSil/'+id,''); 
     $('#resimgoster'+id).remove();
});
</script>
@stop