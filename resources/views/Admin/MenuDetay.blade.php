@extends('Admin.Layout.Master')

@section('title', 'Menü Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Üst Menü Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$ustkat}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Menü Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Link Açılış Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>@if($veri->LinkAcilisTipi=='') Aynı Pencere @else Yeni Pencere @endif</td>
				</tr>
				<tr>
					<th style="width:200px;">Menü Tipi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MenuTipi}}</td>
				</tr>
				@if($veri->MenuTipi=='link' || $veri->MenuTipi=='modul')
				<tr>
					<th style="width:200px;">Menü Linki</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Link}}</td>
				</tr>
				@endif
				@if($veri->MenuTipi=='icerik')
				<tr>
					<th style="width:200px;">Menü İçeriği</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Icerik!!}</td>
				</tr>
				@endif

				@if(!empty($veri->Misyon))
				<tr>
					<th style="width:200px;">Misyon İçeriği</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Misyon!!}</td>
				</tr>
				@endif
				@if(!empty($veri->MisyonResim))
				<tr>
					<th style="width:200px;">Misyon Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width="200" height="200" src="{{url('/')}}/images/image/{{$veri->MisyonResim}}" /></td>
				</tr>
				@endif
				@if(!empty($veri->Vizyon))
				<tr>
					<th style="width:200px;">Vizyon İçeriği</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Vizyon!!}</td>
				</tr>
				@endif
				@if(!empty($veri->VizyonResim))
				<tr>
					<th style="width:200px;">Vizyon Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width="200" height="200" src="{{url('/')}}/images/image/{{$veri->VizyonResim}}" /></td>
				</tr>
				@endif
				@if(!empty($veri->Hakkimizda))
				<tr>
					<th style="width:200px;">Hakkimizda İçeriği</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Hakkimizda!!}</td>
				</tr>
				@endif
				@if(!empty($veri->Resim))
				<tr>
					<th style="width:200px;">Hakkimizda Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width="200" height="200" src="{{url('/')}}/images/image/{{$veri->Resim}}" /></td>
				</tr>
				@endif

                <tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTag}}</td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTitle}}</td>
				</tr>
                                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaDescription}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop