@extends('Admin.Layout.Form')

@section('baslik', 'Haberler')

@section('form')

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Haber Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Haber Adı">
    </div>
</div>

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
    @if( @$veri->Resim )
        <img src="{{url('images/uploads/Haberler/'.$veri->Resim)}}" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
        @endif
      <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Haber Kısa İçeriği</label>
    <div class="col-sm-10">
        <input type="text" name="KisaIcerik" class="form-control" >{{ @$veri->KisaIcerik}}</input>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Haber İçeriği</label>
    <div class="col-sm-10">
        <textarea name="Icerik" class="ck" >{{ @$veri->Icerik}}</textarea>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Keywords</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->MetaTag}}" name="MetaTag" id="MetaTag" placeholder="Meta Keywords">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Title</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->MetaTitle}}" name="MetaTitle" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Meta Description</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->MetaDescription}}" name="MetaDescription" id="MetaDescription" placeholder="Meta Description">
    </div>
</div>

@stop