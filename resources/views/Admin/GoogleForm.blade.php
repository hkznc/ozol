@extends('Admin.Layout.Form')

@section('baslik', 'Google')

@section('form')

<center><h3>Re Captcha</h3></center>
<div class="form-group">
    <label for="site_key" class="col-sm-2 control-label">Site Key</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="{{@$veri->site_key}}" name="site_key" id="site_key" placeholder="site_key">
    </div>
</div>
<div class="form-group">
    <label for="secret_key" class="col-sm-2 control-label">Secret Key</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="{{@$veri->secret_key}}" name="secret_key" id="secret_key" placeholder="secret_key">
    </div>
</div>
<center><h3>Google Site Doğrulama</h3></center>
<div class="form-group">
    <label for="content" class="col-sm-2 control-label">Content Doğrulama Kodu</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="{{@$veri->content}}" name="content" id="content" placeholder="Doğrulama Kodu (content)">
    </div>
</div>
<center><h3>Google Analytics</h3></center>
<div class="form-group">
    <label class="col-sm-2 control-label">Analytics İzleme Kodu</label>
    <div class="col-sm-10">
        <input class="form-control" name="Analytics" id="Analytics" value="{{@$veri->Analytics}}" placeholder="UA-xxxxxxxx-x"></input>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Analytics Görünüm Kimliği</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="{{@$veri->AnalyticsAccountId}}" name="AnalyticsAccountId" id="AnalyticsAccountId" placeholder="Analytics Account Id">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-2 control-label">Açıklama</label>
    <div class="col-sm-10">
        Analytics Account Id alabilmek için google.com/analytics girdikten sonra üst menüden Yönetici > Kullanıcı Yönetimi > "serviceaccount@portakal-yazilim.iam.gserviceaccount.com" Ekle > Ayarları Görüntüle > Görünüm Kimliği buradaki id koplayalıp yapıştırın!
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {
       $('.gizli').hide();
       $('.is_active').hide();
        
    });
</script>
@stop