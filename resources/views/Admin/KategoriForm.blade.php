@extends('Admin.Layout.Form')

@section('baslik', 'Kategori')

@section('form')
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Kategori Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Kategori Adı">
    </div>
</div>

<div class="form-group">
    <label for="KategoriSirasi" class="col-sm-2 control-label">Kategori Sırası</label>
    <div class="col-sm-10">
        <input type="number" class="form-control" name="KategoriSirasi" id="KategoriSirasi" value="{{@$veri->KategoriSirasi}}" placeholder="KategoriSirasi">
    </div>
</div>

<div class="form-group">
    <label for="UstKategoriId" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="UstKategoriId" required name="UstKategoriId">
            <option value="0" >Lütfen Kategori Seçiniz</option>
            @foreach (@$kategoriler as $kategori)
                <option value="{{$kategori['id']}}" @if ($kategori["id"] == @$veri->UstKategoriId) selected @endif>
                    @for ($i = 0; $i < $kategori["derinlik"]; $i++)
                    -
                    @endfor
                    {{$kategori["adi"]}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group" hidden>
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
<div class="form-group">
     <label for="Icerik" class="col-sm-2 control-label">Kategori İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck">{{@$veri->Icerik}}</textarea>
    </div>
</div>
<div class="hide form-group" >
    <label for="Aciklama" class="col-sm-2 control-label" >Açıklama</label>
    <div class="col-sm-10">
        <textarea name="Aciklama" class="ck">{{ @$veri->Aciklama }}</textarea>
    </div>
</div>
@stop

@section('js')

<script type="text/javascript">
    
    $(document).ready(function() {
            $('.gizli').hide();        
    });

</script>

@stop