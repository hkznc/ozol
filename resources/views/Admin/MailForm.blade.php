@extends('Admin.Layout.Form')

@section('baslik', 'E-posta')

@section('form')

<div class="form-group">
    <label for="ayar" class="col-sm-2 control-label">Mail Ayar</label>
    <div class="col-sm-10">
    <select onchange="mailAyariDegisti()" class="form-control" id="ayar" name="ayar">
        <option value="yandex">Yandex</option>
        <option value="gmail">Gmail</option>
        <option value="standart">Manuel</option>
    </select>
    </div>

</div>

<div class="form-group" hidden="true">
    <label for="Driver" class="col-sm-2 control-label">Driver </label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="smtp" value="{{ @$veri->Driver }}" name="Driver" id="Driver" placeholder="Driver(Örn smtp)">
    </div>
</div>

<div class="form-group">
    <label for="Host" class="col-sm-2 control-label">Smtp Sunucusu</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Host}}" name="Host" id="Host" placeholder="Host(Örn smtp.yandex.com.tr)">
    </div>
</div>

<div class="form-group">
    <label for="Port" class="col-sm-2 control-label">Port</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Port}}" name="Port" id="Port" placeholder="Port (Örn 587,26)">
    </div>
</div>

<div class="form-group">
    <label for="GonderilecekMailAdres" class="col-sm-2 control-label">Gönderen E-posta</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->GonderilecekMailAdres}}" name="GonderilecekMailAdres" id="GonderilecekMailAdres" placeholder="Gönderen E-posta (Örn info@portakalyazilim.com)">
    </div>
</div>

<div class="form-group">
    <label for="GonderilecekMailAdresIsmi" class="col-sm-2 control-label">Gönderen E-posta Açıklaması</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->GonderilecekMailAdresIsmi}}" name="GonderilecekMailAdresIsmi" id="GonderilecekMailAdresIsmi" placeholder="Gönderen E-posta Açıklaması (Örn Portakal Yazilim)">
    </div>
</div>

<div class="form-group">
    <label for="Encryption" class="col-sm-2 control-label">Bağlantı Metodu</label>
    <div class="col-sm-10">
        <select id="Encryption" name="Encryption" onchange="baglantiMetodu()" class="form-control">
            <option value="tls"@if(@$veri->Encryption == "tls") selected @endif>tls</option>
            <option value="ssl"@if(@$veri->Encryption == "ssl") selected @endif>ssl</option>
        </select>
    </div>
</div>

<div class="form-group">
    <label for="KullaniciAdi" class="col-sm-2 control-label">Kullanici Adi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->KullaniciAdi}}" name="KullaniciAdi" id="KullaniciAdi" placeholder="Kullanıcı Adı (Örn Portakal Yazılım)">
    </div>
</div>

<div class="form-group">
    <label for="Sifre" class="col-sm-2 control-label">Şifre</label>
    <div class="col-sm-10">
        <input type="password" class="form-control" required value="{{@$veri->Sifre}}" name="Sifre" id="Sifre" placeholder="Şifre (Örn portakal)">
    </div>
</div>

@stop
@section('js')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('.is_active').hide();
            $('#Host').val('smtp.yandex.com.tr');
            $('#Port').val('465');
            var ayar = "{{ @$veri->Host }}";

            if(ayar == "smtp.gmail.com"){
                $('#ayar option[value=gmail]').attr('selected','selected');

            }
            else if(ayar == "smtp.yandex.com.tr"){
                $('#ayar option[value=yandex]').attr('selected','selected');
            }
            else{
                $('#ayar option[value=standart]').attr('selected','selected');

            }
           mailAyariDegisti();
           
        });

        function mailAyariDegisti(){
            if($('[name="ayar"]').val() == 'yandex'){
                $('#Host').val('smtp.yandex.com.tr');
                $('#Port').val('465');

            }else if($('[name="ayar"]').val() == 'gmail'){
                $('#Host').val('smtp.gmail.com');
                $('#Port').val('587');

            }else{
                $('#Host').val('');
                $('#Port').val('');
            }
        }

        function baglantiMetodu(){
            if($('[name="ayar"]').val() == 'gmail' && $('[name="Encryption"]').val()=='tls'){
                $('#Port').val('587');
            }else{
                $('#Port').val('465');
            }

        }
    </script>
@stop

