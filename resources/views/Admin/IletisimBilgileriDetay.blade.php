@extends('Admin.Layout.Master')

@section('title', 'Şube Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Şube Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{@$veri->SubeAdi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Adres</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{@$veri->Adres}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>+9{{@$veri->Telefon}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Telefon 2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>+9{{@$veri->Telefon2}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Fax</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>+9{{@$veri->Fax}}</td>
				</tr>
				<tr>
					<th style="width:200px;">E-Posta</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{@$veri->Eposta}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Haftaiçi Çalışma Saatleri</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{@$veri->Haftaici_calismasaatleri}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Haftasonu Çalışma Saatleri</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{@$veri->Haftasonu_calismasaatleri}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{(@$veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle(@$veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle(@$veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop