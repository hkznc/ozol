@extends('Admin.Layout.Form')
@section('baslik')Dil Tanımlama @endsection
@section('form')
<input type="hidden" name="id" value="{{@$veri->id}}">
<table class="table table-striped table-bordered table-hover">
<tr>
    <th>Slug</th>
    <th>
        <select class="form-control" name="DilId" onchange="window.location.href='{{url('Admin/Dil/Tanimla')}}/'+this.value+'/{{$sabitdil->id}}'">
            @foreach($diller as $dil)
                <option @if($dil->id==Request::segment('4')) selected @endif value='{{$dil->id}}'>{{$dil->UzunAd}}</option>
            @endforeach
        </select>
    </th>
    <th>{{$sabitdil->UzunAd}}</th>
</tr>
@foreach($dilsabitleri as $d)
<tr>
    <td><label class="control-label">{{$d->Slug}}</label></td>
    <td><input type="text" class="form-control" value="{{@$defaultunceviri[$d->Slug]}}" name="" placeholder="@if(isset($gecerlidil[$d->Slug])){{$gecerlidil[$d->Slug]}} (geçerli dil)@else{{$d->SabitAdi}} (sistem)@endif" readonly /></td>
    <td><input type="text" class="form-control" value="{{@$diltanimlamalari[$d->Slug]}}" name="Ceviri[{{$d->id}}]" placeholder="@if(isset($diltanimlamalari[$d->Slug])) {{$diltanimlamalari[$d->Slug]}} @endif"></td>
</tr>
@endforeach
</table>
@stop

@section('js')
@stop
