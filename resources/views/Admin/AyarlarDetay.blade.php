@extends('Admin.Layout.Master')

@section('title', 'Ayarlar Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Dil</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->DilId}}</td>
				</tr>

				<tr>
					<th style="width:200px;">Firma Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->FirmaAdi!!}</td>
				</tr>
				<tr>
					<th style="width:200px;">Anasayfa Alt Slider Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="{{url('images/uploads/'.$veri->AnasayfaResim)}}" style="max-width: 100%;"></td>
				</tr>
				<tr>
					<th style="width:200px;">Logo</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="{{url('images/uploads/'.$veri->Logo)}}" style="max-width: 100%;"></td>
				</tr>

				<tr>
					<th style="width:200px;">Anasayfa Slogan</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaBaslik!!}</td>
				</tr>
				<tr>
					<th style="width:200px;">Anasayfa İçerik Sütun 1</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaIcerikSutun1!!}</td>
				</tr>
 				<tr>
					<th style="width:200px;">Anasayfa İçerik Sütun 2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaIcerikSutun2!!}</td>
				</tr>
				<tr>
					<th style="width:200px;">Anasayfa Alt İçerik Sütun 1</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaAltIcerikSutun1!!}</td>
				</tr>
 				<tr>
					<th style="width:200px;">Anasayfa Alt İçerik Sütun 2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaAltIcerikSutun2!!}</td>
				</tr>
	 			<!--22.12.2016 günü anasayfadaki en alt kısma sadece fotoğraf eklenmek istendiği için iptal edilmiştir -->
	 			<!-- <tr>
					<th style="width:200px;">Anasayfa Alt Slider Başlık</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaAltSliderBaslik!!}</td>
				</tr>
	 			<tr>
					<th style="width:200px;">Anasayfa Alt Slider İçerik Sütun 1</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaAltSliderSutun1!!}</td>
				</tr>
	 			
	 			<tr>
					<th style="width:200px;">Anasayfa Alt Slider İçerik Sütun 2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaAltSliderSutun2!!}</td>
				</tr>
	 			
	 			<tr>
					<th style="width:200px;">Anasayfa Alt Slider İçerik Sütun 3</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnaSayfaAltSliderSutun3!!}</td>
				</tr> -->
				
				<tr>
					<th style="width:200px;">Analytics</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Analytics!!}</td>
				</tr>

				<tr>
					<th style="width:200px;">Google Analytics Id</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->AnalyticsAccountId!!}</td>
				</tr>

				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop