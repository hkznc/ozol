@extends('Admin.Layout.Master')

@section('title', 'İletişim Cevap')

@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
  	<h3 class="panel-title">@yield('title')<div class="progress pull-right" style="width: 200px;display: none;">
		  <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" id="uploadbar"></div>
		</div></h3>
  		
  </div>
  <div class="panel-body">
		<form method="post" enctype="multipart/form-data" class="form-horizontal" action="javascript:;" id="form">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="id" value="{{$veri->id}}">
		  <div class="form-group">
		    <label for="Adi" class="col-sm-2 control-label">Konu</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" required value="RE:{{@$veri->Konu}}" name="Konu" id="Konu" placeholder="Konu">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="Icerik" class="col-sm-2 control-label">Mesaj</label>
		    <div class="col-sm-10">
		    	<textarea required id="Mesaj" name="Mesaj" class="ck">
		    	<br>
		    	<p><b>Alıntı :</b></p>
		    	<hr>
		    	<p><b>Tarih :</b> {{$veri->created_at->format('d.m.Y H:i')}}</p>
		    	<p><b>Konu :</b> {{$veri->Konu}}</p>
		    	<p><b>Mesajınız :</b> {{@$veri->Mesaj}}</p>
		    	</textarea>
		    </div>
		  </div>
		  
		  <div class="form-group">
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" id="btn" class="btn btn-success">Gönder</button>
			    </div>
			  </div>
		</form>
  </div>
</div>
@stop



@section('jsform')
<script src="{{asset('js/jquery.form.js')}}"></script>
<script src="{{asset('plugins/jqueryvalidate/jquery.validate.min.js')}}"></script>
<script src="{{asset('plugins/jqueryvalidate/messages_tr.js')}}"></script>
<script src="{{asset('plugins/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('plugins/ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('plugins/ckfinder/ckfinder.js')}}"></script>


<script type="text/javascript">
$( document ).ready( function() {
	if($( 'textarea.ck' ).length>0){
		$( 'textarea.ck' ).ckeditor();
			if ( typeof CKEDITOR == 'undefined' ){
			}else{
				var editor = $('.ck').ckeditorGet();
				CKFinder.setupCKEditor( editor, '{{dirname($_SERVER["SCRIPT_NAME"])}}/plugins/ckfinder/' ) ;
			}
	}
});
function uploadbartemizle(){
	$('#uploadbar').parent('div').hide();
	var bar = $('#uploadbar');
	var percentVal = '0%';
	bar.width(percentVal);
	bar.html(percentVal);
	bar.attr('aria-valuenow',0);
}

function AjaxPost(url,post,callback){
	console.log('dsadasda');
	post+='&_token={{csrf_token()}}'
	jQuery.post("{{url('')}}/"+url, post, function (data) {
		if(callback && typeof(callback) === "function") {
			callback(data);
		}
	}, "json");
}
	
$( document ).ready(function() {
	$('#form').validate({
		ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
		highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        },
         submitHandler: function(form) {
         	return true;
         }
	});
	
	var bar = $('#uploadbar');
	$('#form').ajaxForm({
		url:'{{url('')}}',
	    beforeSend: function() {
			bar.parent('div').show();
	        var percentVal = '0%';
	        bar.width(percentVal);
	        bar.html(percentVal);
	        bar.attr('aria-valuenow',0);
	        $('#btn').attr('disabled',true);
	    },
	    uploadProgress: function(event, position, total, percentComplete) {
	        var percentVal = percentComplete + '%';
	        bar.attr('aria-valuenow',percentComplete);
	        bar.width(percentVal);
	        bar.html(percentVal);
	    },
	    success: function() {
	        var percentVal = '100%';
	        bar.attr('aria-valuenow',100);
	        bar.width(percentVal);
	        bar.html(percentVal);
	        $('#btn').removeAttr('disabled');
	    },
		complete: function(xhr) {
			var sonuc = JSON.parse(xhr.responseText);
			
			if(sonuc.islem){
				
				new PNotify({
				    title: '@yield("title")',
				    text: '@yield("title") İşleminiz Başarıyla Yapılmıştır.',
				    type: 'success',
				    styling: 'bootstrap3'
				});
				
				if (typeof(submitafter) === "function") {
				 	submitafter(sonuc);
				 }
				
				$("#form")[0].reset();
				

				if($('.selectize').length>0){
					$( ".selectize" ).each(function( index ) {
						if(index%3==0)this.selectize.clear()
					});
				}				
			//	jQuery("a[data-toggle=collapse][href=#adim1]").click();

			}else{
				$.each(sonuc.error, function(index, value) {
				   	new PNotify({
					    title: 'HATA',
					    text: value,
					    type: 'error',
					    styling: 'bootstrap3'
					});
				});
				$('#btn').removeAttr('disabled');
			}
			
			jQuery(document).ready(function () {
			   setTimeout(uploadbartemizle(),3000 );
			});
		}
	});
});
	
	
</script>
@stop