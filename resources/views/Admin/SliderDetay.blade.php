@extends('Admin.Layout.Master')

@section('title', 'Slider Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Dil</th>
					<th style="width: 30px;text-align:center;">:</th>
						<?php
						    $dil = \App\Http\Models\Dil::find($veri->DilId);
						?>
					<td>{{$dil->UzunAd}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Resim Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Slug</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Slug}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Slider</th>
					<th style="width: 30px;text-align:center;">:</th>
                    <td><img width="200" height="200" src="{{url('/')}}/images/uploads/Slider/{{$veri->Resim}}" /></td>
				</tr>
               
                <tr>
					<th style="width:200px;">İçerik</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Icerik}}</td>
				</tr>
				 <tr>
					<th style="width:200px;">İçerik2</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Icerik2}}</td>
				</tr>
				 <tr>
					<th style="width:200px;">İçerik3</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Icerik3}}</td>
				</tr>
                 <tr>
					<th style="width:200px;">Link Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->LinkAdi}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Başlangıç Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->baslangic}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Bitiş Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->bitis}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ @implode(', ',json_decode($veri->MetaTag)) }}</td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTitle}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaDescription}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop