@extends('Admin.Layout.Form')
@section('baslik'){{$Title}}@endsection
@section('form')
<input type="hidden" name="id" value="{{@$veri->id}}">
<div class="form-group">
    <label for="KisaAd" class="col-sm-2 control-label">Dili Eklenecek Ülkeyi Seçin</label>
    <div class="col-sm-10">
       <select class="form-control" id="DilEkle" required name="DilEkle">
           <option data-bayrak="flag.png">Lütfen Dil Ekleyiniz</option>
            @foreach(Fnk::Ulkeler() as $key => $ulke)
                <option data-bayrak="{{ @$key }}.svg" @if(@$veri->KisaAd == $key ) selected @endif value="{{ $key }}">{{ $ulke }} </option>
            @endforeach
        </select>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {

        function formatState (state) {
            if (!state.id) { return state.text; }
            var $state = $(
                '<span><img src="{{ url('/flags/flags') }}/'+$(state.element).data('bayrak')+ '" class="img-flag" style="height:30px ; border-radius: 100%; " /> ' + state.text +' - '+ state.element.value +'</span>'
            );
            return $state;
        };


        $('#DilEkle').select2({
             templateResult: formatState
        });
        
    });
</script>

@stop