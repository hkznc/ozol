@extends('Admin.Layout.Master')

@section('title', 'Dil Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Dil Kısa Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->KisaAd}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Dil Uzun Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->UzunAd}}</td>
				</tr>
                                <tr>
					<th style="width:200px;">Resim</th>
					<th style="width: 30px;text-align:center;">:</th>
                                        <td><img src="{{url('')}}/flags/flags/{{$veri->Resim}}" style="width: 40px;border-radius: 100%" /></td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop