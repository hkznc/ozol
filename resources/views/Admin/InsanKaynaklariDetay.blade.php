@extends('Admin.Layout.Master')


@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div id="printableArea" class="panel-body">
            <table  class="table table-striped table-bordered">
               
                <tr>
                    <th style="width:200px;">Resim</th>
                    <td>
                        @if(!empty($veri->Fotograf))
                        <img width='150' src="{{url('images/uploads/ik/fotograf/'.$veri->Fotograf)}}" style="max-width: 100%;"></img>
                        @endif
                </td>
                </tr>
                <tr>
                    <th style="width:200px;">Belgeler</th>
                    <td>
                        @if(!empty($veri->Belge))
                        @foreach(json_decode($veri->Belge) as $Belge)
                        <a href="{{url('images/uploads/ik/belge/'.$Belge)}}">{{$Belge}}</a><br>
                        @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th style="width:200px;">Ad Soyad</th>
                    <td>{!!$veri->Adi.' '.$veri->Soyadi!!}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Doğum Yeri </th>
                    <td>{!!$veri->DogumYeriIl!!}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Doğum Tarihi</th>
                    <td>{!!$veri->DogumTarihi!!}</td>
                </tr>

                <tr>
                    <th style="width:200px;">Cinsiyet</th>
                    <td>{{$veri->Cinsiyet}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Ev Telefonu</th>
                    <td>{{$veri->TelefonEv}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Cep Telefonu</th>
                    <td>{{$veri->TelefonCep}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Askerlik Durumu</th>
                    <td>{{$veri->AskerlikDurumu}}</td>
                </tr>

                <tr>
                    <th style="width:200px;">TC Kimlik Numarası</th>
                    <td>{{$veri->TcNo}}</td>
                </tr>

                <tr>
                    <th style="width:200px;">Adresi</th>
                    <td>{{$veri->Adres}}</td>
                </tr>
  
                <tr>
                    <th style="width:200px;">Eğitim Seviyesi</th>
                    <td>{{$veri->EgitimSeviyesi}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Mezun Olduğu Okul ve Bölümü</th>
                    <td>{{$veri->YuksekOgretim}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Yabancı Dil</th>
                    <td>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th style="width:150px;">Dil</th>
                                <th style="width:50px;"> Seviyesi</th>

                            </tr>
                            @if(!empty(json_decode($veri->YabanciDiller)))
                            @foreach(json_decode($veri->YabanciDiller) as $x)
                            <tr>
                                <td>{{$x->Adi}}</td>
                                <td>{{$x->Derecesi}}</td>
                            </tr>
                            @endforeach
                            @endif

                        </table>
                    </td>
                </tr>
                <tr>
                    <th style="width:200px;">Bilgisayar Kullanma ve Programlar</th>
                    <td>{{$veri->BilgisayarKullanma}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Ehliyet</th>
                    <td>{{$veri->EhliyetTipi}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Bu İşle İlgili Kullandığınız Araç ve Cihazlar</th>
                    <td>{{$veri->IsleIlgiliKullanilanAraclar}}</td>
                </tr>

             
                <tr>
                    <th style="width:200px;">Referanslar</th>
                    <td>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th style="width:150px;">Adı Soyadı:</th>
                                <th style="width:150px;"> Telefon numarası:</th>

                            </tr>
                            @if(!empty(json_decode($veri->Referanslar)))
                            @foreach(json_decode($veri->Referanslar) as $x)
                            <tr>
                                <td>{{$x->AdiSoyadi}}</td>
                                <td>{{$x->Telefon}}</td>
                            </tr>
                            @endforeach
                            @endif

                        </table>
                    </td>
                </tr>
                <tr>
                    <th style="width:200px;">İş Başvurusu Yapılan Görev</th>
                    <td>{{$veri->BasvuruYapilanPozisyon}}</td>
                </tr>

                <tr>
                    <th style="width:200px;">Oluşturulma Tarihi</th>
                    <td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
                </tr>
                <tr>
                    <th style="width:200px;">Güncelleme Tarihi</th>
                    <td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
                </tr>
                <tr>
                    
                    <td  colspan="3">
                        <div class="pull-right">
                        <a onclick="PrintElem('#printableArea')" class="btn btn-primary ">Yazdır</a>
                        <a href="{{URL::previous()}}" class="btn btn-primary ">Geri Dön</a>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@stop

@section('js')
<script>
function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title>my div</title>');
        mywindow.document.write('<style>table, td, th {text-align: left; border: 1px solid black;}table {border-collapse: collapse;width: 100%;}th { height: 20px;}</style>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        //mywindow.document.write('</body></html>');

        mywindow.print();
       // mywindow.close();

        return true;
    }
</script>
@endsection