@extends('Admin.Layout.Form')
@section('baslik','OnePage Menu')
@section('form')


<input type="hidden" name="id" value="{{@$veri->id}}">
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">OnePage Menü Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Menü Adı">
    </div>
</div>
<div class="form-group">
    <label for="Tip" class="col-sm-2 control-label">OnePage Tipi</label>
    <div class="col-sm-10">
        <select class="form-control" id="Tip" required name="Tip" onchange="TipDegistir()">
            <option value="baslangic">Baslangic</option>
            <option value="klinik">Klinik</option>
            <option value="doktor">Doktor</option>
            <option value="yazılar">Yazılar</option>
            <option value="iletisim">İletişim</option>
        </select>
   </div>
</div>
<div class="form-group baslangic" style="display: none">
    <div class=" alert alert-danger " role="alert" style="margin-left:18%;width: 80%!important">
        Baslangic alanındaki slider resimleri slider yönetiminden eklenir altındaki alanları doldurunuz.
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Kutucuk 1 İsmi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="kutucuk1" value="{{ @$veri->kutucuk1 }}">
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Kutucuk 2 İsmi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="kutucuk2" value="{{ @$veri->kutucuk2 }}">
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Kutucuk 3 İsmi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="kutucuk3" value="{{ @$veri->kutucuk3 }}">
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Kutucuk 3 AltYazılar</label>
    <div class="col-sm-10">
        <textarea type="text" class="ck" name="kutucuk3altyazi" >{{ @$veri->kutucuk3altyazi }}</textarea>
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Kutucuk 4 İsmi</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="kutucuk4" value="{{ @$veri->kutucuk4 }}">
    </div>
</div>

<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Doktor Resmi</label>
    <div class="col-sm-10">
       <img src="{{url('images/uploads/'.@$veri->Resim)}}" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
      <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Doktor Resmi Link Verme</label>
    <div class="col-sm-10">
       <select name="link" class="form-control">
           @foreach($menuler as $menu)
                <option value="{{ $menu->Slug }}">{{ $menu->Adi }}</option>
           @endforeach
       </select>
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">Başlık</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="baslangic_baslik" value="{{ @$veri->baslangic_baslik }}">
    </div>
</div>
<div class="form-group baslangic" style="display: none">
    <label class="col-sm-2 control-label">İçerik</label>
    <div class="col-sm-10">
        <textarea type="text" class="ck" name="baslangic_icerik" >{{ @$veri->baslangic_icerik }}</textarea>
    </div>
</div>
<div class="form-group doktor" style="display: none">
    <label class="col-sm-2 control-label">Doktor Resmi</label>
    <div class="col-sm-10">
       <img src="{{url('images/uploads/'.@$veri->Resim)}}" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
      <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
<div class="form-group doktor" style="display: none">
    <label class="col-sm-2 control-label">Doktor İsim</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="doktor_isim" value="{{ @$veri->doktor_isim }}">
    </div>
</div>
<div class="form-group doktor" style="display: none">
    <label class="col-sm-2 control-label">Doktor Resmi Link Verme</label>
    <div class="col-sm-10">
       <select name="link" class="form-control">
           @foreach($menuler as $menu)
                <option value="{{ $menu->Slug }}">{{ $menu->Adi }}</option>
           @endforeach
       </select>
    </div>
</div>

<div class="form-group doktor" style="display: none">
    <label class="col-sm-2 control-label">Ünvan 1</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="unvan1" value="{{ @$veri->unvan1 }}">
    </div>
</div>
<div class="form-group doktor" style="display: none">
    <label class="col-sm-2 control-label">Ünvan 2</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="unvan2" value="{{ @$veri->unvan2 }}">
    </div>
</div>
<div class="form-group doktor" style="display: none">
    <label class="col-sm-2 control-label">Paragraf 1</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="paragraf1" value="{{ @$veri->paragraf1 }}">
    </div>
</div>
<div class="form-group doktor" style="display: none">
    <label class="col-sm-2 control-label">Paragraf 2</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="paragraf2" value="{{ @$veri->paragraf2 }}">
    </div>
</div>

<div class="form-group klinikx" style="display: none">
    <label class="col-sm-2 control-label">Klinik Arkaplan</label>
    <div class="col-sm-10">
       <img src="{{url('images/uploads/'.@$veri->klinik_arkaplan)}}" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
      <input type="file" class="form-control" name="klinik_arkaplan" id="klinik_arkaplan" placeholder="Resim">
    </div>
</div>
<div class="form-group klinik" style="display: none">
@if(!empty(@$veri->klinik_baslik) || !empty(@$veri->klinik_icerik))
    @foreach(json_decode(@$veri->klinik_baslik) as $key => $value)
        <span>    
            <label class="col-sm-2 control-label">Baslik {{ $key + 1 }} ,İçerik  {{ $key + 1 }}</label>
            <div class="col-sm-1">
            <input class="form-control icp icp-auto" value="{{ json_decode(@$veri->klinik_icons)[$key] }}" type="text" name="klinik_icons[]"/>
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="klinik_baslik[]" value="{{ $value }}">
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="klinik_icerik[]" value="{{ json_decode(@$veri->klinik_icerik)[$key] }}">
            </div>
            <div class="col-sm-1">
            @if($key==0)
                <a onclick="klinikEkle()"><i class="fa fa-plus fa-2x" ></i></a>
            @else
                <a onclick="klinikCikar(this)"><i class="fa fa-minus fa-2x" ></i></a>
            @endif
            </div>
        </span>  
    @endforeach
@else
<span>
    <label class="col-sm-2 control-label">Baslik ,İçerik</label>
    <div class="col-sm-1">
    <input class="form-control icp icp-auto" value="fa-anchor" type="text" name="klinik_icons[]"/>
    </div>
    <div class="col-sm-4">
        <input type="text" class="form-control" name="klinik_baslik[]" value="">
    </div>
    <div class="col-sm-4">
        <input type="text" class="form-control" name="klinik_icerik[]" value="">
    </div>
    <div class="col-sm-1">
        <a onclick="klinikEkle()"><i class="fa fa-plus fa-2x" ></i></a>
    </div> 
</span>  
@endif
</div>




<div class="form-group iletisim" style="display: none">
    <div class=" alert alert-danger " role="alert" style="margin-left:18%;width: 80%!important">
        İletişim alanı eklendikten sonra şube eklenmiş olmalıdır.
    </div>
</div>
@stop

@section('js')
<script>
jQuery(document).ready(function($) {
    //Veritabanında Tip var ise başlangıçta getir onu
    var tipVeritabani = "{{ @$veri->Tip }}";
    $('#Tip').val(tipVeritabani).change();
});
    function TipDegistir(){
        var Tip = $('#Tip').val();
        if (Tip == "doktor") {
            $('.doktor').show("slow");
            $('.klinik').hide("slow");
            $('.klinikx').hide("slow");
            $('.iletisim').hide("slow");
            $('.baslangic').hide("slow");
        }
        else if(Tip == "klinik"){
            $('.klinik').show("slow");
            $('.klinikx').show("slow");
            $('.doktor').hide("slow");
            $('.iletisim').hide("slow");
            $('.baslangic').hide("slow");
        }
        else if(Tip == "iletisim"){
            $('.iletisim').show("slow");
            $('.doktor').hide("slow");
            $('.klinik').hide("slow");
            $('.klinikx').hide("slow");
            $('.baslangic').hide("slow");
        }
        else if(Tip == "baslangic"){
            $('.baslangic').show("slow");
            $('.doktor').hide("slow");
            $('.klinik').hide("slow");
            $('.klinikx').hide("slow");
            $('.iletisim').hide("slow");
        }
        else{
            $('.doktor').hide("slow");
            $('.klinik').hide("slow");
            $('.klinikx').hide("slow");
            $('.iletisim').hide("slow");
            $('.baslangic').hide("slow");
        }
    }

    function klinikEkle(){
        var html =  '<span>'+
                    '<label class="col-sm-2 control-label">Baslik,İçerik</label>'+
                    '<div class="col-sm-1">'+
                        '<input class="form-control icp icp-auto" value="fa-anchor" type="text" name="klinik_icons[]">'+
                    '</div>'+
                    '<div class="col-sm-4">'+
                        '<input type="text" class="form-control" name="klinik_baslik[]" value="">'+
                    '</div>'+
                    '<div class="col-sm-4">'+
                        '<input type="text" class="form-control" name="klinik_icerik[]" value="">'+
                    '</div>'+
                    '<div class="col-sm-1">'+
                        '<a onclick="klinikCikar(this)"><i class="fa fa-minus fa-2x" ></i></a>'+
                    '</div></span>';
        $('.klinik').append(html);
        $('.icp-auto').iconpicker({
            templates: {
                search : '<input type="search" class="form-control iconpicker-search" placeholder="icon ismini giriniz . . ." />',
            }
        });
    }
    function klinikCikar(element){
        $(element).closest('span').remove();
    }
</script>
@stop

