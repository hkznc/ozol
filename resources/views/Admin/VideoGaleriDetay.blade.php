@extends('Admin.Layout.Master')

@section('title', 'Video Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Video Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi}}</td>
				</tr>
                <tr>
                    <th style="width:200px;">Kapak Fotoğrafı</th>
                    <th style="width: 30px;text-align:center;">:</th>
                    <td><img width='150' height='150' src="{{url('images/uploads/FotoGaleri/kapak/'.$veri->KapakFotografi)}}" style="max-width: 100%;"></td>
                </tr>
				<tr>
					<th style="width:200px;">Video</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><iframe width="560" height="315" src="https://www.youtube.com/embed/{{$veri->Link}}" frameborder="0" allowfullscreen></iframe></td>
				</tr>
				
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
                                <?php
                                    $ekleyen = \App\Http\Models\User::find($veri->ekleyen);
                                    $guncelleyen = \App\Http\Models\User::find($veri->songuncelleyen);
                                ?>
                                <tr>
					<th style="width:200px;">Ekleyen</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$ekleyen->name or '-'}}</td>
				</tr>
                                <tr>
					<th style="width:200px;">Son Güncelleyen</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$guncelleyen->name or '-'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop