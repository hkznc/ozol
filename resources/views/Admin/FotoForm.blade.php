@extends('Admin.Layout.Form')
@section('baslik')Fotograf @endsection
@section('form')
<input type="hidden" name="trid" value="{{@$trveri->id}}">
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Foto Galeri Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" disabled value="{{$fotogaleri->Adi}}" id="Adi">
    </div>
</div>
<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resimler</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim[]" id="Resim" placeholder="Resim" multiple>
    </div>
</div>    
@stop

@section('js')

@stop