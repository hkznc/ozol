@extends('Admin.Layout.Form')

@section('baslik', 'Bayilik Bilgileri')

@section('form')
		<div class="form-group">

			<label class="col-md-2 control-label">Bayilik Faliyet Alanı</label>

		    <div class="col-md-10">
		        <select id="bayilik_faliyet_alani" name="bayilik_faliyet_alani" class="form-control" required>
		            <option >Seçiniz</option>
		            <option value="1" @if(@$veri->bayilik_faliyet_alani == 1) selected @endif >Yurtiçi</option>
		            <option value="0" @if(@$veri->bayilik_faliyet_alani == 0) selected @endif >Yurtdışı</option>
		        </select>
		    </div>
		</div>

		<div class="form-group">
			<label class="col-md-2 control-label">Koordinat</label>
			<div class="col-md-10">
				 <div class="checkbox">
					<label>
					  <input type="checkbox" value=1 id="kordinat_kontrol" name="kordinat_kontrol"> Koordinatları Kendim Girmek İstiyorum
					</label>
				  </div>
			</div>
		</div>
		<div class="form-group" style="display:none " id="e" >
			<label class="col-md-2 control-label">Enlem Değeri</label>
			<div class="col-md-10">
				<input class="form-control"  type="text" name="enlem" value="{{@explode(',', $veri->Kordinat)[0]}}"  />
			</div>
		</div>

		<div class="form-group" style="display:none" id="b">
			<label class="col-md-2 control-label">Boylam Değeri</label>
			<div class="col-md-10">
				<input class="form-control"  type="text" name="boylam" value="{{@explode(',', $veri->Kordinat)[1]}}"  />
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-2 control-label">Bayilik Adı</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="BayilikAdi" value="{{@$veri->SubeAdi}}" required />
			</div>
		</div>
		<div class="form-group" >
			<label class="col-md-2 control-label">Adres</label>
			<div class="col-md-10">
				<input type="text" class="form-control" id="HaritaAdres" name="Adres" value="{{@$veri->Adres}}" required>
				<input type="hidden" id="Kordinat" class="form-control" name="Kordinat" value="{{@$veri->Kordinat}}">
				 <div class="checkbox" id="a">
					<label>
					  <input type="checkbox" checked id="adresguncelle"> Adresim Haritadan Güncellensin
					</label>
				  </div>
			</div>
		</div>

		
		
		
		<div class="form-group" id="h">
			<label class="col-md-2 control-label">Harita</label>
			<div class="col-md-10">
				<div id="mapmain">
					<div style="float:left; width:100%; height:300px; clear:both; border:solid 1px #E2DFDA;" id="map"></div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Telefon</label>
			<div class="col-md-10">
				<input class="form-control"  type="text" name="Telefon" value="{{@$veri->Telefon}}" required />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Fax</label>
			<div class="col-md-10">
				<input class="form-control"  type="text" name="Fax" value="{{@$veri->Fax}}" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">E-Posta</label>
			<div class="col-md-10">
				<input class="form-control" type="email" name="Eposta" value="{{@$veri->Eposta}}" />
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-2 control-label">Web Adresi</label>
			<div class="col-md-10">
				<input class="form-control" type="text" name="Web" value="{{@$veri->Web}}" />
			</div>
		</div>
@endsection

@section('js')
	<script src='{{url('plugins/jquery.inputmask/dist/inputmask/inputmask.js')}}'></script>
	<script src='{{url('plugins/jquery.inputmask/dist/inputmask/jquery.inputmask.js')}}'></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBq7hs_cVZsxgoD45l-0gubEDYJD5--doA&sensor=false&language=tr"></script>
	


	<script>
	$(document).ready(function(){
		
		$(":input").inputmask();
$("#HaritaAdres").change(function(){
			$.ajax({url: "https://maps.googleapis.com/maps/api/geocode/json?address="+$("#HaritaAdres").val()+"&key=AIzaSyBq7hs_cVZsxgoD45l-0gubEDYJD5--doA", success: function(result){
				var co=new google.maps.LatLng(result.results[0].geometry.location.lat,result.results[0].geometry.location.lng);
				$('#Kordinat').val(co);
				showMap(co);
			}});
		});
		var geocoder = new google.maps.Geocoder();
		
		@if(isset($veri))
			var latLng = new google.maps.LatLng({{$veri->Kordinat}});
		@else
			var latLng = new google.maps.LatLng(39.91365499655554,32.855811906047165);		
		@endif
		showMap(latLng);
		varImageID = 0;
		
		function showMap(result){
			if(result){
				latLng=result;
			}
			$('#mapmain').show();
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 10,
				center: latLng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			var image = '{{url("img/pin.png")}}';
			var marker = new google.maps.Marker({
				position: latLng,
				icon: image,
				map: map
			});
			google.maps.event.addListener(map, 'mouseup', function(event) {
				geocoder.geocode({
					latLng: event.latLng
				}, function(responses) {
					
					marker.setPosition(event.latLng);
					if (responses && responses.length > 0) {
						if($('#adresguncelle').is( ":checked" )){
							$('#HaritaAdres').val(responses[0].formatted_address);						
						}
						$('#Kordinat').val(event.latLng);
					} 
				});
			});
		}
		showMap(0);

$('#kordinat_kontrol').bind('change', function () {

   	if ($(this).is(':checked')){
     	$('#e').show('slow');
		$('#b').show('slow');
		$('#h').hide('slow');
		$('#a').hide('slow');
	}		
   	else{
   		$('#e').hide('slow');
		$('#b').hide('slow');
		$('#h').show('slow');
		$('#a').show('slow');
	}
});	
		
		
	});
	</script>
@endsection