@extends('Admin.Layout.Master')

@section('title', 'Ürün Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				
				<tr>
					<th style="width:200px;">Tip</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>
						@if( $veri->Tip == "I") Kurucu İçerik
						@elseif($veri->Tip == "K") Kurucu 
						@elseif($veri->Tip == "P") Personel
						@elseif($veri->Tip == "M") Müdür
						@endif
					</td>
				</tr>
				
				<tr>
					<th style="width:200px;">Adı Soyadi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi_soyadi}}</td>
				</tr>

				@if($veri->Tip != "I")
				<tr>
					<th style="width:200px;">Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Telefon}}</td>
				</tr>
				<tr>
					<th style="width:200px;">E-Mail</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Email}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Fotoğraf</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td><img width='150' height='150' src="{{url('images/uploads/ekibimiz/'.$veri->Fotograf)}}" style="max-width: 100%;"></td>
				</tr>
				<tr>
					<th style="width:200px;">Pozisyon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Pozisyon}}</td>
				</tr>
				@endif

				@if($veri->Tip == "I")
				<tr>
					<th style="width:200px;">İçerik</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{!!$veri->Icerik!!}</td>
				</tr>
				<tr>
					<th style="width:200px;">Meta Keywords</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTag}}</td>
				</tr>
                
                <tr>
					<th style="width:200px;">Meta Title</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaTitle}}</td>
				</tr>
                
                <tr>
					<th style="width:200px;">Meta Description</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MetaDescription}}</td>
				</tr>
				@endif
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop