@extends('Admin.Layout.Form')

@section('baslik', 'Ürün')

<?php 
use App\Http\Models\Menu as MN; 
$menu = MN::where('is_active',1)->where('MenuTipi', 'modul')->where('Link', 'LIKE','/tr/urunler%')->get(); 
 
 ?> 

@section('form')
<div class="form-group">
    <label for="Urun_UstKat" class="col-sm-2 control-label">Üst Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_UstKat" required name="Urun_UstKat">
            <option value="">Üst Kategori Seçiniz</option>
            @foreach(@$menu as $td)
                <option value="{{$td->id}}" @if(@$td->id == @$veri->Urun_UstKat) selected @endif>{{$td->Adi}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Ürün Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Ürün Adı">
    </div>
</div>

<div class="form-group">
    <label for="KategoriId" class="col-sm-2 control-label">Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="KategoriId"  required name="KategoriId[]">
            <option value="">Kategori Seçiniz</option>
            @foreach (@$kategoriler as $kategori)
                <option value="{{$kategori['id']}}"@if( \App\Http\Models\UrunKategori::where('urun_id', @$veri->id)->where('kategori_id', $kategori['id'])->first() ) selected @endif>
                    @for ($i = 0; $i < $kategori["derinlik"]; $i++)
                    -
                    @endfor
                    {{$kategori["adi"]}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group">
    <label class="col-md-2 control-label">Ana Kategori Durumu </label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="UrunGizli" name="UrunGizli"  @if(@$veri->UrunGizli == 1) checked @endif >Üst Kategoride Gösterilmesin
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Resim</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="Resim[]" id="Resim" multiple placeholder="Resim">
    </div>
    @if( @$veri->Resim )
        <div class="col-md-10 col-md-offset-2 container-fluid">
            <div class="checkbox">
            @foreach(json_decode(@$veri->Resim) as $key => $value )
                <a>
                    <img class="img" src="{{ url('images/uploads/'.$value) }}" width="150" height="150" alt="">
                    <input type="checkbox" name="secili_belge_resim_kapak[]"   @if( @$value ) checked value="{{$value}}" @else value="0" @endif>
                </a>
            @endforeach
            </div>
        </div>
    @endif
</div>

 
<div class="form-group">
    <label for="urun_gosterme_sekli" class="col-sm-2 control-label">Ürün Gösterimi</label>
    <div class="col-sm-10">
        <select class="form-control" id="urun_gosterme_sekli"  onchange="UrunGosterme()" required name="Gosterim">
            <option value="">Ürün Gösterme Tipini Seçin</option>
            
                <option  value="1" @if( @$veri->Gosterim == 1 ) selected @endif>Katolog Resimleri</option>
                <option value="2" @if( @$veri->Gosterim == 2 ) selected @endif>Ürün Özellikleri</option>
                <option value="3" @if (@$veri->Gosterim == 3 ) selected  @endif>Alt Kategori Ekleme</option>
            
        </select>
    </div>
</div>

<div class="form-group" id="Katalog" @unless( @$veri->Gosterim == 1 ) style="display: none" @endunless>
    <label for="Resim2" class="col-sm-2 control-label">Katalog Resimleri</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="Resim2[]" id="Resim2"  multiple >
    </div>
    @if( @$veri->Resim2 )
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                @foreach(json_decode(@$veri->Resim2)  as $key=>$value)
                <a>
                    <img class="img" src="{{ url('images/uploads/'.$value ) }}" width="150" height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim[]"   @if( @$value ) checked value="{{$value}}" @else value="0" @endif>
                </a>
                @endforeach
            </div>
        </div>
    @endif
</div>
<div class="form-group" id="AltKategori"  @unless( @$veri->Gosterim == 3 )style="display:none"  @endunless >
    <label for="Urun_UstKat" class="col-sm-2 control-label">Alt Kategori</label>
    <div class="col-sm-10">
        <select class="form-control" id="Urun_AltKat" required name="Urun_AltKat">
            <option value="">Alt Kategori Seçiniz</option>
            @foreach (@$kategoriler as $kategori)
                <option value="{{$kategori["slug"]}}" @if( $kategori["slug"]==@$veri->Urun_AltKat ) selected @endif>
                    @for ($i = 0; $i < $kategori["derinlik"]; $i++)
                    -
                    @endfor
                    {{$kategori["adi"]}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group" id="UrunOzellikleriBoyut"  @unless( @$veri->Gosterim == 2 )style="display:none"  @endunless >
    <label for="Icerik" class="col-sm-2 control-label">Boyut</label>
    <div class="col-sm-10">
        <input type="text" name="Icerik" class="form-control" value="{{ @$veri->Icerik }}"></input>
    </div>
</div>
<div class="form-group" id="UrunOzellikleriAna" @unless( @$veri->Gosterim == 2 ) style="display:none" @endunless  >
    <label for="Icerik2" class="col-sm-2 control-label">Ana Maddesi</label>
    <div class="col-sm-10">
        <input type="text" name="Icerik2" class="form-control" value="{{ @$veri->Icerik2 }}"></input>
    </div>
</div>
<div class="form-group" id="UrunOzellikleriRenk"  @unless( @$veri->Gosterim == 2 )style="display: none" @endunless>
    <label for="Resim3" class="col-sm-2 control-label">Ürün Farklı Renkleri</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="Resim3[]" id="Resim3"  multiple >
    </div>
    @if( @$veri->Resim3 )
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                @foreach(json_decode(@$veri->Resim3)  as $key=>$value)
                <a>
                    <img class="img" src="{{ url('images/uploads/'.$value ) }}" width="150" height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim2[]"   @if( @$value ) checked value="{{$value}}" @else value="0" @endif>
                </a>
                @endforeach
            </div>
        </div>
    @endif
</div>





@stop

@section('js')
<script type="text/javascript">
    $(document).ready(function() {
        $('select>option:eq(1)').attr('selected', true);
        $('.gizli').hide();

         

    });
    function UrunGosterme() {
        var tip = $('#urun_gosterme_sekli').val();

        if (tip == "1") {//Katalog resimleri
            $('#Katalog').show('slow');
            $('#UrunOzellikleriBoyut').hide('slow');
            $('#UrunOzellikleriAna').hide('slow');           
            $('#UrunOzellikleriRenk').hide('slow');
            $('#AltKategori').hide('slow');

        } 
        else if (tip == "2") {          //Ürün Özellikleri
            $('#Katalog').hide('slow');
            $('#UrunOzellikleriBoyut').show('slow');
            $('#UrunOzellikleriAna').show('slow');           
            $('#UrunOzellikleriRenk').show('slow');
            $('#AltKategori').hide('slow');



        }else{
            $('#AltKategori').show('slow');
            $('#Katalog').hide('slow');
            $('#UrunOzellikleriBoyut').hide('slow');
            $('#UrunOzellikleriAna').hide('slow');           
            $('#UrunOzellikleriRenk').hide('slow');
        }
       
        
    }

</script>
@stop
