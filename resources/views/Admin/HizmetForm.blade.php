@extends('Admin.Layout.Form')

@section('baslik', 'Hizmet')

@section('form')

<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Hizmet Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Hizmet Adı">
    </div>
</div>

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Anasayfa Küçük Resim</label>
    <div class="col-sm-10">
    @if( @$veri->Resim )
        <img src="{{url('images/uploads/Hizmetler/'.$veri->Resim)}}" style="max-height: 70px;margin-bottom: 10px;" id="anlikresim" class="img-rounded" />
        @endif
      <input type="file" class="form-control" name="Resim" id="Resim" placeholder="Resim">
    </div>
</div>
{{-- <div class="form-group">
    
    @if( @$veri->UygulamaResim )
        <div class="col-md-10 col-md-offset-2 container container-fluid" >
            <div class="checkbox">
                @foreach( json_decode(@$veri->UygulamaResim)  as $key=>$value)
                <a>
                    <img  src="{{ url('images/uploads/Hizmetler/'.$value ) }}"  height="150" alt="">
                    <input type="checkbox"  name="secili_belge_resim_UygulamaResim[]"   @if( @$value ) checked value="{{$value}}" @else value="0" @endif>
                </a>
                @endforeach
            </div>
        </div>
    @endif
    <label for="HaberResim" class="col-sm-2 control-label">Hizmet Detay Resimleri</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" name="UygulamaResim[]" id="UygulamaResim"  placeholder="Kapak Resim" multiple>
    </div>
</div> --}}
<div class="form-group">
    <label  class="col-sm-2 control-label">Hizmet Kısa İçeriği</label>
    <div class="col-sm-10">
        <input type="text" name="KisaIcerik" class="form-control" value="{{ @$veri->KisaIcerik}}" ></input>
    </div>
</div>
<div class="form-group">
    <label  class="col-sm-2 control-label">Hizmet İçeriği</label>
    <div class="col-sm-10">
        <textarea name="Icerik" class="ck" >{{ @$veri->Icerik}}</textarea>
    </div>
</div>

@stop
@section('js')
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $('#MetaTag').select2({
            tags: true,
            multiple: true,
            tokenSeparators: [',',' '],
            
        });
        

        
    });
</script>
@stop