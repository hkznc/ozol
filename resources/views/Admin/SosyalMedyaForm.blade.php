@extends('Admin.Layout.Form')

@section('baslik', 'Sosyal Medya')

@section('form')
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Sosyal Medya Seçiniz</label>
    <div class="col-sm-10">
    <select name="Adi" class="form-control" id="Adi" required>
        <option value="">Sosyal Medya Seçiniz</option>
        <option value="facebook" @if(@$veri->Adi=='facebook') selected @endif>Facebook</option>
        <option value="twitter" @if(@$veri->Adi=='twitter') selected @endif>Twitter</option>
        <option value="youtube-play" @if(@$veri->Adi=='youtube') selected @endif>Youtube</option>
        <option value="google" @if(@$veri->Adi=='google') selected @endif>Google+</option>
        <option value="instagram" @if(@$veri->Adi=='instagram') selected @endif>İnstagram</option>
        <option value="linkedin" @if(@$veri->Adi=='linkedin') selected @endif>Linkedin</option>
        <option value="foursquare" @if(@$veri->Adi=='foursquare') selected @endif>Foursquare</option>
    </select>
    </div>
</div>
<div class="form-group">
    <label for="Link" class="col-sm-2 control-label">Link</label>
    <div class="col-sm-10">
        <input type="text" class="form-control"  value="{{@$veri->Link}}" name="Link" placeholder=" Linki">
    </div>
</div>
<div class="form-group">
    <label for="LinkAcilisTipi" class="col-sm-2 control-label">Link Açılış Tipi</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="LinkAcilisTipi" value="" @if(isset($veri) && $veri->LinkAcilisTipi=='') checked @endif @if(!isset($veri)) checked @endif >Aynı Pencere</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="LinkAcilisTipi" @if(isset($veri) && $veri->LinkAcilisTipi=='target="_blank"') checked @endif value='target="_blank"'>Yeni Pencere</label>
        </div>
    </div>
</div>
@stop