@extends('Admin.Layout.Master')

@section('title', 'Operatör Detay')

@section('content')
<div class="col-lg-12">
	<div class="panel panel-default">
		<div class="panel-heading">@yield('title')</div>
		<div class="panel-body">
			<table class="table table-striped table-bordered">
				<tr>
					<th style="width:200px;">Operatör Adı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Adi}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Makine Marka</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->MakineMarka}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Telefon</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>+9{{$veri->Telefon}}</td>
				</tr>
				<tr>
					<th style="width:200px;">E-Posta</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Eposta}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Adres</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->IkametAdresi}}</td>
				</tr>

				<tr>
					<th style="width:200px;">Çalışma Alanı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{ ($veri->yer_alti_mi==0)?'Yeraltı Makine':'Yerüstü Makine' }}</td>
				</tr>

				<tr>
					<th style="width:200px;">Şuan Çalıştığı Firma</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->SimdikiFirma}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Önceki Çalıştığı Firma</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->OncekiFirma}}</td>
				</tr>
				
				<tr>
					<th style="width:200px;">Operatör İş İlanı</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->CalismaDurumu}}</td>
				</tr>
				

                <tr>
					<th style="width:200px;">Beden</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->Beden}}</td>
				</tr>
                <tr>
					<th style="width:200px;">Yedek Parça Talep</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{$veri->YedekParcaTalep}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Durum</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{($veri->is_active==1)?'Aktif':'Pasif'}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Oluşturulma Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->created_at)}}</td>
				</tr>
				<tr>
					<th style="width:200px;">Güncelleme Tarihi</th>
					<th style="width: 30px;text-align:center;">:</th>
					<td>{{Fnk::TarihDuzenle($veri->updated_at)}}</td>
				</tr>
				<tr>
					<td colspan="3"><a href="{{URL::previous()}}" class="btn btn-primary pull-right">Geri Dön</a></td>
				</tr>
			</table>
		</div>
	</div>
</div>
@stop