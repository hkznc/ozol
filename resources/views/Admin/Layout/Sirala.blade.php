@extends('Admin.Layout.Master')

<?php
	$url = Request::url();
	$link = explode('Sirala',$url);
	$link = $link[0].'Sirala'
?>
@section('title') {{$baslik}} Sıralama @endsection

@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
  <span class="pull-left">@yield('title')</span>
				<select onChange="UstKatDegistir()" id="UstKatId" class="pull-left">
					<option value="0">Ana Kategoriler</option>
					<?php
						Fnk::ParentOption($anakategoriler,$ustkatid);
					?>
				</select>
				
				<div style="clear: both;"></div>
			</div>
  <div class="panel-body">
    <p class="alert alert-info">
		@yield('title') İşlemini Sürükle - Bırak Yaparak Sitenizdeki Görünüm Sırasını Değiştirebilirsiniz.
	</p>
	
	<ul id="sortable" class="list-group">
	@foreach($veri as $v)
			<li class="list-group-item" id="id-{{ $v->id }}">
				@if(isset($v->Resim))
					<img src="{{ url('images/uploads/'.$v->Resim) }}" style="height: 50px;object-fit: contain;display: block;" >
					{{ $v->$vtcolumnname }}
				@else
					{{ $v->$vtcolumnname }}
				@endif	
			</li>
	@endforeach
	
	</ul>
  </div>
</div>
@stop

@section('js')
<script src="{{asset('plugins/jquery-ui/jquery-ui.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#sortable').sortable({
        axis: 'y',
        stop: function (event, ui) {
	        var oData = $(this).sortable('serialize');
	        console.log(oData);
           	$.ajax({
                data: oData+'&_token={{{csrf_token()}}}',
                type: 'POST',
                url: '{{$link}}'
            });
	}
    });
});

 </script>
 <script type="text/javascript">
	function UstKatDegistir(){
		var UstKatId = $('#UstKatId').val();
		window.location = "<?=$link?>/"+UstKatId;
	}
</script>
@stop