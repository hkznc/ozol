@extends('Admin.Layout.Master')

@section('title') {{$Title}} Listesi @stop

<?php
    $url = Request::url();
    $link = str_replace(array('/Listele', '/Bekleyenler'), '', $url);
    use Illuminate\Support\Str;
?>

@section('content')
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">@yield('title')
        @if(Request::segment(2) == "Menu")
        <select id="table-filter" onchange="DilFiltrele(this)" class="form-control"  style=" width: 20%!important; margin-left:5%; display: inline!important;" >
            <option value="">Tüm Diller</option>
            @foreach($tumdiller as $dil)
                <option>{{ $dil->UzunAd }}</option>
            @endforeach
        </select>
        @endif
            <?php
            if ($EkleButonu) {
                echo '<div class="pull-right" style="margin:-5px;">
                        <a href="' . $link . '/Ekle" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Yeni Ekle</a>
                    </div>';
            }

            if ($SiralamaButonu) {
                echo '<div class="pull-right" style="margin-top:-5px;margin-right:10px;">
                        <a href="' . $link . '/Sirala/0" class="btn btn-danger btn-sm"><i class="fa fa-bars"></i> Sırala</a>
                </div>';
            }
            
            if ($GaleriSiralaButonu) {
                echo '<div class="pull-right" style="margin-top:-5px;margin-right:10px;">
                        <a href="' . $link . '/Sirala" class="btn btn-danger btn-sm"><i class="fa fa-bars"></i> Sırala</a>
                </div>';
            }

            if ($TumunuSilButonu) {
                echo '<div class="pull-right" style="margin-top:-5px;margin-right:10px;">
                        <a href="' . $link . '/TumunuSil" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Tümünü Sil</a>
                </div>';
            }
            ?>
        </div>
        <div class="panel-body">
            <div class="dataTable_wrapper">
                <table class="table table-striped table-bordered table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>id</th>
                            <?php
                            foreach ($ListelemeBasliklari as $value) {
                                echo '<th style="min-width:100px">' . $value . '</th>';
                            }
                            ?>
                            <th style="width:250px">İşlem</th>
                        </tr>
                    </thead>       
                        <tfoot>
                            <tr>
                                <th>id</th>
                                <?php
                                foreach ($ListelemeBasliklari as $value) {
                                    echo '<th>' . $value . '</th>';
                                }
                                ?>
                                <th>İşlem</th>
                            </tr>
                        </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@if($SilmeMesaji!='')
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Kapat</span></button>
                <h4 class="modal-title" id="myModalLabel">{{$Title}} Silme İşlemi</h4>
            </div>
            <div class="modal-body">
                Silmek İstediğiniz {{$SilmeMesaji}} Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Vazgeç</button>
                <a id="sillink" href="#" class="btn btn-danger">Sil</a>
            </div>
        </div>
    </div>
</div>
@endif

@stop

@section('css')
<link href="{{asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/datatables-responsive/css/dataTables.responsive.css')}}" rel="stylesheet">
        @stop

        @section('js')
        <script>
                    function uyariveri(id){
                    $('#sillink').attr('href', '{{ $link }}/Sil/' + id);
                            $('#myModal').modal('show');
                    }
        </script>

        @if($KopyalaButonu)
        <script type="text/javascript" src="{{asset('plugins/ZeroClipboard/ZeroClipboard.js')}}"></script>
        <script type="text/javascript">
            function copy(){
                $('.kopyala').each(function(){
                    var clip = new ZeroClipboard(this);
                        clip.on("ready", function() {
                            this.on("aftercopy", function(event) {
                                new PNotify({
                                    title: '{{$Title}}',
                                    text: '{{$Title}} Linki Kopyalama İşleminiz Başarıyla Yapılmıştır.',
                                    type: 'success',
                                    styling: 'bootstrap3'
                                });
                                //  console.log("Copied text to clipboard: " + event.data["text/plain"]);
                            });
                        });
                        clip.on("error", function(event) {
                            new PNotify({
                            title: 'HATA',
                                text: '{{$Title}} Linki Kopyalanamadı.',
                                type: 'error',
                                styling: 'bootstrap3'
                            });
                            $(".demo-area").hide();
                            console.log('error[name="' + event.name + '"]: ' + event.message);
                            ZeroClipboard.destroy();
                        });
                });
            }
        </script>
        @endif

        <script src="{{asset('plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
        <script src="{{asset('plugins/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')}}"></script>

        <script type="text/javascript">
                  
                var datatable = $('#dataTables').DataTable({

                    responsive: true,
                    "columnDefs": [
                        {
                            "targets": [0],
                            "visible": false,
                        }
                    ],
                    "order": [[ {{$ListelemeSirala}}, "desc" ]],
                    "processing": true,
                    "serverSide": true,

                    @if($KopyalaButonu)
                    "fnDrawCallback":function(oSettings) {
                        copy();
                    },
                    @endif
                    "ajax": "{{$link}}/Getir",
                    "language": {
                        "lengthMenu": "Sayfada Gösterilecek Veri Adeti _MENU_",
                        "zeroRecords": "Arama Sonucu Bulunamadı.",
                        "info": "Toplam Kayıt Sayısı : _MAX_ (Gösterilen Sayfa : _PAGE_. Toplam Sayfa : _PAGES_)",
                        "infoEmpty": "Gösterilecek Veri Bulunamadı",
                        "infoFiltered": "(_MAX_ Kayıt Filtrelendi)",
                        "search": "Arama  ",
                        "oPaginate": {
                            "sNext": "İleri",
                            "sPrevious": "Geri"
                        }
                    },
                
                });

  
                if (typeof (copy) === 'function'){
                    copy();
                }

                function DilFiltrele(e) {
                    datatable.data().search($(e).val()).draw();
                }

</script>
@stop