<ul class="nav" id="side-menu">
	<li>
		<a href="{{url('Admin')}}"><i class="fa fa-bar-chart-o fa-fw"></i> Anasayfa</a>
	</li>
	{{-- @if( in_array($admin->role, [0,1]) )
		<li @if(strpos(Request::url(),'InsanKaynaklari')!==false) class="active" @endif >
			<a href="{{url('Admin/InsanKaynaklari/Listele')}}"><i class="fa fa-users"></i> Başvuru Yönetimi</a>
			
		</li>
	@endif --}}
	@if( in_array($admin->role, [0]) )
		<li @if(strpos(Request::url(),'Menu')!==false) class="active" @endif >
			<a href="#"><i class="fa fa-bars fa-fw"></i> Menü Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="{{url('Admin/Menu/Listele')}}"> Menü Listele</a>
				</li>
				<li>
					<a href="{{url('Admin/AltMenu/Listele')}}"> Footer Menü Listele</a>
				</li>

			</ul>
		</li>
		
		<li @if(strpos(Request::url(),'Hizmet')!==false) class="active" @endif >
			<a href="{{url('Admin/Hizmet/Listele')}}"><i class="fa fa-newspaper-o"></i> Hizmetler Yönetimi</a>
		</li>
		{{-- <li @if(strpos(Request::url(),'Mevzuat')!==false) class="active" @endif >
			<a href="{{url('Admin/Mevzuat/Listele')}}"><i class="fa fa-newspaper-o"></i> Mevzuatlar Yönetimi</a>
		</li> --}}
		
		{{-- <li @if(strpos(Request::url(),'Haber')!==false) class="active" @endif >
			<a href="{{url('Admin/Haber/Listele')}}"><i class="fa fa-newspaper-o"></i> Haber Yönetimi</a>
			
		</li> --}}
		{{-- <li @if(strpos(Request::url(),'Bayilik')!==false) class="active" @endif >
			<a href="{{url('Admin/Bayilik/Listele')}}"><i class="fa fa-cubes"></i> Bayilik Yönetimi</a>
			
		</li> --}}
		<li @if(strpos(Request::url(),'Slider')!==false) class="active" @endif >
			<a href="{{url('Admin/Slider/Listele')}}"><i class="fa fa-file-image-o fa-fw"></i> Slider Yönetimi</a>
			
		</li>
		<li @if(strpos(Request::url(),'FotoGaleri')!==false) class="active" @endif >
			<a href="#"><i class="fa fa-camera-retro fa-fw"></i> FotoGaleri Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				
				
				<li>
					<a href="{{url('Admin/FotoGaleriAyar/Listele')}}"> Foto Galeri Sayfa Ayar Listele</a>
				</li>
				
				<li>
					<a href="{{url('Admin/FotoGaleri/Listele')}}"> Foto Galeri Listele</a>
				</li>
			</ul>
		</li>
		<li @if(strpos(Request::url(),'VideoGaleri')!==false) class="active" @endif >
			<a href="#"><i class="fa fa-youtube-play fa-fw"></i> Video Galeri Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				
				
				<li>
					<a href="{{url('Admin/VideoGaleriAyar/Listele')}}"> Video Galeri Sayfa Ayar Listele</a>
				</li>

			
				<li>
					<a href="{{url('Admin/VideoGaleri/Listele')}}"> Video Galeri Listele</a>
				</li>
			</ul>
		</li>
		<li @if(strpos(Request::url(),'SosyalMedya')!==false) class="active" @endif >
			<a href="{{url('Admin/SosyalMedya/Listele')}}"><i class="fa fa-youtube-play fa-fw"></i> Sosyal Medya Yönetimi</a>
		
		</li>
		<li @if(strpos(Request::url(),'Blog')!==false) class="active" @endif >
			<a href="{{url('Admin/Blog/Listele')}}"><i class="fa fa-youtube-play fa-fw"></i> Blog Yönetimi</a>
			
		</li>
		
		{{-- <li @if(strpos(Request::url(),'IletisimBilgileri')!==false || strpos(Request::url(),'IletisimIstekleri')!==false) class="active" @endif >
			<a href="{{url('Admin/Ekibimiz/Listele')}}"><i class="fa fa-users"></i> Ekibimiz Yönetimi</a>
			
		</li> --}}

		<li @if(strpos(Request::url(),'IletisimBilgileri')!==false || strpos(Request::url(),'IletisimIstekleri')!==false) class="active" @endif >
			<a href="#"><i class="fa fa-map-marker fa-fw"></i> İletişim Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="{{url('Admin/IletisimIstekleri/Listele')}}"> İletişim İstekleri</a>
				</li>
				
				<li>
					<a href="{{url('Admin/IletisimBilgileri/Listele')}}"> Şube Listele</a>
				</li>
			</ul>
		</li>
		<li @if(strpos(Request::url(),'Yonetici')!==false || strpos(Request::url(),'Kullanici')!==false) class="active" @endif >
			<a href="{{url('Admin/Yonetici/Listele')}}"><i class="fa fa-users fa-fw"></i> Kullanıcı Yönetimi</a>
			
		</li>
		{{-- @if( in_array($admin->role, [0, 1]) )
		<li @if(strpos(Request::url(),'InsanKaynaklari')!==false || strpos(Request::url(),'InsanKaynaklari')!==false) class="active" @endif >
			<a href="{{url('Admin/InsanKaynaklari/Listele')}}"><i class="glyphicon glyphicon-user fa-fw"></i> Başvuru Formları <span class="fa arrow"></span></a>
			
		</li>
		@endif --}}
               
		{{-- <li @if(strpos(Request::url(),'Popup')!==false || strpos(Request::url(),'Popup')!==false) class="active" @endif >
			<a href="#"><i class="fa fa-comments fa-fw"></i> Popup Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
			
				</li>
				<li>
					<a href="{{url('Admin/Popup/Ekle')}}"> Popup Ayar Ekle</a>
				</li>
				<li>
					<a href="{{url('Admin/Popup/Listele')}}"> Popup Ayar Listele</a>
				</li>
                        
			</ul>
		</li> --}}
		
		<li @if(strpos(Request::url(),'Ayarlar')!==false || strpos(Request::url(),'Kullanici')!==false) class="active" @endif >
			<a href="#"><i class="fa fa-cogs fa-fw"></i> Ayar Yönetimi<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level">
				<li>
					<a href="{{url('Admin/Tawk/Ekle')}}"> Tawk to Ekle</a>
				</li>
				<li>
					<a href="{{url('Admin/Tawk/Listele')}}"> Tawk to Listele</a>
				</li>
				<li>
					<a href="{{url('Admin/Ayarlar/Ekle')}}"> Ayar Ekle</a>
				</li>
				<li>
					<a href="{{url('Admin/Ayarlar/Listele')}}"> Ayar Listele</a>
				</li>

				<li>
					<a href="{{url('Admin/Google/Listele')}}"><i class="fa fa-google" aria-hidden="true">  Google Ayarları</i></a>
				</li>
				<li @if(strpos(Request::url(),'Mail')!==false || strpos(Request::url(),'Mail')!==false) class="active" @endif >
					<a href="#"><i class="fa fa-envelope fa-fw"></i> E-posta Ayarları<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
					
						
						<li style="margin-left:8%">
							<a href="{{url('Admin/MailKullanici/Listele')}}">İletişim Maili Gönderilecek Listele</a>
						</li>
						
						<li style="margin-left:8%">
							<a href="{{url('Admin/Mail/Listele')}}"> E-posta Ayar Listele</a>
						</li>
		                        
					</ul>
				</li>
				<li @if(strpos(Request::url(),'Dil')!==false || strpos(Request::url(),'DilSabiti')!==false) class="active" @endif >
				<a href="#"><i class="fa fa-language fa-fw"></i> Dil Yönetimi<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li style="margin-left: 8%">
							<a href="{{url('Admin/Dil/Ekle')}}"> Dil Ekle</a>
						</li>
						<li style="margin-left: 8%">
							<a href="{{url('Admin/Dil/Listele')}}"> Dil Listele</a>
						</li>
						<li style="margin-left: 8%">
							<a href="{{url('Admin/DilSabiti/Ekle')}}"> Dil Sabiti Ekle</a>
						</li>
						<li style="margin-left: 8%">
							<a href="{{url('Admin/DilSabiti/Listele')}}"> Dil Sabiti Listele</a>
						</li>
					</ul>
				</li>
                          
                            
			</ul>
		</li>

		
                

	@endif


</ul>