@extends('Admin.Layout.Form')

@section('baslik', 'Tawk.To')

@section('form')

<div class="form-group">
    <label for="Script" class="col-sm-2 control-label">Script</label>
    <div class="col-sm-10">
        <textarea type="text" class="form-control" required  name="Script" id="Script" placeholder="Script">{{@$veri->Script}}</textarea>
    </div>
</div>

@stop

@section('js')

<script type="text/javascript">
    
    jQuery(document).ready(function($) {
        $('.gizli').hide();
        $('.is_active').hide();
    });

</script>

@stop
