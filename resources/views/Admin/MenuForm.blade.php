@extends('Admin.Layout.Form')
@section('baslik'){{$Title}}@endsection
@section('form')


<input type="hidden" name="id" value="{{@$veri->id}}">
<div class="form-group">
    <label for="UstKatId" class="col-sm-2 control-label">Üst Menü</label>
    <div class="col-sm-10">
    	<select class="form-control" id="UstKatId" required name="UstKatId">
    		<option value="0">Üst Menüsü Yok</option>
    				{{ 
                        Fnk::ChildrenOption($menuler,@$veri->UstKatId,@$veri->id,'Adi',0,false)
                    }}
    	</select>
   </div>
</div>
<div class="form-group">
    <label for="Adi" class="col-sm-2 control-label">Menü Adı</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" required value="{{@$veri->Adi}}" name="Adi" id="Adi" placeholder="Menü Adı">
    </div>
</div>
<div class="form-group">
    <label for="MenuTipi" class="col-sm-2 control-label">Menü Tipi</label>
    <div class="col-sm-10">
        <select name="MenuTipi" class="form-control" required="" onchange="MenuTipiDegistir()" id="MenuTipi">
            <option value="">Lütfen Seçiniz</option>
            <option  value="icerik" @if(isset($veri) && $veri->MenuTipi=='icerik') selected @endif>İçerik</option>
            <option  value="link"  @if(isset($veri) && $veri->MenuTipi=='link') selected @endif  >Link</option>
            <option   value="modul" @if(isset($veri) && $veri->MenuTipi=='modul') selected @endif>Modül</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label for="LinkAcilisTipi" class="col-sm-2 control-label">Link Açılış Tipi</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="LinkAcilisTipi" value="" @if(isset($veri) && $veri->LinkAcilisTipi=='') checked @endif @if(!isset($veri)) checked @endif >Aynı Pencere</label>
    	</div>
	    <div class="radio">
	        <label><input type="radio" name="LinkAcilisTipi" @if(isset($veri) && $veri->LinkAcilisTipi=='target="_blank"') checked @endif value='target="_blank"'>Yeni Pencere</label>
		</div>
	</div>
</div>
<div class="form-group" id="Linkalani" @if(!isset($veri) || $veri->MenuTipi!='link') style="display:none" @endif >
	<label for="Link" class="col-sm-2 control-label">Menü Linki</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Link" name="Link" value="{{@$veri->Link}}">
    </div>
</div>
<div class="form-group" id="Icerikalani" @if(!isset($veri) || $veri->MenuTipi!='icerik') style="display:none" @endif >
     <label for="Icerik" class="col-sm-2 control-label">Menü İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Icerik" name="Icerik" class="ck">{{@$veri->Icerik}}</textarea>
    </div>
</div>
<div class="form-group" id="Modulalani" @if(!isset($veri) || $veri->MenuTipi!='modul') style="display:none" @endif >
    <label for="Modul" class="col-sm-2 control-label">Modül</label>
    <div class="col-sm-10">
        <select name="Modul" class="form-control" id="Modul" required  onchange="ModulTipiDegistir()">
            <option value="">Lütfen Modül Seçiniz</option>
            <option value="hizmetler-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if( substr(@$veri->Link, 4)  == \App\Http\Fnk::Ceviriadmin('hizmetler-url',substr(@$veri->Link,1,2)) ) selected @endif @endif >Hizmetler Modülü</option>
            <option value="iletisim-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('iletisim-url',substr(@$veri->Link,1,2))) selected @endif @endif>İletişim Modülü</option>
            <option value="video-galeri-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('video-galeri-url',substr(@$veri->Link,1,2))) selected @endif @endif>Video Galeri Modülü</option>
            <option value="foto-galeri-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('foto-galeri-url',substr(@$veri->Link,1,2))) selected @endif @endif>Foto Galeri Modülü</option>
            <option value="tools-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('tools-url',substr(@$veri->Link,1,2))) selected @endif @endif>Tools Modülü</option>
            <option value="bloglar" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('bloglar',substr(@$veri->Link,1,2))) selected @endif @endif>Bloglar  Modülü</option>
            <option value="mevzuat-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('mevzuat-url',substr(@$veri->Link,1,2))) selected @endif @endif>Mevzuat  Modülü</option>
             <option value="kurumsal-url" @if(@$veri->Link and $veri->MenuTipi=='modul') @if(substr(@$veri->Link, 4) == \App\Http\Fnk::Ceviriadmin('kurumsal-url',substr(@$veri->Link,1,2))) selected @endif @endif>Kurumsal  Modülü</option>
        </select>
    </div>
</div>
<div class="form-group hizmet" id="hizmet" @if(!isset($veri) || $veri->MenuTipi!='modul') style="display:none" @endif >
    <label for="Modul" class="col-sm-2 control-label">Hizmet Bağlantısı</label>
    <div class="col-sm-10">
        <select name="baglanti" class="form-control" id="baglanti" required >
            <option value="">Lütfen Hizmet Seçiniz</option>
            @foreach($hizmetler as $key=>$value)
             <option value="{{$value->id}}" @if(@$veri->Baglanti==$value->id) selected  @endif >{{$value->Adi}}</option>
            @endforeach
        
        </select>
    </div>
</div>
<div class="mevzuat" style="display:none" id="mevzuat">
    <div class="form-group">
        <label for="Resim" class="col-sm-2 control-label">Mevzuat Pdf</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" name="Pdf" id="Pdf" placeholder="Mevzuat Pdf">
        </div>
    </div>
    <div class="form-group" id="Icerikalani" @if(!isset($veri) || $veri->MenuTipi!='modul') style="" @endif >
         <label for="Icerik" class="col-sm-2 control-label">Mevzuat İçeriği</label>
         <div class="col-sm-10">
            <textarea required id="mevzuatIcerik" name="mevzuatIcerik" class="ck">{{@$veri->Icerik}}</textarea>
         </div>
    </div>
</div>

<div class="kurumsal" style="display:none" id="kurumsal">
<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Misyon Resmi</label>
    <div class="col-sm-10">
    @if(@$veri->MisyonResim)
        <img src="{{ url('images/image/'.@$veri->MisyonResim) }}" style="height: 100px;object-fit: contain;display: block;">
    @endif

        <input type="file" class="form-control" name="MisyonResim" id="MisyonResim" placeholder="Misyon Resim">
    </div>
</div>
<div class="form-group" id="Icerikalani" @if(!isset($veri) || $veri->MenuTipi!='modul') style="" @endif >
     <label for="Icerik" class="col-sm-2 control-label">Misyon İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Misyon" name="Misyon" class="ck">{{@$veri->Misyon}}</textarea>
    </div>
</div>

<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Vizyon Resmi</label>
    <div class="col-sm-10">
    @if(@$veri->VizyonResim)
        <img src="{{ url('images/image/'.@$veri->VizyonResim) }}" style="height: 100px;object-fit: contain;display: block;">
    @endif

        <input type="file" class="form-control" name="VizyonResim" id="VizyonResim" placeholder="Vizyon Resim">
    </div>
</div>

<div class="form-group" id="Icerikalani" @if(!isset($veri) || $veri->MenuTipi!='modul') style="" @endif >
     <label for="Icerik" class="col-sm-2 control-label">Vizyon İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Vizyon" name="Vizyon" class="ck">{{@$veri->Vizyon}}</textarea>
    </div>
</div>
<div class="form-group">
    <label for="Resim" class="col-sm-2 control-label">Hakkımızda Resmi</label>
    <div class="col-sm-10">
    @if(@$veri->HakkimizdaResim)
        <img src="{{ url('images/image/'.@$veri->HakkimizdaResim) }}" style="height: 100px;object-fit: contain;display: block;">
    @endif

        <input type="file" class="form-control" name="HakkimizdaResim" id="HakkimizdaResim" placeholder="Hakkimizda Resim">
    </div>
</div>
<div class="form-group" id="Icerikalani" @if(!isset($veri) || $veri->MenuTipi!='modul') style="" @endif >
     <label for="Icerik" class="col-sm-2 control-label">Hakkımızda İçeriği</label>
    <div class="col-sm-10">
        <textarea required id="Hakkimizda" name="Hakkimizda" class="ck">{{@$veri->Hakkimizda}}</textarea>
    </div>
</div>

</div>
<div class="form-group">
    <label class="col-md-2 control-label">Katalog Durumu</label>
    <div class="col-md-10">
        <div class="checkbox">
            <label>
                <input type="checkbox" id="ToolDurum" name="ToolDurum"  @if(@$veri->ToolDurum == 1) checked @endif >Sertifika olarak eklensin
            </label>
        </div>
    </div>
</div>
@stop

@section('js')
<script type="text/javascript">
    function MenuTipiDegistir() {
        var MenuTip = $('#MenuTipi').val();
        if (MenuTip == "link") {
            $('#Linkalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Modulalani').hide('slow');
            $('#kurumsal').hide('slow');
            $('#mevzuat').hide('slow');
            $('#hizmet').hide('slow');
        } else if (MenuTip == "icerik") {
            $('#Icerikalani').show('slow');
            $('#Linkalani').hide('slow');
            $('#Modulalani').hide('slow');
            $('#kurumsal').hide('slow');
            $('#mevzuat').hide('slow');
            $('#hizmet').hide('slow');
        } else if (MenuTip == "modul") {

            $('#Modulalani').show('slow');
            $('#Icerikalani').hide('slow');
            $('#Linkalani').hide('slow');
     
            var modul=$("#Modul").val();
            if(modul=="kurumsal-url")
            {
                $('#kurumsal').show('slow');
                $('#mevzuat').hide('slow');
                $('#hizmet').hide('slow');
             }
            else if(modul=="mevzuat-url")
            { 
                console.log("show");
                $('#mevzuat').show('show');
                $('#kurumsal').hide('slow');
                $('#hizmet').hide('slow');
            }
            else if(modul=="hizmetler-url")
            {
                $('#hizmet').show('slow');
                $('#kurumsal').hide('slow');
                $('#mevzuat').hide('slow');
            }
            else
            {
                $('#kurumsal').hide('slow');
                $('#mevzuat').hide('slow');
                $('#hizmet').hide('slow');
            }
            
        }

    }
    function  ModulTipiDegistir()
    {
        var modul=$("#Modul").val();
        if(modul=="kurumsal-url")
        {
            $('#kurumsal').show('slow');
            $('#mevzuat').hide('slow');
            $('#hizmet').hide('slow');

        }
        else if(modul=="mevzuat-url")
        { 
            console.log("show");
            $('#mevzuat').show('show');
            $('#kurumsal').hide('slow');
            $('#hizmet').hide('slow');
        }
        else if(modul=="hizmetler-url")
        {
            $('#hizmet').show('show');
            $('#kurumsal').hide('slow');
            $('#mevzuat').hide('slow');
        }
        else
        {
            $('#kurumsal').hide('slow');
            $('#mevzuat').hide('show');
            $('#hizmet').hide('slow');
        }
    }

jQuery(document).ready(function($) {
        var check;
        var modul=$("#Modul").val();
        if(modul=="kurumsal-url")
        {
            $('#kurumsal').show();
            $('#hizmet').hide('slow');
        }
        else if(modul=="mevzuat-url"){
                $('#mevzuat').show('show');
                $('#hizmet').hide('slow');}
        else if(modul=="hizmetler-url")
            $('#hizmet').show('show');
        else{
            $('#kurumsal').hide('slow');
            $('#mevzuat').hide('slow');
            $('#hizmet').hide('slow');
        }
        
        $("#Modul").change(function(){

            check = $(this).val();
            if(check=="tools-url")
                $('#ToolDurum').show('slow');
            else
                $('#ToolDurum').hide('slow');

    });
});
</script>
@stop