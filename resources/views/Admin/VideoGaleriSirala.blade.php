@extends('Admin.Layout.Master')

<?php
	$url = Request::url();
	$link = explode('Sirala',$url);
	$link = $link[0].'Sirala';
?>
@section('title') Video Galeri Sıralama @endsection

@section('content')
<div class="panel panel-default">
  <div class="panel-heading">
  <span class="pull-left">@yield('title')</span>
			</div>
  <div class="panel-body">
    <p class="alert alert-info">
		@yield('title') İşlemini Sürükle - Bırak Yaparak Sitenizdeki Görünüm Sırasını Değiştirebilirsiniz.
	</p>
	
	<div id="sortable">
	<?php
		foreach($galeri as $sirasayisi=>$v){
                    $sirasayisi++;
                    
                        echo '<div class="previewlive2 col-xs-12 col-sm-4 col-md-3" id="id-'.$v->id.'">
				<p>'.$sirasayisi.'</p><img src="http://img.youtube.com/vi/'.$v->Link.'/default.jpg"></img>
		</div>';
		}
	?>
	</div>
  </div>
</div>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{url('plugins/fancyBox/source/jquery.fancybox.css?v=2.1.5')}}" media="screen" />
<style>
#sortable > div { float: left; }
img { padding: 0.5em; }
.previewlive>a:hover:after{
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 90px;
    width: 100%;
    background: rgba(0,0,0,0.25) url('{{url('/')}}/images/uploads/camera3.png') no-repeat center center;
    background-size: 80px 80px;
}
</style>
@stop
@section('js')
<script src="{{asset('plugins/jquery-ui/jquery-ui.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#sortable').sortable({
        axis: 'x,y',
        revert: true,
        stop: function (event, ui) {
	        var oData = $(this).sortable('serialize');
	        console.log(oData);
           	$.ajax({
                data: oData+'&_token={{{csrf_token()}}}',
                type: 'POST',
                url: '{{$link}}'
            });
	}
    });
});

 </script>
@stop