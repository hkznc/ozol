<!DOCTYPE html>
<html lang="tr">
<head>
	@include('Site.Include.Head')
	@yield('css')
</head>
<body>
@include('Site.Include.Navbar')
@yield('navbar')


<!--img silder-->

@yield('content')

@include('Site.Include.Footer')
@yield('footer')

@include('Site.Include.Scripts')
@yield("js")

</body>
</html>