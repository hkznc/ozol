<!DOCTYPE html>
<html lang="tr" class="photography-demo-2">
    <head>
       
    <meta charset="utf-8">
    <title>@yield('Title')</title>
    <meta name="keywords" content="@yield('MetaTag')">
    <meta name="description" content="@yield('MetaDescription')" /> 
    <meta name="robots" content="index, follow"> 
    <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Vendor CSS -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/animate/animate.min.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.min.css') }}">
        
        <link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.min.css') }}">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="{{ asset('css/theme.css') }}">
        <link rel="stylesheet" href="{{ asset('css/theme-elements.css') }}">
        <link rel="stylesheet" href="{{ asset('css/theme-blog.css') }}">
        <link rel="stylesheet" href="{{ asset('css/theme-shop.css') }}">

        <!-- Current Page CSS -->
        <link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/settings.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/layers.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/navigation.css') }}">
        

        

        <!-- Owl Carousel Assets -->
        <link href="{{ asset('/plugins/owl.carousel/owl-carousel/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ asset('/plugins/owl.carousel/owl-carousel/owl.theme.css') }}" rel="stylesheet">
        <!-- Skin CSS -->
        <link rel="stylesheet" href="{{ asset('css/skins/skin-gym.css') }}"> 

        <!-- Demo CSS -->
        <link rel="stylesheet" href="{{ asset('css/demos/demo-gym.css') }}">


        <!-- Theme Custom CSS -->
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

        <!-- Head Libs -->
        <script src="{{ asset('vendor/modernizr/modernizr.min.js') }}"></script>

        
        
        @yield('css')
    </head>
   <body class="loading-overlay-showing" data-loading-overlay>
        <div class="loading-overlay">
            <div class="bounce-loader">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>        
            @include('Site.Include.Header')

            @yield('header')

            @yield('content')

    @include('Site.Include.Footer')

    @yield('footer')

    @include('Site.Include.Scripts')
    
    @yield('js')
</body>
</html>
