<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1254">
</head>

<body bgcolor="#FFFFFF" text="#000000">
<br>
<table width="47%" border="5" bordercolordark="#999999" bordercolorlight="#CCCCCC" align="center">
  <tr>
    <td>
      <table width="100%" border="1" cellspacing="0" cellpadding="4" bgcolor="#E6E6E6" bordercolor="#FFFFFF">
        <tr bordercolor="#FFFFFF" bgcolor="#4d4d4d"> 
          <td colspan="3"> 
            <div align="center"><b style="color: white">İlietişim Formu Bilgileri</b></div>
          </td>
        </tr>
        <tr bordercolor="#FFFFFF" bgcolor="#CCCCCC"> 
          <td width="37%">{{\App\Http\Fnk::Ceviri('adiniz-soyadiniz')}}</td>
          <td width="2%"> 
            <div align="center">:</div>
          </td>
          <td width="61%">{{$req->AdSoyad}}</td>
        </tr>
        <tr bordercolor="#FFFFFF" bgcolor="#FFFFFF"> 
          <td width="37%">{{\App\Http\Fnk::Ceviri('e-posta-adresiniz')}}</td>
          <td width="2%"> 
            <div align="center">:</div>
          </td>
          <td width="61%">{{$req->Eposta}}</td>
        </tr>
        <tr bordercolor="#FFFFFF" bgcolor="#CCCCCC"> 
          <td width="37%">{{\App\Http\Fnk::Ceviri('telefon-numaraniz')}}</td>
          <td width="2%"> 
            <div align="center">:</div>
          </td>
          <td width="61%">{{$req->Telefon}}</td>
        </tr>
        <tr bordercolor="#FFFFFF" bgcolor="#FFFFFF"> 
          <td width="37%">{{\App\Http\Fnk::Ceviri('konu')}}</td>
          <td width="2%"> 
            <div align="center">:</div>
          </td>
          <td width="61%">{{$req->Konu}}</td>
        </tr>
        <tr bordercolor="#FFFFFF" bgcolor="#CCCCCC"> 
          <td width="37%">{{\App\Http\Fnk::Ceviri('mesajiniz')}}</td>
          <td width="2%"> 
            <div align="center">:</div>
          </td>
          <td width="61%">{{$req->Mesaj}}</td>
        </tr>
        <tr bordercolor="#FFFFFF" bgcolor="#FFFFFF"> 
          <td width="37%">Gelen IP</td>
          <td width="2%"> 
            <div align="center">:</div>
          </td>
          <td width="61%">{{$req->gelen_ip}}</td>
        </tr>
        <tr bordercolor="#FFFFFF" bgcolor="#CCCCCC"> 
          <td width="37%" height="20">Gönderme Zamanı</td>
          <td width="2%" height="20"> 
            <div align="center">:</div>
          </td>
          <td width="61%" height="20">{{$req->created_at}}</td>
        </tr>
      </table>
</td>
  </tr>
</table>
</body>
</html>