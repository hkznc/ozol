@extends('Site.Layout.Master2')
@section('Title',@$mevzuat->MetaTitle)
@section('MetaTag')
    {!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',@$mevzuat->MetaDescription)
@section('content')

<div class="sayfa-my-container">
    <div class="sayfa-my-container-up">
       {{--  <div class="carousel-caption">
            <h1 class="display-3" style="padding-top: 2%!important" ><h1 class="display-3"></h1></h1>
        </div> --}}

    </div>
</div>
<div class="container padding">
    <div class="row text-center padding ">
        <div class="col-xs-12 col-sm-4 col-md-4">
          <br>
          <h4>Mevzuatlar </h4>
          <br>
          <ul class="list-group list-group-flush" style="margin-bottom: 5%">

                @foreach($mevzuatlar as $value)
                    <li class="list-group-item mevzuat-list"><a href="{{url($dil.'/mevzuat-url').'/'.$value->Slug}}"   @if($value->id==$mevzuat->id) style="color:#38cc91" @endif >{{@$value->Adi}}</a></li>
            
                    
                @endforeach
          </ul>      
        </div>
       {{--  <a href="http://localhost/ozol/public/tr/mevzuat-url/Otopark-Yonetmeligi" class="nav-link">Otopark Yönetmeliği </a> --}}
         <div class="col-xs-12 col-sm-8 col-md-8" >
          <br>
           <h3>{{@$mevzuat->Adi}}</h3>
           <br>
            <p >{{$mevzuat->Adi}} hakkında bilinmesi gereken herşeyi PDF formatında açabilirsiniz. </p>
            @if(!empty($mevzuat->Pdf))
             <a target="_blank" title="{{$mevzuat->Adi}}" href="{{url('images/uploads/Mevzuat/'.$mevzuat->Pdf)}}">
                <img src="{{url('/images/uploads/Mevzuat/pdf-shadow.jpg')}}" alt="pdf shadow" onmouseover="this.src='{{url("/images/uploads/Mevzuat/pdf.jpg")}}';" onmouseout="this.src='{{url("/images/uploads/Mevzuat/pdf-shadow.jpg")}}';" style="margin-bottom:2%">
            </a>
            @else
             <img src="{{url('/images/uploads/Mevzuat/pdf-shadow.jpg')}}" alt="pdf shadow" onmouseover="this.src='{{url("/images/uploads/Mevzuat/pdf.jpg")}}';" onmouseout="this.src='{{url("/images/uploads/Mevzuat/pdf-shadow.jpg")}}';" style="margin-bottom:2%">
            @endif
           
        </div>
    </div>
    <hr class="my-4">
</div>


@stop
@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-footer.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-nav.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">

/*.sayfa-my-container{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background: fixed url("../images/image/library6.jpg") ;
    background-size: cover;

}
.sayfa-my-container-up{
    width: 100%;
    height: 45%;
    background: fixed url("../images/image/library6.jpg") ;
    opacity: 0.7;
    position: absolute;
}*/
.my-container-two{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background:  fixed url("../images/image/kurumsaltwo.jpg") ;
    background-size: cover;

}
a{
    color:#6c757d;
}
.mevzuat-list a:hover {
    color: #38cc91!important;
    text-decoration: none!important;
}
h4{
     color:#323435;
}
</style>




@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
@stop

