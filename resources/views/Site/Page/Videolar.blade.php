@extends("Site.Layout.Master2")
@section('content')

<div class=" galeri-my-container">
    <div class="my-container-up">
        <div class="carousel-caption">
            <h1 class="display-3" >Yaptığımız İşleri Görmeden Karar Vermeyin</h1>
        </div>

    </div>
</div>
<div class="container-fluid bg-glr hover01  padding" style=" padding-top: 20px;padding-left: 10%; padding-right: 10%">
    <div class="row padding">

        @if(!empty($videogaleri))
        @foreach($videogaleri as $key=>$video)
          <div class=" col-xs-12 col-sm-6 col-md-3">

            <figure><img src="{{url('images/uploads/FotoGaleri/Kapak/').'/'.@$video->KapakFotografi}}" style="width:100%" onclick="openModal('{{$video->Link}}');currentSlide({{$key+1}})" class="hover-shadow cursor"></figure>
     
          </div>
        @endforeach
        @endif
      
    </div>
</div>
<div id="my-Modal" class="modal">
    <span class="close cursor" onclick="closeModal()">&times;</span>
    <div class="modal-content video_content" style="width: 620px;height: 420px">

        <div class="mySlides " >
            {{-- <div class="numbertext">{{$key+1}} / {{count($videogaleri[0])}}</div> --}}
            {{-- <iframe width="560" height="315" src="{{url('https://www.youtube.com/embed/'.$videogaleri[0]->Link).'?autoplay=1'}}" width="500px" frameborder="0" allowfullscreen></iframe> --}}
            <iframe width="600px" height="400px"  frameborder="0" allowfullscreen="true" tabindex="-1"  id="video_iframe" >

            </iframe>
            </div>
          </div>
        </div>
      </div>

        </div>
        
    </div>
</div>

<div class="galery-my-container-two">
    <div class="my-container-up-gallery-two">
        <div class="carousel-caption">
        </div>
    </div>
</div>
@stop
@section("css")
    <link rel="stylesheet" href="{{asset('css/style-gallery.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-gallery.css')}}">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-footer.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-nav.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
.my-container{
     width: 100%;
    height: 45%;
    margin-top: 0;
    background: fixed url(../images/image/gallery-bottom.jpg);
    background-size: cover;
}
.my-container-up{
    height: 45%;
    background: rgba(50,80,70,0.3)!important;
    opacity: 0.7;
    position: absolute;
}
.my-container-two{
     width: 100%;
    height: 30%;
    margin-top: 0;
    background: fixed url(../image/gallery-bottom.jpg);
    background-size: cover;
}
@media(max-width: 801px){
    .video_content{
        width: 620px!important;
        height: 320px!important;
    }
    iframe{
        width: 600px!important;
        height: 300px!important;
    }

}
@media(max-width: 601px){
    .video_content{
        width: 520px!important;
        height: 270px!important;
    }
    iframe{
        width: 500px!important;
        height: 250px!important;
    }

}
@media(max-width: 490px){
    .video_content{
        width: 320px!important;
        height: 200px!important;
    }
    iframe{
        width: 300px!important;
        height: 180px!important;
    }

}
@media(max-width: 390px){
    .video_content{
        width: 290px!important;
        height: 200px!important;
    }
    iframe{
        width: 270px!important;
        height: 180px!important;
    }

}
@media(max-width: 350px){
    .video_content{
        width: 200px!important;
        height: 150px!important;
    }
    iframe{
        width: 180px!important;
        height: 130px!important;
    }

}



</style>
@stop
@section("js")
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script>
    function openDialog() {
        document.getElementById("myModal").style.zIndex = "2000";
        document.getElementById("myModal").style.top = "-80px";
        document.getElementById("myModal").style.backgroundColor = "rgba(0,0,0,0.1)";
    }
</script>
<script>
var giden="";
    function openModal(link) {
        console.log("link degeri"+link);
        if(window.innerWidth>568){

           
           
            // document.getElementById('fade').style.display = 'block';

             document.getElementById("video_iframe").src ="//www.youtube.com/embed/"+link+"?autoplay=1&amp;cc_load_policy=1&amp;color=null&amp;controls=1&amp;disablekb=0&amp;enablejsapi=0&amp;end=null&amp;fs=1&amp;h1=null&amp;iv_load_policy=1&amp;list=null&amp;listType=null&amp;loop=0&amp;modestbranding=null&amp;origin=null&amp;playlist=null&amp;playsinline=null&amp;rel=0&amp;showinfo=1&amp;start=0&amp;wmode=transparent&amp;theme=dark";

            var lightBoxVideo = document.getElementById("VisaChipCardVideo");
            window.scrollTo(0, 0);
            document.getElementById('my-Modal').style.display = 'block';
            lightBoxVideo.play();

            // document.getElementById('my-Modal').style.display = "block";
            console.log("open modal içinde");
        }

    }

    function closeModal() {
        document.getElementById('my-Modal').style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
        console.log("gelen id:"+n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        captionText.innerHTML = dots[slideIndex - 1].alt;
    }
    

</script>
@stop


