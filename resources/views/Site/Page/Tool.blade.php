@extends('Site.Layout.Master')


@section('Title',$menu->MetaTitle)
@section('MetaTag')
    {!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',$menu->MetaDescription)



@section('content')
<div role="main" class="main">
    <section class="page-header page-header-custom-background parallax" style="background-color: rgba(57, 74, 9, 0.85);" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="{{asset('img/parallax.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="{{ url('/'.$dil) }}">{{\App\Http\Fnk::Ceviri('anasayfa')}}</a></li>
                    <li class="active">Kataloglar</li>
                </ul>
                <h1>{{$tool->Adi}}</h1>
            </div>
        </div>
    </div>
</section>
    
    <div class="container">
        <div class="row">
            <div id="scroller" style="height: 800px; width: 100%; overflow: auto; " >
                
                    <iframe id="iframe" width="100%" height="100%" src="{{url('/pdfjs/web/viewer.html?file='.url('/images/uploads/Tools/Pdf/'.$tool->Pdf).'#page=1&zoom=page-width')}}"></iframe>
                
            </div>
            </div>
            </div>
            <div class="clearfix"></div>
           
       
@endsection
<style type="text/css">
    #main{
        padding-bottom:0px !important;
    }
    .socialheadericon{
        padding-top: 7px;
    }
    .page-box {
        min-height: 40% !important;
        padding-bottom: 20px;
    }
    
</style>
