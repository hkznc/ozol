@extends('Site.Layout.Master')

@section('Title',$menu->MetaTitle)
@section('MetaTag')
    {!! @implode(', ', json_decode(@$ayarlar->MetaTag ,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)) !!}
@endsection
@section('MetaDescription',$menu->MetaDescription)


@section('content')

<div role="main" class="main">
    <section class="page-header page-header-custom-background parallax" style="background-color: rgba(57, 74, 9, 0.85);" data-plugin-parallax data-plugin-options='{"speed": 1.5}' data-image-src="{{asset('img/parallax.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                <ul class="breadcrumb breadcrumb-valign-mid">
                    <li><a href="{{ url('/'.$dil) }}">{{\App\Http\Fnk::Ceviri('anasayfa')}}</a></li>
                    <li class="active">{{ \App\Http\Fnk::Ceviri($menu->Adi)}}</li>
                </ul>
                <h1>{{ \App\Http\Fnk::Ceviri($menu->Adi)}}</h1>
            </div>
        </div>
    </div>
</section>


<div class="clearfix"></div>    
<br/>          
<div class="clearfix"></div>           
    <div class="container">
        <div class="row">
            
            <div class="col-md-12 col-xs-12">
                @if(!empty($kategori->Icerik))
                    <div  class="urun urun-aciklama col-sm-12 col-md-12 col-xs-12">
                        {!!$kategori->Icerik!!}
                    </div>
                @endif
            <div class="clearfix"></div>
            <div class="clearfix"></div>

            @foreach($urunler as $element)
            @if($element->Gosterim == 3)
                <div  class="urun col-sm-3 col-md-3 ">
                <a href="{{url($dil.'/urunler/kategori/'.$element->Urun_AltKat)}}">
                <span class="thumb-info">
                    <span class="thumb-info-wrapper">
                        <img src="{{url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])}}" class="img-responsive" alt="" style="height: 250px; width:150% ; padding: 20px" >
                        <span class="thumb-info-title">
                            <span class="thumb-info-inner">{{$element->Adi}}</span>
                            <span class="thumb-info-type">İncele</span>
                        </span>
                        <span class="thumb-info-action">
                            <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                        </span>
                    </span>
                </span>
                </a> 
                </div>
            @else
                <div  class="urun col-sm-3 col-md-3 ">
                <a href="{{url($dil.'/'.\App\Http\Fnk::Ceviri('urun-url').'/'.$element->Slug)}}">
                    <span class="thumb-info">
                        <span class="thumb-info-wrapper">
                            <img src="{{url('/images/uploads/Urun/resim/'.json_decode($element->Resim)[0])}}" class="img-responsive" alt="" style="height: 250px; width:150% ; padding: 20px" >
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">{{$element->Adi}}</span>
                                <span class="thumb-info-type">İncele</span>
                            </span>
                            <span class="thumb-info-action">
                            <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                        </span>
                    </span>
                </span>
                </a> 
                </div>
            @endif
            @endforeach
            </div>
                <div style="float: right;">
                    {!! $urunler->render() !!}
                </div>
        </div>
    </div>
</div>
</div><!-- .container -->

@endsection

