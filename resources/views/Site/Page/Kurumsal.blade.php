@extends('Site.Layout.Master2')
@section('content')

<div class="kurumsal-my-container">
    <div class="kurumsal-my-container-up">
        <div class="carousel-caption">
            <h1 class="display-3" >Sitenizi En İyi Şekilde Yönetelim</h1>
        </div>

    </div>
</div>
<div class="container-fluid padding">
    <div class="row padding back">
        <div class="col-lg-6">
            <div class="text-right-animate" style="width:100%">
            <h1 class="display-6">Misyonumuz</h1>
            <p class="lead">{!! @$kurumsal->Misyon!!}</p>
        </div>
        </div>
        <div class="col-lg-6">
            @if(!empty($HakkimizdaResim))
               <img src="{{url('images/image').'/'.@$kurumsal->MisyonResim}}" class="img-fluid">
            @else
                <img src="{{url('images/image/misyon.jpg')}}" class="img-fluid">
            @endif
            
        </div>

    </div>

</div>
<div class=" kurumsal-my-container" style="height: 30%; ">
    <div class="kurumsal-my-container-up" style="height: 30%;">
        <div class="carousel-caption">
        </div>
    </div>
</div>
<div class="container-fluid padding">
    <div class="row padding back">
        <div class="col-lg-6 img-mid-size">
            @if(!empty($HakkimizdaResim))
                <img src="{{url('images/image').'/'.@$kurumsal->VizyonResim}}" class="img-fluid" >
            @else
                <img src="{{url('images/image/vision.jpg')}}" class="img-fluid">
            @endif
           
        </div>
        <div class="col-lg-6">
            <div class="text-left-animate" style="width:100%">
            <h1 class="display-6">Vizyonumuz</h1>

                <p class="lead">{!! @$kurumsal->Vizyon!!}</p>
            </div>

        </div>


    </div>

</div>
<div class="kurumsal-my-container-two" style="height: 30%;">
    <div class="kurumsal-my-container-up-two" style="height: 30%;">
        <div class="carousel-caption">
        </div>
    </div>
</div>
<div class="container-fluid padding">
    <div class="row padding back">
        <div class="col-lg-6">
            <div class="text-right-animate" style="width:100%">
            <h1 class="display-6">Hakkımızda </h1>
            <p class="lead">{!! @$kurumsal->Hakkimizda!!}</p>
            </div></div>
        <div class="col-lg-6">
        @if(!empty($kurumsal->Resim))
            <img src="{{url('images/image').'/'.@$kurumsal->Resim}}" class="img-fluid">
        @else
            <img src="{{url('images/image/hakkimizda.jpg')}}" class="img-fluid">
        @endif
        </div>

    </div>

</div>


@stop
@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-footer.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-nav.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
/*.kurumsal-my-container{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background:  fixed url("../images/image/kurumsal.jpg")!important ;
    background-size: cover;

}
.my-container-up{
    width: 100%;
    height: 45%;
    background: rgba(50,80,70,0.3)!important;
    opacity: 0.7;
    position: absolute;
}*/
.my-container-two{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background:  fixed url("../images/image/kurumsaltwo.jpg") ;
    background-size: cover;

}
</style>




@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
@stop

