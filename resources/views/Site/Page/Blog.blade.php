@extends('Site.Layout.Master2')
@section('content')

<div class="my-container">
    <div class="my-container-up">
        <div class="carousel-caption">
            <h1 class="display-3" >Blog Yazıları</h1>
        </div>

    </div>
</div>

<div class="container-fluid padding">
    <div class="row padding back" style="padding: 1%; background-color: #e5f4fd59">
       @if(!empty($blog->Resim))

         <div class="col-lg-4  col-md-4 img-mid-size" >
                <img src="{{url('images/uploads').'/'.@$blog->Resim}}" class="img-fluid"  >
                  </div>
            <div class="col-lg-8 col-md-8">
            <div class="text-left-animate" style="width:100%;word-break: break-word;" >
            <h4 class="display-6">{{@$blog->Adi}}</h4>
                <p class="lead">{!! @$blog->Icerik!!}</p>
            </div>
            </div>
        @else
         <div class="col-lg-12 col-md-12">
            <div class="text-left-animate" style="width:100%;word-break: break-word; margin:2%" >
            <h4 class="display-6">{{@$blog->Adi}}</h4>
                <p class="lead">{!! @$blog->Icerik!!}</p>
            </div>
            </div>
        @endif
           
 


    </div>

</div>
<div class="my-container">
    <div class="my-container-up">
        <div class="carousel-caption">
            
        </div>

    </div>
</div>


@stop
@section('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-footer.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-nav.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
/*.kurumsal-my-container{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background:  fixed url("../images/image/kurumsal.jpg")!important ;
    background-size: cover;

}
.my-container-up{
    width: 100%;
    height: 45%;
    background: rgba(50,80,70,0.3)!important;
    opacity: 0.7;
    position: absolute;
}*/
.my-container-two{
    width: 100%;
    height: 45%;
    margin-top: -65px;
    background:  fixed url("../images/image/kurumsaltwo.jpg") ;
    background-size: cover;

}
</style>




@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
@stop

