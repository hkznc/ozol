@extends('Site.Layout.Master2')
@section('content')

<div class="hizmetler-my-container">
    <div class="hizmetler-my-container-up">
        <div class="carousel-caption">
            <h1 class="display-3">Temizleme ve Yönetme İşini Bize Bırakın</h1>
        </div>
    </div>
</div>
<div class="container-fluid" style="padding-bottom: 50px;padding-top: 50px">
    <div id="accordion">
    
    @if(@$hizmetler)
        @foreach($hizmetler as $key=>$hizmet)
            <div class="card ">
                <div class="card-header " id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link"  data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne">
                           {{@$hizmet->Adi}}
                        </button>
                    </h5>
                </div>

                <div id="collapse{{$key}}" @if(@$aktifHizmet && @$aktifHizmet==$hizmet->id) class="collapse bg-card show" @elseif(empty($aktifHizmet) && $key==0)  class="collapse bg-card show" @else class="collapse bg-card "  @endif  aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="row bg-row">
                        <div class="col-lg-5">
                            <img src="{{url('images/uploads/Hizmetler/').'/'.@$hizmet->Resim}}" class="img-fluid img-fluid-hizmetler">
                        </div>
                        <div class="col-lg-7">
                            <div class="acordion-text">
                                <p class="lead">{!! @$hizmet->Icerik!!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

        @endforeach
    @endif





      {{--   <div class="card ">
            <div class="card-header " id="headingOne">
                <h5 class="mb-0">
                    <button class="btn btn-link"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                       Ev ve Ofis Temizliği
                    </button>
                </h5>
            </div>

            <div id="collapseOne" class="collapse bg-card show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="row bg-row">
                    <div class="col-lg-5">
                        <img src="{{url('images/image/ev-ofis-temizliği.jpg')}}" class="img-fluid">
                    </div>
                    <div class="col-lg-7">
                        <div class="acordion-text">
                            <p class="lead">Temizlik her yerde her mekanda gerekli olan ve hayatımızda ki olmazsa olmazlardandır.Gelişen
                                şartlar
                                doğrultusunda,temizliği en ekonomik ve en profesyonel koşullarla müşterilerine sunmayı amaç edinmiştir.

                                Özellikle ofislerimize ve toplu yaşam alanlarımıza optimum çözümle maksimum faydayı sizlere sunmayı amaç
                                edinmiştir.

                                Unutmamalıdır ki temiz olmayan ortamlar,sağlığımızı direkt tehdit etmektedir.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Aparatman Temizliği
                    </button>
                </h5>
            </div>
            <div id="collapseTwo" class="collapse bg-card" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="row bg-row">
                    <div class="col-lg-5">
                        <img src="{{url('images/image/hizmet-temizlik.jpg')}}" class="img-fluid">
                    </div>
                    <div class="col-lg-7">
                        <div class="acordion-text">
                            <p class="lead">Temizlik her yerde her mekanda gerekli olan ve hayatımızda ki olmazsa olmazlardandır.Gelişen
                                şartlar
                                doğrultusunda,temizliği en ekonomik ve en profesyonel koşullarla müşterilerine sunmayı amaç edinmiştir.

                                Özellikle ofislerimize ve toplu yaşam alanlarımıza optimum çözümle maksimum faydayı sizlere sunmayı amaç
                                edinmiştir.

                                Unutmamalıdır ki temiz olmayan ortamlar,sağlığımızı direkt tehdit etmektedir.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header " id="headingThree">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Site Yönetimi
                    </button>
                </h5>
            </div>
            <div id="collapseThree" class="collapse bg-card" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="row bg-row">
                    <div class="col-lg-5">
                        <img src="{{url('images/image/hizmetler-site-yonetimi.jpg')}}" class="img-fluid">
                    </div>
                    <div class="col-lg-7">
                        <div class="acordion-text">
                            <p class="lead">Temizlik her yerde her mekanda gerekli olan ve hayatımızda ki olmazsa olmazlardandır.Gelişen
                                şartlar
                                doğrultusunda,temizliği en ekonomik ve en profesyonel koşullarla müşterilerine sunmayı amaç edinmiştir.

                                Özellikle ofislerimize ve toplu yaşam alanlarımıza optimum çözümle maksimum faydayı sizlere sunmayı amaç
                                edinmiştir.

                                Unutmamalıdır ki temiz olmayan ortamlar,sağlığımızı direkt tehdit etmektedir.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header " id="headingFour">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                        Bahçe Temizliği
                    </button>
                </h5>
            </div>
            <div id="collapseFour" class="collapse bg-card" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="row bg-row">
                    <div class="col-lg-5">
                        <img src="{{url('images/image/hizmet-bahce.jpg')}}" class="img-fluid">
                    </div>
                    <div class="col-lg-7">
                        <div class="acordion-text">
                            <p class="lead">Temizlik her yerde her mekanda gerekli olan ve hayatımızda ki olmazsa olmazlardandır.Gelişen
                                şartlar
                                doğrultusunda,temizliği en ekonomik ve en profesyonel koşullarla müşterilerine sunmayı amaç edinmiştir.

                                Özellikle ofislerimize ve toplu yaşam alanlarımıza optimum çözümle maksimum faydayı sizlere sunmayı amaç
                                edinmiştir.

                                Unutmamalıdır ki temiz olmayan ortamlar,sağlığımızı direkt tehdit etmektedir.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" >
            <div class="card-header " id="headingFive">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                        Bahçe Budama
                    </button>
                </h5>
            </div>
            <div id="collapseFive" class="collapse bg-card" aria-labelledby="headingFive" data-parent="#accordion">
                <div class="row bg-row">
                    <div class="col-lg-5">
                        <img src="{{url('images/image/budama.jpg')}}" class="img-fluid">
                    </div>
                    <div class="col-lg-7">
                        <div class="acordion-text">
                            <p class="lead">Temizlik her yerde her mekanda gerekli olan ve hayatımızda ki olmazsa olmazlardandır.Gelişen
                                şartlar
                                doğrultusunda,temizliği en ekonomik ve en profesyonel koşullarla müşterilerine sunmayı amaç edinmiştir.

                                Özellikle ofislerimize ve toplu yaşam alanlarımıza optimum çözümle maksimum faydayı sizlere sunmayı amaç
                                edinmiştir.

                                Unutmamalıdır ki temiz olmayan ortamlar,sağlığımızı direkt tehdit etmektedir.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</div>





@stop
@section('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-footer.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-nav.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/style-services.css')}}">
<style type="text/css">
.my-container{
     width: 100%;
    height: 40%;
    margin-top: 0;
    background:  fixed url("../images/image/srou-isareti.jpg") ;
    background-size: cover;
}
.my-container-up{

    height: 40%;
    background: rgba(50,80,70,0.3)!important;
    opacity: 0.7;
    position: absolute;
}


/*.bg-card{
    background: fixed url("../image/card-backgraound-leave.jpg");
     /*background: rgba(50,80,70,0.3)!important;*/
 /*}*/
</style>


@stop

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
@stop