@extends('Site.Layout.Master2')
@section('content')

<div class="my-container iletisim-my-container">
    <div class="my-container-up ">
        <div class="carousel-caption">
            <h1 class="display-3">Teklif Almak İçin Bize Ulaşın</h1>
        </div>
    </div>
</div>


<div class="container" style="padding-top: 20px">

        <div class="card-header" style="text-align: center" >
            <h1>İletişim Bilgileri</h1>
        </div>
       
        <div class="jumbotron" style="text-align:left">
            <div class="row bg-row">
                <div class="col-lg-6">
                  <form id="contactform" class="form-box register-form contact-form" enctype="multipart/form-data"  method="POST" action="{{url($dil.'/'.\App\Http\Fnk::Ceviri('iletisim-url'))}}" id="form">
          <input type='hidden' name='_token' value='{{csrf_token()}}' ></input>
                    <div class="form-group">
                        <label for="usr">İsim Soyisim:</label>
                        <input type="text" placeholder="ornek isim" class="form-control" id="AdSoyad" name="AdSoyad">
                    </div>
                    <div class="form-group">
                        <label for="eml">E-mail:</label>
                        <input type="email" placeholder="orenk@gmail.com" class="form-control" name="Eposta" id="Eposta">
                    </div>
                     <div class="form-group">
                        <label for="num">Telefon:</label>
                        <input type="text" placeholder="555 555 5555 "  class="form-control" id="Telefon" name="Telefon" maxlength="15">
                    </div>
                    <div class="form-group">
                        <label for="num">Konu:</label>
                        <input type="text" placeholder="Eposta Konusu "  class="form-control" id="Konu" name="Konu" maxlength="50">
                    </div>
                    <div class="form-group"><!-- comment(yorum-->
                        <label for="comment">Mesaj:</label>
                        <textarea class="form-control" rows="5" placeholder="Bize burdan yazın" id="Mesaj" name="Mesaj"></textarea>
                    </div>
                    <button type="sumbit" class="btn btn-primary">Gönder</button>
                    </form>
                </div>
                <div class="col-lg-6">
                    <hr class="light">
                    <p>
                   
                        <a href="tel:{{$iletisim->Telefon}}" class="col-md-3">
                            <i class="fa fa-phone-square "></i>
                         {{@$iletisim->Telefon}} 
                        </a>
                    </p>
                    @if(!empty($iletisim->Telefon2))
                     <p>
                        <a href="tel:{{$iletisim->Telefon2}}" class="col-md-3">
                            <i class="fa fa-phone-square "></i>
                       
                         {{@$iletisim->Telefon2}}
                           </a>
                    </p>
                    @endif
                   
                    <p>
                        <a href="#" class="col-md-3">
                            <i class="fa fa-fax "></i>
                        </a>
                        Fax: {{@$iletisim->Fax}}
                    </p>
                    <p>
                        <a href="#" class="col-md-3">
                            <i class="fa fa-location-arrow"></i>
                        </a>
                        Adres: {{@$iletisim->Adres}}
                    </p>
                </div>
            </div>
    </div>



</div>

<div class="container" id="map" style="width: 100%; margin-bottom: 50px;height: 400px; position: relative; overflow: hidden;">
    <div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);">
        <div class="gm-style"
             style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;">
            <div tabindex="0"
                 style="position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;), default; touch-action:pan-x pan-y;">
            
                   
                   
               
<iframe src="https://www.google.com/maps/embed?pb=!1m21!1m12!1m3!1d303.63874004163125!2d32.88271433731413!3d39.9081265559602!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m6!3e3!4m3!3m2!1d39.9082393!2d32.882749499999996!4m0!5e0!3m2!1str!2str!4v1539086954830" width="1200" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>

            

            
                   
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section("css")
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-footer.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-nav.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<<style type="text/css" media="screen">
.my-container{
    background:  fixed url("../images/image/iletisim-header-ahize.jpg") ;
    width: 100%;
    height: 70%;
    margin-top: -85px;
    /*background: fixed url(../image/iletisim-header-ahize.jpg);*/
    background-size: cover;
}
.my-container-up{
	width: 100%;
    height: 70%;
    background: rgba(50,80,70,0.1)!important;
    background: white;
    position: absolute;
}
.display-3 
{
	margin-top:10%;
}
@media (max-width: 801px) {
.iletisim-my-container{
    height: 45%;
}
}

	
</style>
@stop
@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{asset('js/jquery.mask.js')}}"></script>

<script>
     $(document).ready(function() {
      $('#Telefon').mask('999 999 99 99');     
    });

    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;

        if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else if (currentScrollPos>300){
            document.getElementById("navbar").style.top = "-85px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>
@stop
