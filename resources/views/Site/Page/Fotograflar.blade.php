@extends("Site.Layout.Master2")
@section('content')

<div class=" galeri-my-container">
    <div class="my-container-up">
        <div class="carousel-caption">
            <h1 class="display-3" >Yaptığımız İşleri Görmeden Karar Vermeyin</h1>
        </div>

    </div>
</div>
<div class="container-fluid bg-glr hover01  padding" style=" padding-top: 20px;padding-left: 10%; padding-right: 10%">
    <div class="row padding">

        @if(!empty($fotograflar))
        @foreach($fotograflar as $key=>$foto)
          <div class=" col-xs-12 col-sm-6 col-md-3">
            <figure><img src="{{url('images/uploads/FotoGaleri/').'/'.$foto->Resim}}" style="width:100%;" onclick="openModal();currentSlide({{$key+1}})" class="hover-shadow cursor"></figure>
          </div>
        @endforeach
        @endif
      
    </div>
</div>
<div id="my-Modal" class="modal">
    <span class="close cursor" onclick="closeModal()">&times;</span>
    <div class="modal-content">
        @foreach($fotograflar as $key=>$foto)
        <div class="mySlides">
            <div class="numbertext">{{$key+1}} / {{count($fotograflar)}}</div>
            <img src="{{url('images/uploads/FotoGaleri/').'/'.$foto->Resim}}" style="width:100%;height: 100%">
        </div>
        @endforeach
        
        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
        <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>
</div>

<div class="galery-my-container-two">
    <div class="my-container-up-gallery-two">
        <div class="carousel-caption">
        </div>
    </div>
</div>
@stop
@section("css")
    <link rel="stylesheet" href="{{asset('css/style-gallery.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-gallery.css')}}">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-footer.css')}}">
    <link rel="stylesheet" href="{{asset('css/style-nav.css')}}">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<style type="text/css">
.my-container{
     width: 100%;
    height: 45%;
    margin-top: 0;
    background: fixed url(../images/image/gallery-bottom.jpg);
    background-size: cover;
}
.my-container-up{
    height: 45%;
    background: rgba(50,80,70,0.3)!important;
    opacity: 0.7;
    position: absolute;
}
.my-container-two{
     width: 100%;
    height: 30%;
    margin-top: 0;
    background: fixed url(../image/gallery-bottom.jpg);
    background-size: cover;
}


</style>
@stop
@section("js")
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
<script>
    function openDialog() {
        document.getElementById("myModal").style.zIndex = "2000";
        document.getElementById("myModal").style.top = "-80px";
        document.getElementById("myModal").style.backgroundColor = "rgba(0,0,0,0.1)";
    }
</script>
<script>

var modal = document.getElementById('my-Modal');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

    function openModal() {
        if(window.innerWidth>568){
            document.getElementById('my-Modal').style.display = "block";
            value=true;
        }

    }

    function closeModal() {
        document.getElementById('my-Modal').style.display = "none";
        value=false;
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {
            slideIndex = 1
        }
        if (n < 1) {
            slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
        captionText.innerHTML = dots[slideIndex - 1].alt;
    }

</script>
@stop


