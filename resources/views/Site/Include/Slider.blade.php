@section('slider')
<div id="sld" class="carousel slide" data-ride="carousel">

@if(@$sliders)
    <ul class="carousel-indicators">
        @foreach($sliders as $key=>$slider)
            <li data-target="#sld" data-slide-to="{{$key}}" @if($key==0) class="active" @endif></li>
        @endforeach
    </ul>
    <div class="carousel-inner ">
    @foreach($sliders as $key1=>$slider)
      <div @if($key1==0) class="carousel-item active " @else class="carousel-item " @endif >
            <img src="{{url('images/uploads/Slider').'/'.$slider->Resim}}" alt="Ev ve ofis" class="img-slide-size">
            <div class="carousel-caption">
                <h1 class="display-2">{{@$slider->Icerik}}</h1>
                <h3>{{@$slider->Icerik2}}</h3>
                 @if($key1==0)
                <a href="{{url(Request::segment(1).'/'.\App\Http\Fnk::Ceviri('iletisim-url'))}}" type="button" class="btn btn-success btn-lg" style="margin-top: 13%">Bizden Teklif Alın</a>
                @endif
            </div>
        </div>

    @endforeach
    <a class="carousel-control-prev" href="#sld" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#sld" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
@endif
</div>


@endsection