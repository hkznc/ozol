<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$router->pattern('id', '[0-9]+'); // tüm {id} kullanımları integer olmak zorunda

if (in_array(Request::segment(1), Config::get('app.alt_langs'))) {
    App::setLocale(Request::segment(1));
    Config::set('app.locale_prefix', Request::segment(1));
}

if (Schema::hasTable('diller')){
    $diller = \App\Http\Models\Dil::where('is_active',1)->lists('KisaAd','KisaAd');
    $dillerstring = '(';
    $x = 0;
    foreach($diller as $key => $dil){
    if($x>0){
        $dillerstring.='|';
    }
        $dillerstring .= $dil;
        $x++;
    }
    $dillerstring .= ')';
$router->pattern('dil',$dillerstring); // Tüm diller Route Pattern olarak ayarlandı.
}



Route::group(['middleware'=>'web'], function () {
    //Route::auth();
    Route::get('/',function(){
        $diller = \App\Http\Models\Dil::where('is_active',1)->get();
        return view('Site.redirect',compact('diller'));
    });
    
    Route::group(['prefix' => '{dil}'], function() {
        Route::get('/', 'Site\DashboardController@index');
        Route::get('Ara','Site\AraController@show');
        Route::get(\App\Http\Fnk::Ceviri('iletisim-url'), 'Site\IletisimController@index');
        Route::post(\App\Http\Fnk::Ceviri('iletisim-url'), 'Site\IletisimController@store');
        Route::get(\App\Http\Fnk::Ceviri('menu-url').'/{slug}', 'Site\MenuController@show');
        Route::get(\App\Http\Fnk::Ceviri('alt-menu-url').'{slug}', 'Site\AltMenuController@show');
        Route::get(\App\Http\Fnk::Ceviri('slider-url').'/{slug}', 'Site\SliderController@show');
        Route::get(\App\Http\Fnk::Ceviri('haberler-url'), 'Site\HaberController@index');
        Route::get(\App\Http\Fnk::Ceviri('haber-url').'/{slug}', 'Site\HaberController@show');
        Route::get(\App\Http\Fnk::Ceviri('kategoriler-url'), 'Site\KategoriController@index');
        Route::get(\App\Http\Fnk::Ceviri('kategori-url').'/{slug}', 'Site\KategoriController@show');


        Route::get(\App\Http\Fnk::Ceviri('fotograflar').'/{id}', 'Site\FotograflarController@index');
        Route::get(\App\Http\Fnk::Ceviri('foto-galeri-url'), 'Site\FotoGaleriController@index');

        Route::get(\App\Http\Fnk::Ceviri('urunler-url').'/{id}', 'Site\UrunController@index');
        Route::get(\App\Http\Fnk::Ceviri('urunler-url').'/{slug}'.'/{kategori}', 'Site\UrunController@kategoriindex');
        Route::get(\App\Http\Fnk::Ceviri('urun-url').'/{slug}', 'Site\UrunController@show');

        Route::get(\App\Http\Fnk::Ceviri('tools-url'), 'Site\ToolsController@index');
        Route::get(\App\Http\Fnk::Ceviri('sertifika-url'), 'Site\ToolsController@index');
        Route::get(\App\Http\Fnk::Ceviri('tool-url').'/{tool_id}', 'Site\ToolsController@show');
        
        
       
        Route::get(\App\Http\Fnk::Ceviri('bayilik-url'), 'Site\BayilerController@index');
        Route::get(\App\Http\Fnk::Ceviri('ekibimiz-url'), 'Site\EkibimizController@index');

        Route::get(\App\Http\Fnk::Ceviri('kurumsal-url'), 'Site\KurumsalController@index');
        // Route::get(\App\Http\Fnk::Ceviri('hizmetler-url'), 'Site\HizmetlerController@index');
        Route::get(\App\Http\Fnk::Ceviri('hizmetler-url').'/{slug}', 'Site\HizmetlerController@show');

        Route::get(\App\Http\Fnk::Ceviri('mevzuat-url').'/{Slug}', 'Site\MevzuatController@index');

        

        Route::get(\App\Http\Fnk::Ceviri('haber-url').'/{Slug}', 'Site\HaberController@index');
    
        Route::get(\App\Http\Fnk::Ceviri('ihracat-url'), 'Site\IhracatController@index');

        

        Route::get(\App\Http\Fnk::Ceviri('video-galeri-url'), 'Site\VideolarController@index');
        Route::get(\App\Http\Fnk::Ceviri('foto-galeri-url').'/{id}', 'Site\FotoGaleriController@show');
        Route::get(\App\Http\Fnk::Ceviri('insan-kaynaklari-url'),'Site\InsanKaynaklariController@index');
        Route::post(\App\Http\Fnk::Ceviri('insan-kaynaklari-url'),'Site\InsanKaynaklariController@store');
        
        Route::get(\App\Http\Fnk::Ceviri('operator-kart-url-0'),'Site\OperatorKartController@index');
        Route::get(\App\Http\Fnk::Ceviri('operator-kart-url-1'),'Site\OperatorKartController@index');
        
        Route::post(\App\Http\Fnk::Ceviri('operator-kart-url-0'),'Site\OperatorKartController@store');
        Route::post(\App\Http\Fnk::Ceviri('operator-kart-url-1'),'Site\OperatorKartController@store');

        Route::get(\App\Http\Fnk::Ceviri('operator-arayan-url'),'Site\OperatorArayanFirmaController@index');
        Route::post(\App\Http\Fnk::Ceviri('operator-arayan-url'),'Site\OperatorArayanFirmaController@store');
        
        Route::get(\App\Http\Fnk::Ceviri('cevir').'/{segment2}/{segment3}/{segment4}', 'Site\CevirController@index4');
        Route::get(\App\Http\Fnk::Ceviri('cevir').'/{segment2}/{segment3}', 'Site\CevirController@index3');
        Route::get(\App\Http\Fnk::Ceviri('cevir').'/{segment2}', 'Site\CevirController@index2');
        Route::get(\App\Http\Fnk::Ceviri('cevir'), 'Site\CevirController@index1');

        Route::get(\App\Http\Fnk::Ceviri('/404'), 'Site\SayfaYokController@index');
        Route::get(\App\Http\Fnk::Ceviri('/404'), 'Site\SayfaYokController@index');   


        Route::get(\App\Http\Fnk::Ceviri('bloglar'), 'Site\BlogController@index');
        Route::get(\App\Http\Fnk::Ceviri('bloglar').'/{id}', 'Site\BlogController@show');
        Route::post(\App\Http\Fnk::Ceviri('bloglar').'/{id}', 'Site\BlogController@update');
     
    });  
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['prefix' => 'Admin','middleware'=>'web'], function () {
    
    Route::get('login', 'Admin\Auth\AuthController@showLoginForm');
    Route::post('login', 'Admin\Auth\AuthController@login');
    Route::get('logout', 'Admin\Auth\AuthController@logout');
    Route::get('password/reset/{token?}', 'Admin\Auth\PasswordController@showResetForm');
    Route::post('password/email', 'Admin\Auth\PasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Admin\Auth\PasswordController@reset');
        
    Route::group(['middleware' => 'adminauth'], function () {
        
        Route::get('/', 'Admin\AnalyticsController@index');
        Route::post('excelYap', 'Admin\AnalyticsController@excelYap');

        Route::get('degistir', 'Admin\DashboardController@degistir');

        Route::get('Menu/Ekle','Admin\MenuController@create');
        Route::post('Menu/Ekle','Admin\MenuController@store');
        Route::get('Menu/Tag/Duzenle/{id}','Admin\MenuController@tagDuzenle');
        Route::get('Menu/Duzenle/{id}','Admin\MenuController@edit');
        Route::post('Menu/Duzenle/{id}','Admin\MenuController@update');
        Route::get('Menu/Listele','Admin\MenuController@index');
        Route::get('Menu/Detay/{id}','Admin\MenuController@show');
        Route::get('Menu/ActiveUpdate/{id}/{deger}','Admin\MenuController@ActiveUpdate');
        Route::get('Menu/Sil/{id}','Admin\MenuController@destroy');
        Route::get('Menu/Sirala/{id}/{dil?}','Admin\MenuController@Sirala');
        Route::post('Menu/Sirala','Admin\MenuController@SiraKaydet');
        Route::get('Menu/Getir','Admin\MenuController@Getir');

        

        Route::get('OnePage/Ekle','Admin\OnePageController@create');
        Route::post('OnePage/Ekle','Admin\OnePageController@store');
        Route::get('OnePage/Tag/Duzenle/{id}','Admin\OnePageController@tagDuzenle');
        Route::get('OnePage/Duzenle/{id}','Admin\OnePageController@edit');
        Route::post('OnePage/Duzenle/{id}','Admin\OnePageController@update');
        Route::get('OnePage/Listele','Admin\OnePageController@index');
        Route::get('OnePage/Detay/{id}','Admin\OnePageController@show');
        Route::get('OnePage/ActiveUpdate/{id}/{deger}','Admin\OnePageController@ActiveUpdate');
        Route::get('OnePage/Sil/{id}','Admin\OnePageController@destroy');
        Route::get('OnePage/Sirala/{id}/{dil?}','Admin\OnePageController@Sirala');
        Route::post('OnePage/Sirala','Admin\OnePageController@SiraKaydet');
        Route::get('OnePage/Getir','Admin\OnePageController@Getir');
        
        
        Route::get('AltMenu/Ekle','Admin\AltMenuController@create');
        Route::post('AltMenu/Ekle','Admin\AltMenuController@store');
        Route::get('AltMenu/Duzenle/{id}','Admin\AltMenuController@edit');
        Route::post('AltMenu/Duzenle/{id}','Admin\AltMenuController@update');
        Route::get('AltMenu/Listele','Admin\AltMenuController@index');
        Route::get('AltMenu/Detay/{id}','Admin\AltMenuController@show');
        Route::get('AltMenu/ActiveUpdate/{id}/{deger}','Admin\AltMenuController@ActiveUpdate');
        Route::get('AltMenu/Sil/{id}','Admin\AltMenuController@destroy');
        Route::get('AltMenu/Sirala/{id}/{dil?}','Admin\AltMenuController@Sirala');
        Route::post('AltMenu/Sirala','Admin\AltMenuController@SiraKaydet');
        Route::get('AltMenu/Getir','Admin\AltMenuController@Getir');
        
        Route::get('Yonetici/Detay/{id}', 'Admin\YoneticiController@show');
        Route::get('Yonetici/Sil/{id}', 'Admin\YoneticiController@destroy');
        Route::get('Yonetici/Ekle', 'Admin\YoneticiController@create');
        Route::get('Yonetici/Listele', 'Admin\YoneticiController@index');
        Route::post('Yonetici/Ekle', 'Admin\YoneticiController@store');
        Route::get('Yonetici/Duzenle/{id}', 'Admin\YoneticiController@edit');
        Route::post('Yonetici/Duzenle/{id}', 'Admin\YoneticiController@update');

        Route::get('SosyalMedya/Ekle','Admin\SosyalMedyaController@create');
        Route::post('SosyalMedya/Ekle','Admin\SosyalMedyaController@store');
        Route::get('SosyalMedya/Duzenle/{id}','Admin\SosyalMedyaController@edit');
        Route::post('SosyalMedya/Duzenle/{id}','Admin\SosyalMedyaController@update');
        Route::get('SosyalMedya/Listele','Admin\SosyalMedyaController@index');
        Route::get('SosyalMedya/Detay/{id}','Admin\SosyalMedyaController@show');
        Route::get('SosyalMedya/ActiveUpdate/{id}/{deger}','Admin\SosyalMedyaController@ActiveUpdate');
        Route::get('SosyalMedya/Sil/{id}','Admin\SosyalMedyaController@destroy');
        Route::get('SosyalMedya/Sirala/{id}/{dil?}','Admin\SosyalMedyaController@Sirala');
        Route::post('SosyalMedya/Sirala','Admin\SosyalMedyaController@SiraKaydet');
        Route::get('SosyalMedya/Getir','Admin\SosyalMedyaController@Getir');

        Route::get('Haber/Ekle','Admin\HaberController@create');
        Route::post('Haber/Ekle','Admin\HaberController@store');
        Route::get('Haber/Duzenle/{id}','Admin\HaberController@edit');
        Route::post('Haber/Duzenle/{id}','Admin\HaberController@update');
        Route::get('Haber/Listele','Admin\HaberController@index');
        Route::get('Haber/Detay/{id}','Admin\HaberController@show');
        Route::get('Haber/ActiveUpdate/{id}/{deger}','Admin\HaberController@ActiveUpdate');
        Route::get('Haber/Sil/{id}','Admin\HaberController@destroy');
        Route::get('Haber/Sirala/{id}/{dil?}','Admin\HaberController@Sirala');
        Route::post('Haber/Sirala','Admin\HaberController@SiraKaydet');
        Route::get('Haber/Getir','Admin\HaberController@Getir');

        Route::get('Hizmet/Ekle','Admin\HizmetController@create');
        Route::post('Hizmet/Ekle','Admin\HizmetController@store');
        Route::get('Hizmet/Duzenle/{id}','Admin\HizmetController@edit');
        Route::post('Hizmet/Duzenle/{id}','Admin\HizmetController@update');
        Route::get('Hizmet/Listele','Admin\HizmetController@index');
        Route::get('Hizmet/Detay/{id}','Admin\HizmetController@show');
        Route::get('Hizmet/ActiveUpdate/{id}/{deger}','Admin\HizmetController@ActiveUpdate');
        Route::get('Hizmet/Sil/{id}','Admin\HizmetController@destroy');
        Route::get('Hizmet/Sirala/{id}/{dil?}','Admin\HizmetController@Sirala');
        Route::post('Hizmet/Sirala','Admin\HizmetController@SiraKaydet');
        Route::get('Hizmet/Getir','Admin\HizmetController@Getir');


        Route::get('Urun/Ekle','Admin\UrunController@create');
        Route::post('Urun/Ekle','Admin\UrunController@store');
        Route::get('Urun/Duzenle/{id}','Admin\UrunController@edit');
        Route::post('Urun/Duzenle/{id}','Admin\UrunController@update');
        Route::get('Urun/Listele','Admin\UrunController@index');
        Route::get('Urun/Detay/{id}','Admin\UrunController@show');
        Route::get('Urun/ActiveUpdate/{id}/{deger}','Admin\UrunController@ActiveUpdate');
        Route::get('Urun/ActiveUpdateOneCikar/{id}/{deger}','Admin\UrunController@ActiveUpdateOneCikar');
        Route::get('Urun/ActiveUpdateKampanyaliYap/{id}/{deger}','Admin\UrunController@ActiveUpdateKampanyaliYap');
        Route::get('Urun/Sil/{id}','Admin\UrunController@destroy');
        Route::get('Urun/Getir','Admin\UrunController@Getir');
        
        Route::get('UrunToKategori', 'Admin\UrunController@UrunToKategori');

        Route::get('Urun/Sirala/{id}','Admin\UrunController@UrunSirala');
        Route::post('Urun/Sirala','Admin\UrunController@UrunSiralaKaydet');
//Tawk to script ekleme alanı Yapılandırılması

        Route::get('Tawk/Duzenle/{id}', 'Admin\TawkController@edit');
        Route::post('Tawk/Duzenle/{id}', 'Admin\TawkController@update');
        Route::get('Tawk/Ekle','Admin\TawkController@create');
        Route::post('Tawk/Ekle','Admin\TawkController@store');
        Route::get('Tawk/Listele','Admin\TawkController@index');
        Route::get('Tawk/Getir','Admin\TawkController@Getir');
        Route::get('Tawk/Detay/{id}','Admin\TawkController@show');
        Route::get('Tawk/Sil/{id}','Admin\TawkController@destroy');


///////////////////////////////

//Popup Yapılandırılması

        Route::get('Popup/Duzenle/{id}', 'Admin\PopupController@edit');
        Route::post('Popup/Duzenle/{id}', 'Admin\PopupController@update');
        Route::get('Popup/Ekle','Admin\PopupController@create');
        Route::post('Popup/Ekle','Admin\PopupController@store');
        Route::get('Popup/Listele','Admin\PopupController@index');
        Route::get('Popup/Getir','Admin\PopupController@Getir');
        Route::get('Popup/Detay/{id}','Admin\PopupController@show');
        Route::get('Popup/Sil/{id}','Admin\PopupController@destroy');


///////////////////////////////



//Google Site-Verifications Yapılandırılması

        Route::get('Google/Duzenle/{id}', 'Admin\GoogleController@edit');
        Route::post('Google/Duzenle/{id}', 'Admin\GoogleController@update');
        Route::get('Google/Ekle','Admin\GoogleController@create');
        Route::post('Google/Ekle','Admin\GoogleController@store');
        Route::get('Google/Listele','Admin\GoogleController@index');
        Route::get('Google/Getir','Admin\GoogleController@Getir');
        Route::get('Google/Detay/{id}','Admin\GoogleController@show');
        Route::get('Google/Sil/{id}','Admin\GoogleController@destroy');


///////////////////////////////
        


//Mail Yapılandırması
        Route::get('Mail/Duzenle/{id}', 'Admin\MailController@edit');
        Route::post('Mail/Duzenle/{id}', 'Admin\MailController@update');
        Route::get('Mail/Ekle','Admin\MailController@create');
        Route::post('Mail/Ekle','Admin\MailController@store');
        Route::get('Mail/Listele','Admin\MailController@index');
        Route::get('Mail/Getir','Admin\MailController@Getir');
        Route::get('Mail/Detay/{id}','Admin\MailController@show');
        Route::get('Mail/Sil/{id}','Admin\MailController@destroy');

        Route::get('MailKullanici/Duzenle/{id}', 'Admin\MailKullaniciController@edit');
        Route::post('MailKullanici/Duzenle/{id}', 'Admin\MailKullaniciController@update');
        Route::get('MailKullanici/Ekle','Admin\MailKullaniciController@create');
        Route::post('MailKullanici/Ekle','Admin\MailKullaniciController@store');
        Route::get('MailKullanici/Listele','Admin\MailKullaniciController@index');
        Route::get('MailKullanici/Getir','Admin\MailKullaniciController@Getir');
        Route::get('MailKullanici/Detay/{id}','Admin\MailKullaniciController@show');
        Route::get('MailKullanici/Sil/{id}','Admin\MailKullaniciController@destroy');
///////////////////////////////////////

        Route::get('Bayilik/Ekle','Admin\BayilikController@create');
        Route::post('Bayilik/Ekle','Admin\BayilikController@store');
        Route::get('Bayilik/Duzenle/{id}','Admin\BayilikController@edit');
        Route::post('Bayilik/Duzenle/{id}','Admin\BayilikController@update');
        Route::get('Bayilik/Listele','Admin\BayilikController@index');
        Route::get('Bayilik/Sil/{id}','Admin\BayilikController@destroy');
        Route::get('Bayilik/Getir','Admin\BayilikController@Getir');
        Route::get('Bayilik/Detay/{id}','Admin\BayilikController@show');

        Route::get('Tools/Ekle','Admin\ToolsController@create');
        Route::post('Tools/Ekle','Admin\ToolsController@store');
        Route::get('Tools/Duzenle/{id}','Admin\ToolsController@edit');
        Route::post('Tools/Duzenle/{id}','Admin\ToolsController@update');
        Route::get('Tools/Listele','Admin\ToolsController@index');
        Route::get('Tools/Detay/{id}','Admin\ToolsController@show');
        Route::get('Tools/ActiveUpdate/{id}/{deger}','Admin\ToolsController@ActiveUpdate');
        Route::get('Tools/ActiveUpdateOneCikar/{id}/{deger}','Admin\ToolsController@ActiveUpdateOneCikar');
        Route::get('Tools/ActiveUpdateKampanyaliYap/{id}/{deger}','Admin\ToolsController@ActiveUpdateKampanyaliYap');
        Route::get('Tools/Sil/{id}','Admin\ToolsController@destroy');
        Route::get('Tools/Getir','Admin\ToolsController@Getir');
        Route::get('Tools/Sirala/{id}', 'Admin\ToolsController@sirala');
        Route::post('Tools/Sirala', 'Admin\ToolsController@SiraKaydet');

        Route::get('Ekibimiz/Ekle','Admin\EkibimizController@create');
        Route::post('Ekibimiz/Ekle','Admin\EkibimizController@store');
        Route::get('Ekibimiz/Duzenle/{id}','Admin\EkibimizController@edit');
        Route::post('Ekibimiz/Duzenle/{id}','Admin\EkibimizController@update');
        Route::get('Ekibimiz/Listele','Admin\EkibimizController@index');
        Route::get('Ekibimiz/Detay/{id}','Admin\EkibimizController@show');
        Route::get('Ekibimiz/ActiveUpdate/{id}/{deger}','Admin\EkibimizController@ActiveUpdate');
        Route::get('Ekibimiz/ActiveUpdateOneCikar/{id}/{deger}','Admin\EkibimizController@ActiveUpdateOneCikar');
        Route::get('Ekibimiz/ActiveUpdateKampanyaliYap/{id}/{deger}','Admin\EkibimizController@ActiveUpdateKampanyaliYap');
        Route::get('Ekibimiz/Sil/{id}','Admin\EkibimizController@destroy');
        Route::get('Ekibimiz/Getir','Admin\EkibimizController@Getir');


         Route::get('Ekibimiz/Sirala/{id}','Admin\EkibimizController@EkibimizSirala');
        Route::post('Ekibimiz/Sirala','Admin\EkibimizController@EkibimizSiralaKaydet');

        Route::get('Kategori/Ekle','Admin\KategoriController@create');
        Route::post('Kategori/Ekle','Admin\KategoriController@store');
        Route::get('Kategori/Duzenle/{id}','Admin\KategoriController@edit');
        Route::post('Kategori/Duzenle/{id}','Admin\KategoriController@update');
        Route::get('Kategori/Listele','Admin\KategoriController@index');
        Route::get('Kategori/Detay/{id}','Admin\KategoriController@show');
        Route::get('Kategori/ActiveUpdate/{id}/{deger}','Admin\KategoriController@ActiveUpdate');
        Route::get('Kategori/Sil/{id}','Admin\KategoriController@destroy');
        Route::get('Kategori/Getir','Admin\KategoriController@Getir');

        Route::get('Ayarlar/Duzenle/{id}', 'Admin\AyarlarController@edit');
        Route::post('Ayarlar/Duzenle/{id}', 'Admin\AyarlarController@update');
        Route::get('Ayarlar/Ekle','Admin\AyarlarController@create');
        Route::post('Ayarlar/Ekle','Admin\AyarlarController@store');
        Route::get('Ayarlar/Listele','Admin\AyarlarController@index');
        Route::get('Ayarlar/Getir','Admin\AyarlarController@Getir');
        Route::get('Ayarlar/Detay/{id}','Admin\AyarlarController@show');
        Route::get('Ayarlar/Sil/{id}','Admin\AyarlarController@destroy');

        Route::get('IletisimBilgileri/Ekle','Admin\IletisimBilgileriController@create');
        Route::post('IletisimBilgileri/Ekle','Admin\IletisimBilgileriController@store');
        Route::get('IletisimBilgileri/Duzenle/{id}','Admin\IletisimBilgileriController@edit');
        Route::post('IletisimBilgileri/Duzenle/{id}','Admin\IletisimBilgileriController@update');
        Route::get('IletisimBilgileri/Listele','Admin\IletisimBilgileriController@index');
        Route::get('IletisimBilgileri/Bekleyenler','Admin\IletisimBilgileriController@Bekleyenler');
        Route::get('IletisimBilgileri/Detay/{id}','Admin\IletisimBilgileriController@show');
        Route::get('IletisimBilgileri/ActiveUpdate/{id}/{deger}','Admin\IletisimBilgileriController@ActiveUpdate');
        Route::get('IletisimBilgileri/Sil/{id}','Admin\IletisimBilgileriController@destroy');
        Route::get('IletisimBilgileri/Sirala/{id}','Admin\IletisimBilgileriController@Sirala');
        Route::get('IletisimBilgileri/Getir','Admin\IletisimBilgileriController@Getir');

        Route::get('IletisimIstekleri/Listele','Admin\IletisimIstekleriController@index');
        Route::get('IletisimIstekleri/Bekleyenler','Admin\IletisimIstekleriController@Bekleyenler');
        Route::get('IletisimIstekleri/Detay/{id}','Admin\IletisimIstekleriController@show');
        Route::get('IletisimIstekleri/ActiveUpdate/{id}/{deger}','Admin\IletisimIstekleriController@ActiveUpdate');
        Route::get('IletisimIstekleri/Sil/{id}','Admin\IletisimIstekleriController@destroy');
        Route::get('IletisimIstekleri/Cevapla/{id}','Admin\IletisimIstekleriController@Cevapla');
        Route::post('IletisimIstekleri/Cevapla/{id}','Admin\IletisimIstekleriController@CevapGonder');
        Route::get('IletisimIstekleri/Getir','Admin\IletisimIstekleriController@Getir');
        
        Route::get('Dil/Ekle','Admin\DilController@create');
        Route::post('Dil/Ekle','Admin\DilController@store');
        Route::get('Dil/Duzenle/{id}','Admin\DilController@edit');
        Route::post('Dil/Duzenle/{id}','Admin\DilController@update');
        Route::get('Dil/Listele','Admin\DilController@index');
        Route::get('Dil/Detay/{id}','Admin\DilController@show');
        Route::get('Dil/ActiveUpdate/{id}/{deger}','Admin\DilController@ActiveUpdate');
        Route::get('Dil/Sil/{id}','Admin\DilController@destroy');
        Route::get('Dil/Getir','Admin\DilController@Getir');
        Route::get('Dil/Tanimla/{id}/{id2}', 'Admin\DilController@tanimlaedit');
        Route::post('Dil/Tanimla/{id}/{id2}', 'Admin\DilController@tanimlaupdate');
        Route::get('Dil/DefaultDilUpdate/{id}/{deger}','Admin\DilController@DefaultDilUpdate');
        Route::get('Dil/Sirala/{id}/{dil?}','Admin\DilController@Sirala');
        Route::post('Dil/Sirala','Admin\DilController@SiraKaydet');
        
        Route::get('DilSabiti/Ekle','Admin\DilSabitiController@create');
        Route::post('DilSabiti/Ekle','Admin\DilSabitiController@store');
        Route::get('DilSabiti/Duzenle/{id}','Admin\DilSabitiController@edit');
        Route::post('DilSabiti/Duzenle/{id}','Admin\DilSabitiController@update');
        Route::get('DilSabiti/Listele','Admin\DilSabitiController@index');
        Route::get('DilSabiti/Detay/{id}','Admin\DilSabitiController@show');
        Route::get('DilSabiti/ActiveUpdate/{id}/{deger}','Admin\DilSabitiController@ActiveUpdate');
        Route::get('DilSabiti/Sil/{id}','Admin\DilSabitiController@destroy');
        Route::get('DilSabiti/Getir','Admin\DilSabitiController@Getir');
        
        Route::get('VideoGaleri/Ekle','Admin\VideoGaleriController@create');
        Route::post('VideoGaleri/Ekle','Admin\VideoGaleriController@store');
        Route::get('VideoGaleri/Duzenle/{id}','Admin\VideoGaleriController@edit');
        Route::post('VideoGaleri/Duzenle/{id}','Admin\VideoGaleriController@update');
        Route::get('VideoGaleri/Listele','Admin\VideoGaleriController@index');
        Route::post('VideoGaleri/Listele','Admin\VideoGaleriController@Sectim');
        Route::get('VideoGaleri/Detay/{id}','Admin\VideoGaleriController@show');
        Route::get('VideoGaleri/ActiveUpdate/{id}/{deger}','Admin\VideoGaleriController@ActiveUpdate');
        Route::get('VideoGaleri/Sil/{id}','Admin\VideoGaleriController@destroy');
        Route::get('VideoGaleri/Getir','Admin\VideoGaleriController@Getir');
        Route::get('VideoGaleri/Sirala','Admin\VideoGaleriController@galerisirala');
        Route::post('VideoGaleri/Sirala','Admin\VideoGaleriController@galerisiralakaydet');
        
        Route::get('VideoGaleriAyar/Ekle','Admin\VideoGaleriAyarController@create');
        Route::post('VideoGaleriAyar/Ekle','Admin\VideoGaleriAyarController@store');
        Route::get('VideoGaleriAyar/Duzenle/{id}','Admin\VideoGaleriAyarController@edit');
        Route::post('VideoGaleriAyar/Duzenle/{id}','Admin\VideoGaleriAyarController@update');
        Route::get('VideoGaleriAyar/Listele','Admin\VideoGaleriAyarController@index');
        Route::post('VideoGaleriAyar/Listele','Admin\VideoGaleriAyarController@Sectim');
        Route::get('VideoGaleriAyar/Getir','Admin\VideoGaleriAyarController@Getir');
        Route::get('VideoGaleriAyar/Sil/{id}','Admin\VideoGaleriAyarController@destroy');
        Route::get('VideoGaleriAyar/Detay/{id}','Admin\VideoGaleriAyarController@show');
        Route::get('VideoGaleriAyar/ActiveUpdate/{id}/{deger}','Admin\VideoGaleriAyarController@ActiveUpdate');

        Route::get('FotoGaleriAyar/Ekle','Admin\FotoGaleriAyarController@create');
        Route::post('FotoGaleriAyar/Ekle','Admin\FotoGaleriAyarController@store');
        Route::get('FotoGaleriAyar/Duzenle/{id}','Admin\FotoGaleriAyarController@edit');
        Route::post('FotoGaleriAyar/Duzenle/{id}','Admin\FotoGaleriAyarController@update');
        Route::get('FotoGaleriAyar/Listele','Admin\FotoGaleriAyarController@index');
        Route::post('FotoGaleriAyar/Listele','Admin\FotoGaleriAyarController@Sectim');
        Route::get('FotoGaleriAyar/Getir','Admin\FotoGaleriAyarController@Getir');
        Route::get('FotoGaleriAyar/Sil/{id}','Admin\FotoGaleriAyarController@destroy');
        Route::get('FotoGaleriAyar/Detay/{id}','Admin\FotoGaleriAyarController@show');
        Route::get('FotoGaleriAyar/ActiveUpdate/{id}/{deger}','Admin\FotoGaleriAyarController@ActiveUpdate');

        Route::get('FotoGaleri/Ekle','Admin\FotoGaleriController@create');
        Route::post('FotoGaleri/Ekle','Admin\FotoGaleriController@store');
        Route::get('FotoGaleri/Duzenle/{id}','Admin\FotoGaleriController@edit');
        Route::post('FotoGaleri/Duzenle/{id}','Admin\FotoGaleriController@update');
        Route::get('FotoGaleri/Listele','Admin\FotoGaleriController@index');
        Route::get('FotoGaleri/Detay/{id}','Admin\FotoGaleriController@show');
        Route::get('FotoGaleri/ActiveUpdate/{id}/{deger}','Admin\FotoGaleriController@ActiveUpdate');
        Route::get('FotoGaleri/Sil/{id}','Admin\FotoGaleriController@destroy');
        Route::get('FotoGaleri/Getir','Admin\FotoGaleriController@Getir');
        Route::post('FotoGaleri/ResimSil/{resim}','Admin\FotoGaleriController@destroy');
        Route::get('FotoGaleri/Sirala','Admin\FotoGaleriController@galerisirala');
        Route::post('FotoGaleri/Sirala','Admin\FotoGaleriController@galerisiralakaydet');

        Route::get('Foto/Ekle/{id}','Admin\FotoGaleriController@fotoekle');
        Route::post('Foto/Ekle/{id}','Admin\FotoGaleriController@fotokaydet');
        Route::post('Foto/ResimSil/{resim}','Admin\FotoGaleriController@ResimSil');
        Route::get('Foto/Sirala/{id}', 'Admin\FotoGaleriController@fotosirala');
        Route::post('Foto/Sirala', 'Admin\FotoGaleriController@fotosiralakaydet');
        
        Route::get('Slider/Ekle','Admin\SliderController@create');
        Route::post('Slider/Ekle','Admin\SliderController@store');
        Route::get('Slider/Duzenle/{id}','Admin\SliderController@edit');
        Route::post('Slider/Duzenle/{id}','Admin\SliderController@update');
        Route::get('Slider/Listele','Admin\SliderController@index');
        Route::post('Slider/Listele','Admin\SliderController@Sectim');
        Route::get('Slider/Bekleyenler','Admin\SliderController@Bekleyenler');
        Route::get('Slider/Detay/{id}','Admin\SliderController@show');
        Route::get('Slider/ActiveUpdate/{id}/{deger}','Admin\SliderController@ActiveUpdate');
        Route::get('Slider/Sil/{id}','Admin\SliderController@destroy');
        Route::get('Slider/Sirala/{id}/{dil?}','Admin\SliderController@Sirala');
        Route::post('Slider/Sirala','Admin\SliderController@SiraKaydet');
        Route::get('Slider/Getir','Admin\SliderController@Getir');
        Route::post('Slider/Crop','Admin\SliderController@Crop');

        Route::get('InsanKaynaklari/Listele','Admin\InsanKaynaklariController@index');
        Route::get('InsanKaynaklari/Getir','Admin\InsanKaynaklariController@Getir');
        Route::get('InsanKaynaklari/Detay/{id}','Admin\InsanKaynaklariController@show');
        Route::get('InsanKaynaklari/Sil/{id}','Admin\InsanKaynaklariController@destroy');

        Route::get('OperatorKart/Listele','Admin\OperatorKartController@index');
        Route::get('OperatorKart/Getir','Admin\OperatorKartController@Getir');
        Route::get('OperatorKart/Detay/{id}','Admin\OperatorKartController@show');
        Route::get('OperatorKart/Sil/{id}','Admin\OperatorKartController@destroy');

        Route::get('OperatorArayanFirma/Listele','Admin\OperatorArayanFirmaController@index');
        Route::get('OperatorArayanFirma/Getir','Admin\OperatorArayanFirmaController@Getir');
        Route::get('OperatorArayanFirma/Detay/{id}','Admin\OperatorArayanFirmaController@show');
        Route::get('OperatorArayanFirma/Sil/{id}','Admin\OperatorArayanFirmaController@destroy');


        Route::get('Blog/Duzenle/{id}', 'Admin\BlogController@edit');
        Route::post('Blog/Duzenle/{id}', 'Admin\BlogController@update');
        Route::get('Blog/Ekle','Admin\BlogController@create');
        Route::post('Blog/Ekle','Admin\BlogController@store');
        Route::get('Blog/Listele','Admin\BlogController@index');
        Route::get('Blog/Getir','Admin\BlogController@Getir');
        Route::get('Blog/Detay/{id}','Admin\BlogController@show');
        Route::get('Blog/Sil/{id}','Admin\BlogController@destroy');
        Route::get('Blog/ActiveUpdate/{id}/{deger}','Admin\BlogController@ActiveUpdate');

    });
}); 

Route::get('admin', function() {
    return Redirect::to('Admin');
});

Route::get('sitemap.xml', function() {
   return Response::view('Site.sitemap')->header('Content-Type', 'application/xml');
});

