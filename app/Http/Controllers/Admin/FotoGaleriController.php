<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\FotoGaleri as VT;
use App\Http\Models\Fotograf;
use Validator;
use Input;
use App\Http\Fnk;
use Auth;

class FotoGaleriController extends AdminController{
	
    protected $Table = 'fotogaleriler'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi','created_at','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi','Eklenme Tarihi','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Foto Galeri';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTab = false; // Form viewlerde Dil Tabları olması istenirse
    protected $FotoButonu = true;
    protected $FotoSiralaButonu = true;
    protected $GaleriSiralaButonu = true;
    protected $Dil = false;
    protected $MetaAlani = true;
    
    public function show($id) {
	$veri = VT::find($id);
        $foto = Fotograf::where('GaleriId', $id)->where('is_active',1)->orderby('Sira','asc')->get();
        return view('Admin.FotoGaleriDetay', ['veri' => $veri, 'foto' => $foto]);
    }
    
    public function update(Request $request,$id){
    	
    	$rules = [
                'Adi' => 'required',
                'is_active' => 'required',
                'MetaTag' => 'required',
                'MetaTitle' => 'required',
                'MetaDescription' => 'required'
            ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }
        
        if($id==0){
        	$trvt = new VT;
            $trvt->ekleyen = Auth::guard('admin')->user()->id;
            $trvt->songuncelleyen = Auth::guard('admin')->user()->id;
        }else{
        	$trvt = VT::find($id);
            $trvt->songuncelleyen = Auth::guard('admin')->user()->id;
        }
        
        $trvt->Adi = $request->Adi;
        $trvt->is_active = $request->is_active;
        $trvt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $trvt->MetaTitle = $request->MetaTitle;
        $trvt->MetaDescription = $request->MetaDescription;
        $save = $trvt->save();

        if($request->hasFile('KapakFotografi')) {
            $klasor = 'images/uploads/FotoGaleri/kapak';
            $ad = Fnk::ResimAdi($klasor, $request->file('KapakFotografi')[0]->getClientOriginalName());
            $ad = $trvt->id . '_' . $ad;
            $request->file('KapakFotografi')[0]->move($klasor, $ad);
            $resim = $ad;
            
            $trvt->KapakFotografi = @$resim;
            $save = $trvt->save();
        }
		
        return json_encode(array('islem' => $save));
    }
    
    public function fotoekle($id){
        $fotogaleri = VT::find($id);
        return view('Admin.FotoForm', compact('fotogaleri'));
    }
    
    public function fotokaydet($id, Request $request){
        $rules = [
                'Resim' => 'required',
            ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }
        $galeri = VT::find($id);	
        $arr = array();
        if ($request->hasFile('Resim')){           
            $files = $request->file('Resim');
            $filecount = count($files);
            $uploadcount = 0;
            $sira=1;
            foreach($files as $index=>$file){
               
                $resimtrvt = new Fotograf();
                $resimtrvt->ekleyen = Auth::guard('admin')->user()->id;
                $resimtrvt->songuncelleyen = Auth::guard('admin')->user()->id;
                $klasor = 'images/uploads/FotoGaleri';
                $ad = Fnk::ResimAdi($klasor,$file->getClientOriginalName());
                $file->move($klasor,$ad);
                $uploadcount++;
                $arr[$index] = $ad;
                Fnk::ThumbnailYap($klasor.'/'.$ad, 270, 197,true,true,'ccc');
                $resimtrvt->Resim = $ad;
                $resimtrvt->Sira = $sira;
                $sira++;
                $resimtrvt->GaleriId = $galeri->id;
                $resimtrvt->is_active = 1;
                $save = $resimtrvt->save();
                $mesaj['yeniresim'] = $klasor.'/'.$ad; //url('images/uploads') sabittir.
            } 
        }
        return json_encode(array('islem' => $save));
    }
    
    public function ResimSil($resim){
        Fotograf::where('id',$resim)->update(['is_active'=>-1]);
    }
    
    public function fotosirala($id){
        $veri = Fotograf::where('GaleriId', $id)->where('is_active',1)->orderby('Sira','asc')->get();
        return view('Admin.FotografSirala', compact('veri'));
    }
    
    public function fotosiralakaydet(Request $request){
        foreach($request->id as $sira=>$fotoid){
            $sira++;
            Fotograf::where('id',$fotoid)->update(['Sira'=>$sira]);
        }
    }
    
    public function galerisirala(){
        $galeri = VT::where('is_active','!=','-1')->get();
        return view('Admin.FotoGaleriSirala', compact('galeri'));
    }
    
    public function galerisiralakaydet(Request $request){
        foreach($request->id as $sira=>$fotoid){
            $sira++;
            VT::where('id',$fotoid)->update(['Sira'=>$sira]);
        }
    }
    
}
