<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Models\VideoGaleri as VT;
use Validator;
use Input;
use Fnk;
use Auth;

class VideoGaleriController extends AdminController{
	
    protected $Table = 'videolar'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi','Link','created_at','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi','Link','Eklenme Tarihi','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Video Galeri';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTab = false; // Form viewlerde Dil Tabları olması istenirse
    protected $GaleriSiralaButonu = true;
    protected $Dil = false;
    
    public function update(Request $request,$id){
    	
    	$rules = [
                'Adi' => 'required',
                'is_active' => 'required',
                'Link' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
            }
          
            if($id==0){
				$trvt = new VT;
				 $trvt->ekleyen = Auth::guard('admin')->user()->id;
                $trvt->songuncelleyen = Auth::guard('admin')->user()->id;
			}else{
            	$trvt = VT::find($id);
                $trvt->songuncelleyen = Auth::guard('admin')->user()->id;
			}
			
                $trvt->Adi = $request->Adi;
                $trvt->Link = $this->LinkKontrol($request->Link);
                $trvt->Slug = $this->Slug($request->Adi,$id);
                $trvt->is_active = $request->is_active;
            $save = $trvt->save();
            
		if($request->hasFile('KapakFotografi')) {
            $klasor = 'images/uploads/FotoGaleri/kapak';
            $ad = Fnk::ResimAdi($klasor, $request->file('KapakFotografi')[0]->getClientOriginalName());
            $ad = $trvt->id . '_' . $ad;
            $request->file('KapakFotografi')[0]->move($klasor, $ad);
            $resim = $ad;
            
            $trvt->KapakFotografi = @$resim;
            $save = $trvt->save();
        }
        return json_encode(array('islem' => $save));
    }
    
    private function LinkKontrol($link){
//        $bol = explode('?t=',$link);
        //https://www.youtube.com/watch?v=r8OipmKFDeM&list=RDEMkjHYJjL1a3xspEyVkhHAsg
//        if(count($bol)==2){
//                $bol2 = explode('&v=',$bol[1]);
//                $link = $bol[0].'?v='.$bol2[1];
//                return $bol2[1];
//        }else{
            $link = explode('?v=',$link);
            if(isset($link[1])){
            	$link = explode('&list=',$link[1]);
			}
            	return $link[0];
//        }
    }
    
    public function galerisirala(){
        $galeri = VT::where('is_active','!=','-1')->get();
        return view('Admin.VideoGaleriSirala', compact('galeri'));
    }
    
    public function galerisiralakaydet(Request $request){
        foreach($request->id as $sira=>$videoid){
            $sira++;
            VT::where('id',$videoid)->update(['Sira'=>$sira]);
        }
    }
    
}
