<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Haber as VT;
use App\Http\Models\Dil as VD;
use App\Http\Models\DilSabiti as DilSabitiVT;
use App\Http\Models\DilTanimlama as DilTanımlamaVT;

use Validator;
use Image;
use File;
use Fnk;


class HaberController extends AdminController
{
    protected $Table = 'haber'; // Database Alanları
    protected $ListelemeAlanlari = ['DilID', 'Adi', 'KisaIcerik' ,'updated_at']; // Database Alanları
    protected $ListelemeBasliklari = ['Dil', 'Haber Adi', 'Haber İçeriği', 'Tarih'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = ['DilID' => ['diller.id', 'diller.UzunAd']]; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Haberler';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = []; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun = [];
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = false;

    protected function DegiskenAta() {
        $this->Degiskenler['diller'] = VD::select('id', 'UzunAd')->get();
    }

    public function update(Request $request, $id) {

        $rules = [
            'Adi' => 'required|max:255',
            'Dil' => 'required',
            'Resim'=>'mimes:jpeg,bmp,png,gif',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }

        $mesaj = array();
        $vt->DilId = $request->Dil;
        $vt->Adi = $request->Adi;

        if(!$vt->Slug){
            if ($request->Dil == 1) {
            $vt->Slug = $this->Slug($request->Adi, $request->id);
            $dilsabitivt = new DilSabitiVT;
            $dilsabitivt->SabitAdi = $vt->Slug;
            $dilsabitivt->Slug = $vt->Slug;
            $dilsabitivt->save();
            
            $diltanimlamavt = new DilTanımlamaVT;
            $diltanimlamavt->DilId = 1;
            $diltanimlamavt->SabitId = DilSabitiVT::where('Slug',$vt->Slug )->first()->id;
            $diltanimlamavt->Slug = $vt->Slug ;
            $diltanimlamavt->Ceviri = $vt->Slug;
            $diltanimlamavt->save();
            }else{
            $vt->Slug = $this->Slug($request->Adi, $request->id);
            }
        }    
        $vt->KisaIcerik = $request->KisaIcerik;
        $vt->Icerik = $request->Icerik;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;
        $vt->is_active = $request->is_active;

        if ($request->hasFile('Resim'))
        {
            $file = $request->file('Resim');
            $klasor = 'images/uploads/Haberler';
            $ad = Fnk::ResimAdi($klasor,$file->getClientOriginalName());

            $file->move($klasor,$ad);
            if($vt->Resim!='../portakalyazilim.gif'){
                File::delete('images/uploads/Haberler'.$vt->Resim);
            }
            $vt->Resim = $ad;
            $mesaj['yeniresim'] = $klasor.'/'.$ad; //url('images/uploads') sabittir.
        }

        $mesaj['islem'] = $vt->save();
        return json_encode($mesaj);
    }
    public function destroy($id)
    {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

}
