<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Models\IletisimIstekleri as VT;
use Validator;
use Input;
use Illuminate\Support\Str;
use Image;
use File;
use Fnk;
use Mail;

class IletisimIstekleriController extends AdminController {
  	protected $Table = 'iletisimistekleri'; // Database Alanları
    protected $ListelemeAlanlari = ['AdSoyad','Konu','created_at']; // Database Alanları
    protected $ListelemeBasliklari = ['Ad Soyad','Konu','Tarih'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = false; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = false; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'İletişim İstekleri';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Okunmadı','Okundu']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTab = false; // Form viewlerde Dil Tabları olması istenirse


	public function Bekleyenler(){
		$where = ['is_active',0];
		return $this->index($where);
	}
	
	public function Cevapla($id){
		$veri = VT::find($id);
		$isactivetext = ['Okundu','Okunmadı'];
		return view('Admin.IletisimCevaplaForm',['veri'=>$veri,'isactivetext'=>$isactivetext]);
	}
	
	public function CevapGonder(){
		 $rules = [
		        'id' => 'required',
		        'Konu' => 'required',
		        'Mesaj' => 'required'
			];
			
		    $validator = Validator::make($request->all(), $rules);
		    if ($validator->fails()){
		        return json_encode(['islem'=>false,'error'=>$validator->errors()->all()]);
		    }
                
                $iletisim = VT::find($request->input('id'));
                
                if(count($iletisim)<1){
					return json_encode(['islem'=>false,'error'=>['Hatalı Veri']]);
				}
                                
				$konu = $request->input('Konu');
                $html = $request->input('Mesaj');
                
                $gonderilecekmail = $iletisim->Eposta;
				$gonderilecekad = $iletisim->AdSoyad;
				
				$mailbilgileri = ['konu' => $konu,'gonderilecekmail'=>$gonderilecekmail,'gonderilecekad'=>$gonderilecekad,'html'=>$html];
				
				$mailgonder = Mail::send('emails.boshtml', $mailbilgileri, function($message) use ($mailbilgileri) {
				    $message->to($mailbilgileri['gonderilecekmail'], $mailbilgileri['gonderilecekad'])->subject($mailbilgileri['konu']);
				});
				
			return json_encode(['islem'=>true]);
	}
	


	public function show($id){
		$veri = VT::find($id);
		$veri->is_active=1;
		$veri->save();
		return view('Admin.IletisimIstekDetay',['veri'=>$veri]);
	}
	

}
