<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Kategori as VT;
use App\Http\Models\DilSabiti as DilSabitiVT;
use App\Http\Models\DilTanimlama as DilTanımlamaVT;
use Validator;
use Fnk;

class KategoriController extends AdminController {

    protected $Table = 'kategori'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi','UstKategoriId','KategoriSirasi','updated_at','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi','Üst Kategori','Kategori Sırası','Tarih','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Kategori';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = ['UstKategoriId', 'Adi']; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = ['UstKategoriId', 'kategoriler']; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = true;

    protected function DegiskenAta() {
        //$this->Degiskenler['kategoriler'] = VT::select('UstKategoriId','Adi','id')->orderBy('UstKategoriId','asc')->get();

        $result = VT::select('UstKategoriId', 'Adi', 'id')->where('is_active',1)->orderBy('UstKategoriId', 'asc')->get();
        $GLOBALS['count'] = 0;
        $tree = [];

        function generatePageTree($datas, $tree, $parent = 0, $limit = 0, $x = 0) {
            if ($limit > 1000)
                return ''; // Make sure not to have an endless recursi
            if ($x == 0) {
                $x = 1;
            }
            for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
                if ($datas[$i]['UstKategoriId'] == $parent) {
                    $datas[$i]["derinlik"]=$GLOBALS['count'];
                    array_push($tree,['id'=>$datas[$i]["id"],'adi'=>$datas[$i]["Adi"],'derinlik'=>$datas[$i]["derinlik"]]);
                    $GLOBALS['count'] ++;
                    $tree = generatePageTree($datas, $tree, $datas[$i]['id'], $limit++, $x++);
                    $GLOBALS['count'] --;
                }
            }
            return $tree;
        }

        $this->Degiskenler['kategoriler'] = generatePageTree($result,$tree);
    }

    public function update(Request $request, $id) {
        $kayit = true;
        $rules = [
            'Adi' => 'required',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required',
            'is_active' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }

        $vt->DilId = 1;
        $vt->UstKategoriId = $request->UstKategoriId;
        $vt->KategoriSirasi = $request->KategoriSirasi;
        $vt->Adi = $request->Adi;
        $vt->Aciklama = $request->Aciklama;
        //Çeviriye uygun hale getirilebilmesi için yapıldı-16.01.2017
        if(!($vt->Slug)){
            $vt->Slug = $this->Slug($request->Adi, $request->id);
            $dilsabitivt = new DilSabitiVT;
            $dilsabitivt->SabitAdi = $vt->Slug;
            $dilsabitivt->Slug = $vt->Slug;
            $dilsabitivt->save();

            $diltanimlamavt = new DilTanımlamaVT;
            $diltanimlamavt->DilId = 1;
            $diltanimlamavt->SabitId = DilSabitiVT::where('Slug',$vt->Slug )->first()->id;
            $diltanimlamavt->Slug = $vt->Slug ;
            $diltanimlamavt->Ceviri = $vt->Adi;
            $diltanimlamavt->save();
        }
        else{
            $vt->Slug = $this->Slug($request->Adi, $request->id);
        }
        
        /////////////////////////////////////////////////////////////////////
        
        $vt->Icerik = $request->Icerik;
        $vt->is_active = $request->is_active;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;

        if ($request->hasFile('Resim')) {
            $file = $request->file('Resim');
            $klasor = 'images/uploads/Kategori';
            $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
            $file->move($klasor, $ad);
            $vt->Resim = $ad;
        }
        $save = $vt->save();

        $vt->save();

        return json_encode(array('islem' => $save, 'kategori' => @$vt));
    }

    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        VT::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

}
