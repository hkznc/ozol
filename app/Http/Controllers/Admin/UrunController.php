<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Urun as VT;
use App\Http\Models\Kategori as VK;
use App\Http\Models\UrunKategori;

use Validator;
use Fnk;

class UrunController extends AdminController {

    protected $Table = 'urun'; // Database Alanları
    protected $ListelemeAlanlari = ['Adi', 'id', 'is_active', 'updated_at']; // Database Alanları
    protected $ListelemeBasliklari = ['Adi', 'Kategori', 'Durum', 'Tarih'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = ['id'=>['urun_kategori.urun_id','urun_kategori.kategori_id']]; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Ürün';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = ['Hayır', 'Evet']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun = ['Hayır', 'Evet'];
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = ['id', 'Adi','Slug']; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = true;

    protected function DegiskenAta() {
        //$this->Degiskenler['kategoriler'] = VT::select('UstKategoriId','Adi','id')->orderBy('UstKategoriId','asc')->get();

        $result = VK::select('UstKategoriId', 'Adi', 'id','Slug')->where('is_active',1)->orderBy('UstKategoriId', 'asc')->get();
        $GLOBALS['count'] = 0;
        $tree = [];

        function generatePageTree($datas, $tree, $parent = 0, $limit = 0, $x = 0) {
            if ($limit > 1000)
                return ''; // Make sure not to have an endless recursi
            if ($x == 0) {
                $x = 1;
            }
            for ($i = 0, $ni = count($datas); $i < $ni; $i++) {
                if ($datas[$i]['UstKategoriId'] == $parent) {
                    $datas[$i]["derinlik"]=$GLOBALS['count'];
                    array_push($tree,['id'=>$datas[$i]["id"],'adi'=>$datas[$i]["Adi"],'slug'=>$datas[$i]["Slug"],'derinlik'=>$datas[$i]["derinlik"]]);
                    $GLOBALS['count'] ++;
                    $tree = generatePageTree($datas, $tree, $datas[$i]['id'], $limit++, $x++);
                    $GLOBALS['count'] --;
                }
            }
            return $tree;
        }

        $this->Degiskenler['kategoriler'] = generatePageTree($result,$tree);
    }

    public function update(Request $request, $id) {
        $kayit = true;
        $rules = [
            'Adi' => 'required',
            'KategoriId' => 'required',
            'is_active' => 'required',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }

       
        $vt->DilId =1;
        $vt->Urun_UstKat = $request->Urun_UstKat;
        $vt->Adi = $request->Adi;
        $vt->Slug = $this->Slug($request->Adi, $request->id);
        $vt->Gosterim = $request->Gosterim;
        $vt->Icerik  = $request->Icerik;
        $vt->Icerik2 = $request->Icerik2;
        $vt->UrunGizli = $request->has('UrunGizli');
        $vt->Urun_AltKat=$request->Urun_AltKat;
        
        // $vt->KategoriId = $request->KategoriId;
       
        $vt->UrunGizli=$request->has('UrunGizli');
        $vt->is_active = $request->is_active;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;
        $save = $vt->save();

        

        UrunKategori::where('urun_id', $vt->id)->delete();
        foreach ($request->KategoriId as $kategoriId) {
            $urun_kategori = UrunKategori::where('urun_id', $vt->id)->where('kategori_id', $kategoriId)->first();
            if (empty($urun_kategori)) {
                $urun_kategori = new UrunKategori;
            }
            $urun_kategori->kategori_id = $kategoriId;
            $urun_kategori->urun_id = $vt->id;
            $urun_kategori->save();
        }

        if(Fnk::optimizeAndReseizeMoreImage($request->id , $request->secili_belge_resim_kapak,'Resim','images/uploads',50)){
            $vt->Resim = Fnk::optimizeAndReseizeMoreImage($request->id , $request->secili_belge_resim_kapak,'Resim','images/uploads',50);
        }

         if(Fnk::optimizeAndReseizeMoreImage($request->id , $request->secili_belge_resim,'Resim2','images/uploads',50)){
            $vt->Resim2 = Fnk::optimizeAndReseizeMoreImage($request->id , $request->secili_belge_resim,'Resim2','images/uploads',50);
        }

         if(Fnk::optimizeAndReseizeMoreImage($request->id , $request->secili_belge_resim2,'Resim3','images/uploads',50)){
            $vt->Resim3 = Fnk::optimizeAndReseizeMoreImage($request->id , $request->secili_belge_resim2,'Resim3','images/uploads',50);
        }
        
        
        $save = $vt->save();
        $vt->save();     
       
        


        return json_encode(array('islem' => $save, 'urun' => @$vt));
    }

    public function destroy($id) {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

    public function UrunSirala() {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi';
        $vtDilId='DilId';
        $veri = VT::where('is_active', '!=', '-1')->where('DilId','1')->orderBy('Sira', 'ASC')->get();
         
        return view('Admin.UrunSirala', ['veri'=>$veri  ,'vtcolumnname' => $vtcolumnname, 'vtDilId' => $vtDilId, 'baslik' => $baslik]);
    }

     public function UrunSiralaKaydet(Request $request){
        foreach($request->id as $sira=>$id){
            $sira++;
            $urun = VT::find($id);
            VT::where('Adi',$urun->Adi)->update(['Sira'=>$sira]);
        }

        return json_encode(["status" => true]);
    }

    // Eğer ürün tablosunda KategoriId diye bir şey var ise önce bu fonksiyon çalıştırılıp sonra ise ürün tablosundaki KategoriId kaldırılacak.
    public function UrunToKategori() {
        $vts = VT::all();
        foreach ($vts as $vt) {
            $urun_kategori = UrunKategori::where('urun_id', $vt->id)->where('kategori_id', $vt->KategoriId)->first();
            if (empty($urun_kategori)) {
                $urun_kategori = new UrunKategori;
            }
            $urun_kategori->kategori_id = $vt->KategoriId;
            $urun_kategori->urun_id = $vt->id;
            $urun_kategori->save();
        }
    }

    // public function ActiveUpdateOneCikarilmis($id, $deger) {
    //   	Vt::->where('id',$id)->update(['OneCikarilmis'=>$deger]);
    //       return redirect()->back();
    //   }
}
