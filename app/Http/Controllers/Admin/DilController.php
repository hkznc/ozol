<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Http\Models\Dil as VT;
use App\Http\Models\DilTanimlama;
use App\Http\Models\DilSabiti;
use Validator;
use Fnk;
use Image;
use File;



class DilController extends AdminController{
	
    protected $Table = 'diller'; // Database Alanları
    protected $ListelemeAlanlari = ['KisaAd','UzunAd','is_active','DefaultDil']; // Database Alanları
    protected $ListelemeBasliklari = ['Kısa Adi','Uzun Adi','Durum','Varsayılan Dil'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = []; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = false; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Dil';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTanimlamaButonu = true;
    protected $Dil = false;
  
  
    public function update(Request $request,$id){
        $rules = [
           
            'is_active' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
        	return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }
	       
        if($request->id==''){
            $vt = new VT;
		}else{
        	$vt = VT::find($request->id);
		}
		
        $vt->KisaAd = $request->DilEkle;
        $vt->Resim = $request->DilEkle.'.svg';
        foreach (Fnk::Ulkeler() as $key => $value) {
            if($key==$request->DilEkle){
                $vt->UzunAd = $value;
            }
        }
        $vt->is_active = $request->is_active;
		       
		
        $save = $vt->save();	
        return json_encode(array('islem' => $save));
    }
    
    
    public function DefaultDilUpdate($id, $deger) {
    	VT::where('id',$id)->update(['DefaultDil'=>$deger]);
        VT::where('id','!=',$id)->update(['DefaultDil'=>0]);
        return redirect()->back();
    }

    public function tanimlaedit($id,$id2){
        $dilsabitleri = DilSabiti::all();
        $diltanimlamalari = DilTanimlama::where('DilId',$id2)->lists('Ceviri','Slug');
        $sabitdil = VT::find($id2);
        $gecerlidil = VT::where('DefaultDil',1)->first();
        $gecerlidil = DilTanimlama::where('DilId',$gecerlidil->id)->lists('Ceviri','Slug');
        $diller = VT::all();
        $dildefaultunid = VT::find($id);
        $defaultunceviri = DilTanimlama::where('DilId', $dildefaultunid->id)->lists('Ceviri','Slug');
        
        return view('Admin.DilTanimlamaForm', compact('dilsabitleri','gecerlidil','defaultunceviri', 'diltanimlamalari', 'diller', 'sabitdil'));
    }
    
    public function tanimlaupdate(Request $request, $id, $id2){
                DilTanimlama::where('DilId',$id2)->delete();
                
                foreach($request->Ceviri as $key=>$value){
                    if($value!=''){
                        $sabit = DilSabiti::find($key);
                        $ekle = new DilTanimlama;
                            $ekle->DilId = $id2;
                            $ekle->SabitId = $key;
                            $ekle->Slug = $sabit->Slug;
                            $ekle->Ceviri = $value;
                        $ekle->save();
                    }
                }
		
        return json_encode(array('islem' => true));
    }

    public function Sirala() {
        $baslik = $this->Title;
        $vtcolumnname = 'UzunAd';
        $veri = VT::where('is_active', '!=', '-1')->orderBy('Sira', 'ASC')->get();
         
        return view('Admin.DilSirala', ['veri'=>$veri  ,'vtcolumnname' => $vtcolumnname, 'baslik' => $baslik]);
    }

     public function SiraKaydet(Request $request){
        foreach($request->id as $sira=>$id){
            $sira++;
            $dil = VT::find($id);
            VT::where('UzunAd',$dil->UzunAd)->update(['Sira'=>$sira]);
        }

        return json_encode(["status" => true]);
    }
    
}
