<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Uygulama as VT;
use App\Http\Models\Dil as VD;
use App\Http\Models\DilSabiti as DilSabitiVT;
use App\Http\Models\DilTanimlama as DilTanımlamaVT;

use Validator;
use Image;
use File;
use Fnk;

class HizmetController extends AdminController
{
    //
     protected $Table = 'uygulama'; // Database Alanları
    protected $ListelemeAlanlari = ['DilID', 'Adi', 'KisaIcerik' ,'updated_at','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Dil', 'Uygulama Adi', 'Uygulama İçeriği', 'Tarih','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : HaberTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['HaberTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = ['DilID' => ['diller.id', 'diller.UzunAd']]; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = true; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'Uygulamalar';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = true; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif', 'Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextOneCikarilmis = []; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $AktifTextKampanyaliUrun = [];
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.
    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $MetaAlani = true;

    protected function DegiskenAta() {
        $this->Degiskenler['diller'] = VD::select('id', 'UzunAd')->get();
    }

    public function update(Request $request, $id) {

        $rules = [
            'Adi' => 'required|max:255',
            'Dil' => 'required',
            'Resim'=>'mimes:jpeg,bmp,png,gif',
            'MetaTag' => 'required',
            'MetaTitle' => 'required',
            'MetaDescription' => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return json_encode(['islem' => false, 'error' => $validator->errors()->all()]);
        }

        if ($request->id == '') {
            $vt = new VT;
        } else {
            $vt = VT::find($request->id);
        }

        $mesaj = array();
        $vt->DilId = $request->Dil;
        $vt->Adi = $request->Adi;
       

        if(!$vt->Slug){
            if ($request->Dil == 1) {
            $vt->Slug = $this->Slug($request->Adi, $request->id);
            $dilsabitivt = new DilSabitiVT;
            $dilsabitivt->SabitAdi = $vt->Slug;
            $dilsabitivt->Slug = $vt->Slug;
            $dilsabitivt->save();
            
            $diltanimlamavt = new DilTanımlamaVT;
            $diltanimlamavt->DilId = 1;
            $diltanimlamavt->SabitId = DilSabitiVT::where('Slug',$vt->Slug )->first()->id;
            $diltanimlamavt->Slug = $vt->Slug ;
            $diltanimlamavt->Ceviri = $vt->Slug;
            $diltanimlamavt->save();
            }else{
            $vt->Slug = $this->Slug($request->Adi, $request->id);
            }
        }    
        $vt->KisaIcerik = $request->KisaIcerik;
        $vt->Icerik = $request->Icerik;
        $vt->MetaTag = json_encode($request->MetaTag,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        $vt->MetaTitle = $request->MetaTitle;
        $vt->MetaDescription = $request->MetaDescription;
        $vt->is_active = $request->is_active;

        if ($request->hasFile('Resim'))
        {
            $file = $request->file('Resim');
            $klasor = 'images/uploads/Hizmetler';
            $ad = Fnk::ResimAdi($klasor,$file->getClientOriginalName());

            $file->move($klasor,$ad);
            if($vt->Resim!='../portakalyazilim.gif'){
                File::delete('images/uploads/Hizmetler'.$vt->Resim);
            }
            $vt->Resim = $ad;
            $mesaj['yeniresim'] = $klasor.'/'.$ad; //url('images/uploads') sabittir.
        }

        if ($request->id == ''){
            //Slider
            if ($request->hasFile('UygulamaResim')) {
                foreach ($request->file('UygulamaResim') as $key => $file) {
                    $klasor = 'images/uploads/Hizmetler/';
                    $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                    $ad = $vt->id . '_' . $key . '_' . $ad;
                    $resimArray[$key] = $ad;
                    $file->move($klasor, $ad);
                }
                $resim_ = json_encode($resimArray);
            }
            $vt->UygulamaResim = @$resim_;
        }elseif ( $request->id ) {
            //Slider
            if( $request->hasFile('UygulamaResim')) {
                foreach ($request->file('UygulamaResim') as $key => $file) {
                    $klasor = 'images/uploads/Hizmetler/';
                    $ad = Fnk::ResimAdi($klasor, $file->getClientOriginalName());
                    $ad = $vt->id . '_' . $key . '_' . $ad;
                    $resimArray[$key] = $ad;
                    $file->move($klasor, $ad);
                }
                if( count($request->secili_belge_resim_UygulamaResim) >0 ){
                    foreach ($request->secili_belge_resim_UygulamaResim as $key_ => $value_) {
                        $resimArray[count($resimArray)] = $value_;
                    }
                }
                $resim__ = json_encode($resimArray);
                $vt->UygulamaResim = @$resim__;
            }elseif( !( $request->hasFile('UygulamaResim') ) ) {
                if(count($request->secili_belge_resim_UygulamaResim) > 0) {
                    foreach ($request->secili_belge_resim_UygulamaResim as $key_ => $value__) {
                        $resimArray[$key_] = $value__;
                    }
                    $resim__ = json_encode($resimArray);
                    $vt->UygulamaResim = @$resim__;
                }else
                    $vt->UygulamaResim = "";
            }
        }

        $mesaj['islem'] = $vt->save();
        return json_encode($mesaj);
    }
    public function destroy($id)
    {
        $vt = VT::find($id);
        $silinecekler = Fnk::ChildrenIds($vt);
        Vt::whereIn('id', $silinecekler)->update(['is_active' => -1]);
        return redirect()->back();
    }

   public function Sirala($UstKatId = 0) {
        $baslik = $this->Title;
        $vtcolumnname = 'Adi';
        $anakategoriler = VT::where('is_active', '!=', '-1')->get();
        $veri = VT::where('is_active', '!=', '-1')->orderBy('Sira', 'desc')->get();
        return view('Admin.Layout.Sirala', ['veri' => $veri, 'anakategoriler' => $anakategoriler, 'ustkatid' => $UstKatId, 'vtcolumnname' => $vtcolumnname, 'baslik' => $baslik]);
    }
}
