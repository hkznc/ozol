<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Models\IletisimBilgileri as VT;
use Validator;
use Input;
use Illuminate\Support\Str;
use Image;
use File;
use Fnk;
use Mail;

class IletisimBilgileriController extends AdminController {
private $title = 'İletişim Bilgileri';

  	protected $Table = 'iletisimbilgileri'; // Database Alanları
    protected $ListelemeAlanlari = ['DilId','SubeAdi','Telefon','is_active']; // Database Alanları
    protected $ListelemeBasliklari = ['Dil','Şube Adı','Telefon','Durum'];
    protected $WhereListele = ""; // listeleme sorgusuna where koşulu koymak istenirse örn : MenuTipi='icerik'
    protected $BirlesmelerText = []; // Listelenecek Veride if atıp farklı veri göstermek içindir. örn : ['MenuTipi'=>['icerik'=>'İçerik','link'=>'Link']] veya ['Dil'=>['tr'=>'Türkçe','en'=>'İngilizce']]
    protected $Birlesmeler = ['DilId'=>['diller.id','diller.UzunAd']]; // key kısmına ListelemeAlanlarindaki ilgili alan yazılıyor. Value Kısmı Array olacak. 0 eleman inner join yapılacak tabloadi.columnadi 1. eleman Kısmı tabloadi.goruntulenecekColumn örn: 'id'=>['ayarlar.id','ayarlar.Logo']
    protected $ListelemeSirala = 0; // Liste sıralaması desc olarak hangi kolondan olacağı belirtilir. 0 dan başlar
    protected $EkleButonu = true; // Listeleme sayfasının üzerindeki Yeni Ekle Butonu
    protected $SiralamaButonu = false; // Listeleme sayfasının üzerinde Yeni Ekle Butonu yanında Sırala Butonu
    protected $TumunuSilButonu = false; // Listeleme sayfasının üzerinde Tümünü Sil Butonu
    protected $KopyalaButonu = false; // Listeleme sayfasının veri satırlarında Kopyala butonu
    protected $KopyalaVeri = ''; // eval fonksiyonu için php yazılabilir
    protected $DuzenleButonu = true; // Listeleme sayfasının veri satırlarında Düzenle butonu
    protected $SilButonu = true; // Listeleme sayfasının veri satırlarında Sil butonu
    protected $DetayButonu = true; // Listeleme sayfasının veri satırlarında Detay butonu
    protected $Title = 'İletişim Bilgileri';
    protected $SilmeMesaji = ''; // Boş Bırakılırsa Direk Silme İşlemi Yapar Dolu İsede /// Silmek İstediğiniz "$silmemesaji" Bulunuyorsa Onlarda Silinecektir. <br>Devam etmek istediğinizden Emin misiniz? şeklinde yazar.
    protected $ResimKlasor = false; // Veritabanından Resim adında bir alan çekildiyse public/uploads baz alınır ek klasör varsa belirtilir.
    protected $AktifText = ['Pasif','Aktif']; // key value mantığındadır. is_active alanı için geçerlidir.
    protected $ustkatkontrol = []; // inner join aynı tablo içinde ise ve bağlılıkları var ise array 0. eleman ListelemeAlanlari 1. eleman da column name örn: ['UstKatId','Adi'] // Detay sayfası ve Listeleme Sayfasında Kullanılır.

    protected $Degiskenler = []; //form viewlere gönderilecek veriler
    protected $is_activeButonu = true; //Form viewlerinde is_active selectbox durumu
    protected $DilTab = false; // Form viewlerde Dil Tabları olması istenirse


	
	public function update(Request $request,$id){
		$rules = [
            'Adres' => 'required',
            'Telefon' => 'required',
            'Eposta' => 'email',
            'SubeAdi' => 'required',
            'is_active' => 'required',
		];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return json_encode(['islem'=>false,'error'=>$validator->errors()->all()]);
        }
        $kordinat = str_replace(' ','',$request->input('Kordinat'));
        $kordinat = rtrim($kordinat,")");
        $kordinat = ltrim($kordinat,"(");
                
        $mesaj = array();
        if($request->id==''){
        	$vt = new VT;
        }else{
        	$vt = VT::find($request->id);
        }
        	$vt->DilId = $request->input('Dil');
        	$vt->Adres = $request->input('Adres');
            $vt->Telefon = $request->input('Telefon');
            $vt->Telefon2 = $request->input('Telefon2');
            $vt->Fax = $request->input('Fax');
            $vt->Eposta = $request->input('Eposta');
            $vt->SubeAdi = $request->input('SubeAdi');
            $vt->ust_Adres = $request->input('ust_Adres');
            $vt->Haftaici_calismasaatleri = $request->input('Haftaici');
            $vt->Haftasonu_calismasaatleri = $request->input('Haftasonu');
            $vt->Kordinat = $kordinat;
            $vt->iletisimde_goster = $request->input('iletisimde_goster');
            $vt->is_active = $request->input('is_active');
	    
	    $mesaj['islem'] = $vt->save();
	    return json_encode($mesaj);
	}
	
}
