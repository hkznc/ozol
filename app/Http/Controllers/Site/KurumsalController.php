<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Slider;
use App\Http\Models\Popup;
use App\Http\Models\Dil;
use App\Http\Models\Urun;
use App\Http\Models\Menu;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\Haber;

class KurumsalController extends Controller
{
    //
    public function index($dil)
    {
    	 $data['kurumsal']=Menu::where('Link','/'.$dil.'/kurumsal-url')->where('MenuTipi','modul')->where('is_active',1)->orderby('id','asc')->first();
    	 return view('Site.Page.Kurumsal',$data);

    }
}
