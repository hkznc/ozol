<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Menu;
use App\Http\Models\Dil;
use App\Http\Models\Slider;
use App\Http\Models\Fotograf;




class MenuController extends Controller
{
  public function show($dil, $slug){

    $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('is_active',1)->where('Slug',$slug)->first();
    if(empty($data["menu"])){
      abort(404);
    }

    // $data["sliders"] = Slider::where('DilId', Dil::where('KisaAd', $dil)->first()->id)
    //             ->whereRaw('is_active = 1 and ((baslangic <="' . date('Y-m-d') . '" AND bitis >= "' . date('Y-m-d') . '") OR (baslangic = "0000-00-00 00:00:00" OR bitis = "0000-00-00 00:00:00"))')
    //             ->orderBy('sira', 'asc')
    //             ->get();
    //     if (empty($data["sliders"]->first())) {
    //         $data["sliders"] = Slider::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)
    //                 ->whereRaw('is_active = 1 and ((baslangic <="' . date('Y-m-d') . '" AND bitis >= "' . date('Y-m-d') . '") OR (baslangic = "0000-00-00 00:00:00" OR bitis = "0000-00-00 00:00:00"))')
    //                 ->orderBy('sira', 'asc')
    //                 ->get();
    //     }
    // $data["digermenuler"] = Menu::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('is_active',1)->where('UstKatId', $data["menu"]->UstKatId)->where('UstKatId','!=', 0)->orderBy('Sira','asc')->get();

    // $data["fotograf"] = Fotograf::where('GaleriId', 1)->where('is_active',1)->orderby('Sira', 'asc')->paginate(8);
   
   return view('Site.Page.Sayfa', $data);
 }

}
