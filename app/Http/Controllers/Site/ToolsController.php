<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Tools;
use App\Http\Models\Urun;
use App\Http\Models\Menu;
use App\Http\Models\Kategori;
use App\Http\Models\Dil;

class ToolsController extends Controller {

    public function show(Request $request, $dil, $slug) {
        $tool_id = $request->segment(3);
        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('Slug', urldecode($request->segment(2)))->first();

        $data["tool"] = Tools::where('id', $tool_id)->first();
        
        if (empty($data["tool"])) {
            $data["tools"] = Tools::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)->where('is_active', 1)->where('Slug', $slug)->first();
            if (empty($data["tools"])) {
                abort(404);
            }
        }
        if(@$data["tool"]["Adi"] == "MİTSUBİSHİ") 
            return view('Site.Page.Tool_Mitsubishi', $data);
        
        return view('Site.Page.Tool', $data);
    }
    
   
    public function index(Request $request, $dil) {
        $data["tools"] = Tools::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('is_active', 1)->orderby('Sira', 'asc')->get();
        if (empty($data["tools"]->first())) {
            $data["tools"] = Tools::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)->where('is_active', 1)->first();
            if (empty($data["tools"]->first())) {
                abort(404);
            }
        }
        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('is_active', 1)->where('Slug', urldecode($request->segment(2)))->first();
        
        return view('Site.Page.Tools', $data);
    }
}
