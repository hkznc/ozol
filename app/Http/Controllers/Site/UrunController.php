<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Urun;
use App\Http\Models\UrunKategori;
use App\Http\Models\Menu;
use App\Http\Models\Kategori;
use App\Http\Models\Dil;

class UrunController extends Controller {

    public function show(Request $request, $dil, $slug) {

        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('Slug', urldecode($request->segment(2)))->first();

        $data["urun"] = Urun::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('is_active', 1)->where('Slug', $slug)->orderby('Sira','ASC')->first();
        if (empty($data["urun"])) {
            $data["urun"] = Urun::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)->where('is_active', 1)->where('Slug', $slug)->orderby('Sira','ASC')->first();
            if (empty($data["urun"])) {
                abort(404);
            }
        }
     

        return view('Site.Page.Urun', $data);
    }

    public function index(Request $request, $dil, $id) {

        $data["urunler"] = Urun::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('is_active', 1)->where('Urun_UstKat', $id)->where('UrunGizli',0)->where('Urun_AltKat','!=','')->orderby('Sira','ASC')->paginate(20);
        if (empty($data["urunler"]->first())) {
            $data["urunler"] = Urun::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)->where('is_active', 1)->where('Urun_UstKat', $id)->where('UrunGizli',0)->where('Urun_AltKat','!=','')->orderby('Sira','ASC')->paginate(20);
            if (empty($data["urunler"]->first())) {
               abort(404);
            }
        }


        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('id',$id)->first();
        
        
        return view('Site.Page.Urunler', $data);
    }

    public function kategoriindex(Request $request, $dil, $slug, $kategori) {

        $dil='tr';
        $kategoribulundumu = Kategori::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('Slug', $kategori)->first();
  
        if (empty($kategoribulundumu)) {
            abort(404);
        }
        $kategoriid = Kategori::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('Slug', $kategori)->first()->id;
 
       
        

        $urun_kategoriler = UrunKategori::where('kategori_id', $kategoriid)->lists('urun_id');

        $data["urunler"] = Urun::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->whereIn('id', $urun_kategoriler)->where('is_active', 1)->orderBy('Sira', 'ASC')->paginate(20);

        if (empty($data["urunler"]->first())) {
            $data["urunler"] = Urun::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)->whereIn('id', $urun_kategoriler)->where('is_active', 1)->orderBy('Sira', 'ASC')->paginate(20);
            if (empty($data["urunler"]->first())) {
                //abort(404);

            }
        }
        $data["menu"] = Kategori::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('Slug', urldecode($request->segment(4)))->first();
       
        $data["kategori"] = Kategori::where('DilId', Dil::where('KisaAd', $dil)->first()->id)->where('Slug', urldecode($request->segment(4)))->first();
        if(empty($data["kategori"])){
            $data["kategori"] = Kategori::where('DilId', Dil::where('KisaAd', 'tr')->first()->id)->where('Slug', urldecode($request->segment(4)))->first();
        }
        
        return view('Site.Page.Urunler', $data);
    }

}
