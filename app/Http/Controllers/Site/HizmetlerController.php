<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Slider;
use App\Http\Models\Popup;
use App\Http\Models\Dil;
use App\Http\Models\Urun;
use App\Http\Models\Menu;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\Haber;
use App\Http\Models\Uygulama as VT;

class HizmetlerController extends Controller
{
    //
     public function index($dil)
    {
    	 $data['menu']=VT::where('is_active',1)->orderby('Sira','Desc')->get();
    	 return view('Site.Page.Sayfa',$data);

    }
    public function show($dil,$id)
    {
    	 $data['menu']=VT::where('is_active',1)->orderby('Sira','Desc')->where('id',$id)->first();
         if(!empty($data['menu']->id))
    	 return view('Site.Page.Sayfa',$data);
         else
         abort(404);
    }
}
