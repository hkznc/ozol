<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\IletisimIstekleri;
use App\Http\Models\Mail ;
use App\Http\Models\MailKullanici ;
use Validator;
use Mail as MailSend;
use App\Http\Fnk;
use App\Http\Models\Dil;
use App\Http\Models\Menu;


class IletisimController extends Controller {

    public function index(Request $request) {
        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', urldecode($request->segment(1)))->first()->id)->where('is_active', 1)->where('Slug', urldecode($request->segment(2)))->first();     
        $data['iletisim']=IletisimBilgileri::where('DilId',Dil::where('KisaAd', urldecode($request->segment(1)))->first()->id)->where('is_active',1)->first();
        return view('Site.Page.Iletisim',$data);
    }

    public function store(Request $request) {
        $rules = [
            'AdSoyad' => 'required',
            'Eposta' => 'required|email',
            'Konu' => 'required',
            'Mesaj' => 'required',
            
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        @$kullanicilar = MailKullanici::where('is_active',1)->where('DilId',Dil::where('KisaAd', urldecode($request->segment(1)))->first()->id)->first()->get();
        @$gonderen = Mail::where('DilId',Dil::where('KisaAd', urldecode($request->segment(1)))->first()->id)->first()->GonderilecekMailAdres;
       
        $istek = new IletisimIstekleri;
        $istek->AdSoyad = $request->AdSoyad;
        $istek->Eposta = $request->Eposta;
        $istek->Konu = $request->Konu;
        $istek->Mesaj = $request->Mesaj;
        $istek->Telefon = $request->Telefon;
        $istek->gelen_ip = $request->ip();
        $kaydet = $istek->save();
        $dilIlet= urldecode($request->segment(1));   
       
       
        if ($kaydet) {
            $mail =$this->mail_send($istek,$dilIlet,$kullanicilar,$gonderen);
            $data["status"] = 'TRUE';
            
        } else {
            $data["status"] = 'FALSE';
        }

        
        return redirect()->back()->with('status', $data["status"]);
    }

     public function mail_send($req,$dil,$kullanicilar,$gonderen)
    {
        $title = 'İletişim Formu';
        $content = '';
        $req = $req;
        Fnk::mailSet($dil);
  
        MailSend::send('Site.mail_iletisim', ['title'=>$title,'req'=>$req ], function ($message) use ($req,$dil,$kullanicilar,$gonderen)
        {
            $message->setSubject('İletişim Formu');
            $message->from($gonderen , 'İletişim Formu');

            foreach ($kullanicilar as $key => $value) {
                $message->to($value->Eposta);
            }
            //$message->attach($dosya[0]);
        });
    }

}
