<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Bayilik;
use Validator;
use App\Http\Models\Dil;
use App\Http\Models\Menu;

class BayilerController extends Controller {

    public function index(Request $request) {
        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', urldecode($request->segment(1)))->first()->id)->where('is_active', 1)->where('Slug', urldecode($request->segment(2)))->first();     
        
        return view('Site.Page.Bayiler',$data);
    }

    

}
