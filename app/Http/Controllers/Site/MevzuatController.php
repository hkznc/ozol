<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Slider;
use App\Http\Models\Popup;
use App\Http\Models\Dil;
use App\Http\Models\Urun;
// use App\Http\Models\Menu;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\Haber;
use App\Http\Models\Menu as VT;
use PDF;


class MevzuatController extends Controller
{
    //
    public function index($dil,$slug)
    {

    	 $link='/'.$dil.'/mevzuat-url';
    	 $data['mevzuat']=VT::where('is_active',1)->where('Slug',$slug)->where('Link',$link)->first();
    	 $data['mevzuatlar']=VT::where('is_active',1)->where('Link',$link)->where('UstKatId','!=',0)->orderby('Sira','Desc')->get();
    	 if(!empty($data['mevzuat']->id))
    		 return view('Site.Page.Mevzuat',$data);
    	 else
    	 	 abort(404);


    }


}
