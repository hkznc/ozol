<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\DilTanimlama;
use App\Http\Models\Dil;

class CevirController extends Controller {

    public function index1($dil) {
        return redirect('/' . $dil);
    }

    public function index2($dil, $segment2) {
        if ($segment2 != 'menu') {
            $cevirislug = DilTanimlama::select('Slug')->where('Slug', 'like', '%-url%')->where('Ceviri', $segment2)->first()->Slug;
            $ceviri = DilTanimlama::where('Slug', $cevirislug)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()->Ceviri;
            if (empty($ceviri)) {
                abort(404);
            }
            return redirect('/' . $dil . '/' . $ceviri);
        }
    }

    public function index3($dil, $segment2, $segment3) {
       

        if ($segment2 != 'menu') {
        
            $cevirislug = DilTanimlama::select('Slug')->where('Slug', 'like', '%-url%')->where('Ceviri', $segment2)->first()->Slug;
            $ceviri = DilTanimlama::where('Slug', $cevirislug)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()->Ceviri;
            if (empty($ceviri)) {
                abort(404);
            }
            return redirect('/' . $dil . '/' . $ceviri . '/' . $segment3);
        } else {
            $cevirislug = DilTanimlama::select('Slug')->where('Ceviri', $segment3)->first()->Slug;
            if(empty(DilTanimlama::where('Slug', $cevirislug)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()))
            {
             return redirect('/' . $dil . '/404/');
            }
            $ceviri = DilTanimlama::where('Slug', $cevirislug)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()->Ceviri;
            return redirect('/' . $dil . '/menu/' . $ceviri);
        }

    }

    public function index4($dil, $segment2, $segment3, $segment4){

         if ($segment3 != 'menu') {
            $cevirislug = DilTanimlama::select('Slug')->where('Slug', 'like', '%-url%')->where('Ceviri', $segment2)->first()->Slug;
            $ceviri = DilTanimlama::where('Slug', $cevirislug)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()->Ceviri;
            $cevirislug2 = DilTanimlama::select('Slug')->where('Ceviri', $segment3)->first()->Slug;
            $ceviri2 = DilTanimlama::where('Slug', $cevirislug2)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()->Ceviri;
            if (empty($ceviri || $ceviri2)) {
                abort(404);
            }
            return redirect('/' . $dil . '/' . $ceviri . '/' . $ceviri2. '/' .$segment4);
        } else {
            $cevirislug = DilTanimlama::select('Slug')->where('Ceviri', $segment3)->first()->Slug;
            if(empty(DilTanimlama::where('Slug', $cevirislug)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()))
            {
             return redirect('/' . $dil . '/404/');
            }
            $ceviri = DilTanimlama::where('Slug', $cevirislug)->where('DilId', Dil::where('KisaAd', $dil)->first()->id)->first()->Ceviri;
            return redirect('/' . $dil . '/' . $segment2 .'/' . $ceviri . '/' .$segment4 );
        }

    }

}
