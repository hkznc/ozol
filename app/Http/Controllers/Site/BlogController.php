<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use \App\Http\Models\Bloglar;
use \App\Http\Models\Dil;
use App\Http\Models\Mail ;
use App\Http\Models\MailKullanici ;
use Mail as MailSend;
use App\Http\Fnk;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dil_id = Dil::where('is_active',1)->where('KisaAd',$request->segment(1))->first()->id;
        $data["bloglar"] = Bloglar::where('is_active',1)->where('DilId',$dil_id)->get();
        return view('Site.Page.Bloglar',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($dil,$id)
    {
        
        $data["blog"] = Bloglar::where('id',$id)->first();
        // $data["yorumlar"] = BloglarYorum::where('blog_id',$data["blog"]->id)->where('is_active',1)->get();
        return view("Site.Page.Blog",$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$dil,$id)
    {

        
           $yorum = new BloglarYorum;
           $yorum->blog_id = $id;
           $yorum->ad = $request->ad;
           $yorum->eposta = $request->eposta;
           $yorum->tel = $request->tel;
           $yorum->mesaj = $request->mesaj;
           $yorum->is_active = 0;
           $yorum->save();      

           @$kullanicilar = MailKullanici::where('is_active',1)->where('DilId',Dil::where('KisaAd', $request->segment(1))->first()->id)->get();
           @$gonderen = Mail::where('DilId',Dil::where('KisaAd', $request->segment(1))->first()->id)->first()->GonderilecekMailAdres;
           
           if($request->ip() == "::1"){
                $env = 0;
            }
            else{
                $env = 1;
            }
           if($yorum ){
                $mail = $this->mail_send($yorum,$request->segment(1),$kullanicilar,$gonderen,$env);
                $data["yorum"] = "TRUE";
           }
           else{
                $data["yorum"] = "FALSE";
           }
       
       return redirect()->back()->with("yorum",$data["yorum"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function mail_send($req,$dil,$kullanicilar,$gonderen,$env)
    {
        $title = 'Yorum Formu';
        $content = '';
        $req = $req;
        $env = $env;
        Fnk::mailSet($dil);
        MailSend::send('Site.mail_yorum', ['title'=>$title,'req'=>$req ,'env'=>$env ], function ($message) use ($req,$dil,$kullanicilar,$gonderen,$env)
        {
            $message->setSubject('Yorum Formu');
            $message->from($gonderen , 'Yorum Formu');
 
            foreach ($kullanicilar as $key => $value) {
                $message->to($value->Eposta);
            }
            //$message->attach($dosya[0]);
        });
    }
}

