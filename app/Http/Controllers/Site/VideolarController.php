<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\VideoGaleri;
use App\Http\Models\VideoGaleriAyar;
use App;
use App\Http\Models\Dil;
use App\Http\Models\Menu;

class VideolarController extends Controller
{
    public function index(Request $request) {

        $data["dil"] = $request->segment(1);

        $data["menu"] = Menu::where('DilId', Dil::where('KisaAd', $request->segment(1))->first()->id)->where('Slug', urldecode($request->segment(2)))->first();

        if( !empty(VideoGaleriAyar::where('DilId', Dil::where('KisaAd', $request->segment(1))->first()->id)->where('is_active',1)->first()->Fotograf) )

            $data["fotograf"] = VideoGaleriAyar::where('DilId', Dil::where('KisaAd', $request->segment(1))->first()->id)->first()->Fotograf;

        $data["videogaleri"] = VideoGaleri::where('is_active', 1)->orderby('Sira', 'asc')->get();

        

        return view('Site.Page.Videolar', $data);

    }

}
