<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Slider;
use App\Http\Models\Popup;
use App\Http\Models\Dil;
use App\Http\Models\Urun;
use App\Http\Models\Menu;
use App\Http\Models\IletisimBilgileri;
use App\Http\Models\Haber;
use App\Http\Models\Fotograf as VT;
use App\Http\Models\FotoGaleri as FG;

class FotoGaleriController extends Controller
{
     public function index($dil)
    {
    	# code...
    	$data['fotogaleri']=FG::where('is_active',1)->get();
    	return view('Site.Page.FotoGaleri',$data);
    }
}
