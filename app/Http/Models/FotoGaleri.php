<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class FotoGaleri extends Model
{
    protected $table = 'fotogaleriler';
    
    public function Fotograflar() {
        return $this->hasMany('App\Http\Models\Fotograf','GaleriId');
    }
}
