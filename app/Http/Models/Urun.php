<?php 
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Urun extends Model 
{
	protected $table = 'urun';

	public function UrunKategori()
	{
		return $this->hasMany('App\Http\Models\UrunKategori', 'urun_id', 'id');
	}

	public function UrunKategoriKontrol($id)
	{
		return $this;
	}
}