<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Analytics extends Model
{
    protected $table = "analytics";
}
