<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class AltMenu extends Model {

	protected $table = 'altmenuler';
	
	public function parents(){
		return $this->belongsTo('App\Http\Models\AltMenu','UstKatId','id');
	}
	public function children(){
		return $this->hasMany('App\Http\Models\AltMenu','UstKatId');
	}

}
