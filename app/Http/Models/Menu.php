<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

	protected $table = 'menuler';
	
	public function parents(){
		return $this->belongsTo('App\Http\Models\Menu','UstKatId','id');
	}
	public function children(){
		return $this->hasMany('App\Http\Models\Menu','UstKatId')->where('is_active',1);
	}

}
